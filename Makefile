
binary: install

install: protos
	go install raftdb/server raftdb/proto raftdb/server/locking raftdb/server/storage raftdb/server/log raftdb/server/versionedstorage raftdb/rdbcl raftdb/client raftdb

test:
	go test -v -cover raftdb/server && go test -v -cover raftdb/server/storage && go test -v -cover raftdb/server/log && go test -v -cover raftdb/server/locking && go test -v -cover raftdb/server/versionedstorage

bench:
	go test -bench .

protos:
	protoc --gogofast_out=. proto/raft.proto proto/client.proto proto/log.proto

