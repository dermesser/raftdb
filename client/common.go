package client

import (
	"errors"
	"raftdb/proto"
	"raftdb/server/constants"
)

// Returns the response and nil if the request and all actions succeeded.
// Returns the response and an error != nil if the response was negative.
// Returns nil and an error != nil if an error occurred on a layer below, e.g. RPC or we talked to the wrong member.
func (cl *realClient) doClientRequest(rq *proto.ClientRequest) (*proto.ClientResponse, error) {
	rp := new(proto.ClientResponse)
	var err error

	rq.ClusterId = &cl.cluster_id

	for i := 0; i < cl.max_retry; i++ {

		err = cl.master_cl.RequestProtobuf(rq, rp, constants.RPC_SERVICE, "ClientRequest", nil)

		if err != nil {
			cl.reconnect()
			continue
		}

		if rp.GetStatus() == proto.ClientResponse_ERROR_NOT_MASTER {
			cl.reconnect()
			err = errors.New(proto.ClientResponse_ERROR_NOT_MASTER.String())
			continue
		}

		if rp.GetStatus() != proto.ClientResponse_SUCCESS {
			return rp, errors.New(rp.GetStatus().String())
		} else {
			return rp, nil
		}

	}

	return nil, err
}

func (cl *realClient) doHangingClientRequest(rq *proto.ClientHangingRequest) (*proto.ClientHangingResponse, error) {
	rp := new(proto.ClientHangingResponse)
	var err error

	rq.ClusterId = &cl.cluster_id

	for i := 0; i < cl.max_retry; i++ {

		err = cl.master_cl.RequestProtobuf(rq, rp, constants.RPC_SERVICE, "ClientHangingRequest", nil)

		if err != nil {
			cl.reconnect()
			continue
		}

		if rp.GetStatus() == proto.ClientHangingResponse_NOT_MASTER {
			cl.reconnect()
			err = errors.New(proto.ClientHangingResponse_NOT_MASTER.String())
			continue
		}

		if rp.GetStatus() != proto.ClientHangingResponse_SUCCESS {
			return rp, errors.New(rp.GetStatus().String())
		} else {
			return rp, nil
		}

	}

	return nil, err
}
