package client

import (
	"raftdb/proto"
	"raftdb/server/locking"
	"time"

	pb "github.com/gogo/protobuf/proto"
)

func event2String(ev int) string {
	return locking.NotificationType(ev).String()
}

type Watcher struct {
	Events  chan string
	cl      *realClient
	key     string
	stop    bool
	timeout time.Duration
}

func (cl *realClient) GetWatcher(key string) (*Watcher, error) {
	w := &Watcher{Events: make(chan string, 5), cl: cl, key: key,
		timeout: cl.lock_timeout}

	go w.run()
	return w, nil
}

func (w *Watcher) Wait(timeout time.Duration) string {
	t := time.NewTimer(timeout)
	select {
	case ev := <-w.Events:
		t.Stop()
		return ev
	case <-t.C:
		return locking.Timeout.String()
	}
}

func (w *Watcher) Stop() {
	w.stop = true
}

func (w *Watcher) sendRequest() (int, error) {
	cr := &proto.ClientHangingRequest{Type: proto.ClientHangingRequest_WATCH.Enum(),
		WatchKey:    []byte(w.key),
		ClusterId:   &w.cl.cluster_id,
		TimeoutNsec: pb.Uint64(uint64(w.timeout.Nanoseconds())),
	}

	rp, err := w.cl.doHangingClientRequest(cr)

	return int(rp.GetEvent()), err
}

func (w *Watcher) run() {
	for !w.stop {
		old_rc := w.cl.reconnect_count

		ev, err := w.sendRequest()

		if err != nil {
			if old_rc == w.cl.reconnect_count {
				w.cl.reconnect()
			}
			continue
		}

		if ev != int(locking.Timeout) {
			select {
			case w.Events <- locking.NotificationType(ev).String():
				continue
			default:
				continue
			}
		}
	}
}
