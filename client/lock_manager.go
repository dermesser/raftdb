package client

import (
	"sync"
	"time"
)

// Implements a manager that can be used to hold timing-out locks,
// renewing them automatically.

// Type of function that is called every time the lock is acquired or renewed
type OnLockAcquiredCallback func(key string)

// Type of function that is called every time the lock could not be acquired.
type OnLockFailedCallback func(key string)

// Type of function that is called when the lock got lost (someone else acquired it)
type OnLockLostCallback func(key string)

type LockManager struct {
	sync.Mutex
	cl         *realClient
	is_polling bool
	// We own the lock at least until
	is_owner_until time.Time
	// Either our client ID if owner or the one of the current holder
	current_holder string
	// Id of the lock and value to set it to once we've acquired the lock
	key, value string
	// How long our lock is valid. The expiration sent to the server is this
	// plus 5 seconds to account for latencies; this is the poll interval that
	// we use to renew the lock.
	lock_expiration_duration time.Duration
	// How long we hang waiting for a lock.
	lock_acquire_interval time.Duration

	on_acquired OnLockAcquiredCallback
	on_failed   OnLockFailedCallback
	on_lost     OnLockLostCallback
}

func (cl *realClient) GetLockManager(key, value string) *LockManager {
	lm := &LockManager{}

	lm.cl = cl
	lm.is_polling = false

	lm.key = key
	lm.value = value
	lm.current_holder = ""

	lm.lock_acquire_interval = cl.lock_timeout
	lm.lock_expiration_duration = cl.lock_expiration

	lm.on_acquired = nil
	lm.on_failed = nil
	lm.on_lost = nil

	return lm
}

func (lm *LockManager) SetValue(val string) error {
	lm.Lock()
	defer lm.Unlock()
	lm.value = val
	expiration := lm.lock_expiration_duration + 5*time.Second

	result, err := lm.cl.lock_internal(lm.key, []byte(lm.value), lm.cl.client_id,
		expiration, lm.lock_acquire_interval)

	if err != nil {
		if err.Error() == "ERROR_LOCKED" {
			lm.current_holder = result.Owner()
			lm.value = ""
			return err
		}
	}
	return err
}

func (lm *LockManager) OnAcquired(callback OnLockAcquiredCallback) {
	lm.on_acquired = callback
}

func (lm *LockManager) OnLost(callback OnLockLostCallback) {
	lm.on_lost = callback
}

func (lm *LockManager) OnFailed(callback OnLockFailedCallback) {
	lm.on_failed = callback
}

// attempt_interval is the duration that LOCK calls hang at most. If we don't
// hold the lock after this, we try again.
// expiration is the duration that our locks last, i.e. after this a re-lock
// is issued in order to continue holding the lock.
func (lm *LockManager) SetDurations(attempt_interval, expiration time.Duration) {
	lm.Lock()
	defer lm.Unlock()
	lm.lock_acquire_interval = attempt_interval
	lm.lock_expiration_duration = expiration
}

func (lm *LockManager) GetHolder() string {
	return lm.current_holder
}

// Tries to release the lock and stops the try-lock thread.
// Waits at most for the lock expiration duration.
// Returns true if lock could be released.
func (lm *LockManager) Release() error {
	lm.Lock()
	defer lm.Unlock()

	lm.is_polling = false
	_, err := lm.cl.Unlock(lm.key)
	lm.is_owner_until = time.Unix(0, 0)
	lm.current_holder = ""

	return err
}

func (lm *LockManager) Run() error {
	if lm.is_polling {
		return nil
	}

	lm.is_polling = true
	defer func() { lm.is_polling = false }()

	for lm.is_polling {
		lm.Lock()

		expiration := lm.lock_expiration_duration + 20*time.Second // account for raftdb re-election period

		result, err := lm.cl.lock_internal(lm.key, []byte(lm.value), lm.cl.client_id,
			expiration, lm.lock_acquire_interval)

		if err != nil {
			if err.Error() == "ERROR_LOCKED" {
				lm.current_holder = result.Owner()
			} else if err.Error() == "TIMEOUT" {
				lm.Unlock()
				continue
			} else if err.Error() == "ERROR_INTERNAL" || err.Error() == "ERROR" {
				return err
			} else { // Reconnectable error
				err := lm.cl.slow_reconnect()

				if err != nil {
					lm.Unlock()
					return err
				}
			}

			if lm.is_owner_until.UnixNano() != 0 && lm.on_lost != nil {
				lm.on_lost(lm.key)
			}

			lm.is_owner_until = time.Unix(0, 0)

			if lm.on_failed != nil {
				lm.on_failed(lm.key)
			}
		} else {
			lm.current_holder = lm.cl.client_id
			lm.is_owner_until = time.Now().Add(lm.lock_expiration_duration)

			if lm.on_acquired != nil {
				lm.on_acquired(lm.key)
			}
		}

		lm.Unlock()

		// If not owner, try again right after this.
		if lm.is_owner_until.After(time.Now()) {
			time.Sleep(lm.lock_expiration_duration)
		}
	}
	return nil
}
