package client

import (
	"errors"
	lkup "github.com/dermesser/clusterlookup/client"
	rpc "github.com/dermesser/clusterrpc/client"
	"github.com/dermesser/clusterrpc/log"
	smgr "github.com/dermesser/clusterrpc/securitymanager"
	"os"
	"raftdb/server/constants"
	"time"
)

type Client interface {
	SetClientId(string)
	SetLkupSrv(string) error
	SetLockExpiration(time.Duration)
	SetLockTimeout(time.Duration)
	SetRetryCount(int)
	SetLoglevel(int)

	Close()

	Count(string, int64) (int64, error)
	SetCounter(string, int64) (int64, error)

	Get(string) (Result, error)
	Set(string, []byte) (Result, error)
	Delete(string) (Result, error)

	GetLockManager(string, string) *LockManager
	Lock(string, []byte) (Result, error)
	Unlock(string) (Result, error)
	LockWithTimeout(string, []byte, time.Duration, time.Duration) (Result, error)

	GetWatcher(string) (*Watcher, error)

	NewTransaction() TransactionBuilder
}

type realClient struct {
	lkupcl    *lkup.LookupClient
	master_cl *rpc.Client

	lock_timeout    time.Duration
	lock_expiration time.Duration

	cluster_id string
	client_id  string

	max_retry        int
	reconnect_count  int
	reconnect_period time.Duration
}

func NewClient(cluster_id string) (Client, error) {
	return NewClientLK(cluster_id, "")
}

func NewClientLK(cluster_id, lkupsrv string) (Client, error) {
	cl := new(realClient)
	var err error

	hn, err := os.Hostname()

	cl.cluster_id = cluster_id
	cl.lock_timeout = 60 * time.Second
	cl.client_id = hn + "_" + getNiceId()
	cl.lkupcl, err = lkup.NewLookupClient(lkupsrv)
	cl.max_retry = 3
	// This avoids very frequent RPCs in times when no new master has been elected
	cl.reconnect_period = 2 * time.Second
	cl.lkupcl.SetLocalExpiration(0)

	if err != nil {
		return nil, err
	}

	err = cl.reconnect()

	if err != nil {
		return nil, err
	}

	return cl, nil
}

func (cl *realClient) Close() {
	cl.lkupcl.Close()
	cl.master_cl.Destroy()
}

// Set lock duration. If not called, value is 60 sec.
// It is recommended to set it not much higher; if the master crashes, the client
// is not notified until an RPC timeout occurs. In the worst case, an application
// is blocked for `d` without actually getting notified. That should not be a huge
// problem in general, though.
func (cl *realClient) SetLockTimeout(d time.Duration) {
	cl.lock_timeout = d
	cl.master_cl.SetTimeout(d, true)
}

// Set lock expiration. Default is 90s after obtaining.
func (cl *realClient) SetLockExpiration(d time.Duration) {
	cl.lock_expiration = d
}

// Unique client id, used both for locking and ownership determination.
func (cl *realClient) SetClientId(id string) {
	cl.client_id = id
}

// How many times requests are retried (default 3)
func (cl *realClient) SetRetryCount(c int) {
	cl.max_retry = c
}

// Reconnect to the given lookup server
func (cl *realClient) SetLkupSrv(host string) error {
	var err error
	cl.lkupcl, err = lkup.NewLookupClient(host)

	if err != nil {
		return err
	}
	return nil
}

func (cl *realClient) SetLoglevel(ll int) {
	log.SetLoglevel(ll)
}

// waits for cl.reconnect_period before trying again
func (cl *realClient) slow_reconnect() error {
	time.Sleep(cl.reconnect_period)
	return cl.reconnect()
}

func (cl *realClient) reconnect() error {

	conn, err := cl.connect(cl.cluster_id + constants.CLKUP_SUFFIX)

	if err != nil {
		return err
	}

	conn.SetTimeout(10*time.Second, true)

	if !conn.IsHealthy() {
		return errors.New("Not healthy")
	}

	if cl.master_cl != nil {
		cl.master_cl.Destroy()
	}

	conn.SetTimeout(10*time.Second+cl.lock_timeout, true)

	cl.master_cl = conn

	return nil
}

func newClient(name, host string, port uint, secmgr *smgr.ClientSecurityManager) (*rpc.Client, error) {
	ch, err := rpc.NewRpcChannel(secmgr)

	if err != nil {
		return nil, err
	}

	err = ch.Connect(rpc.Peer(host, port))

	if err != nil {
		return nil, err
	}

	cl := rpc.NewClient(name, ch)

	return &cl, nil
}

// Don't forget to return the connection to the cache after use.
// Expects a full clkup name.
func (cl *realClient) connect(target string) (*rpc.Client, error) {
	tasks, _, err := cl.lkupcl.LookupService(target, 1)

	if err != nil {
		return nil, err
	}

	hn, _ := os.Hostname()

	secmgr := smgr.NewClientSecurityManager()
	secmgr.SetServerPubkey(tasks[0].GetPublicKey())
	id := getNiceId()[0:5]

	conn, err := newClient("rftcl."+id+"@"+hn, tasks[0].GetTaskName(), uint(tasks[0].GetPort()), secmgr)

	if err != nil {
		return nil, err
	}

	conn.SetTimeout(cl.lock_timeout+10*time.Second, true)

	cl.reconnect_count++

	return conn, nil
}
