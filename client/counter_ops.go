package client

// Adds delta (may be negative) and returns the new counter value.
func (cl *realClient) Count(key string, delta int64) (int64, error) {
	rq := cl.NewTransaction().COUNT(key).Add(delta).Build()
	rp, err := cl.doClientRequest(rq)

	if err != nil || len(rp.GetResults()) == 0 {
		return 0, err
	}

	return rp.GetResults()[0].GetCounterResult(), err
}

// Set counter to specified value
func (cl *realClient) SetCounter(key string, value int64) (int64, error) {
	rq := cl.NewTransaction().COUNT(key).Set(value).Build()

	rp, err := cl.doClientRequest(rq)

	if err != nil || len(rp.GetResults()) == 0 {
		return 0, err
	}

	return rp.GetResults()[0].GetCounterResult(), err
}
