package client

import (
	"errors"
	"fmt"
)

/*
Implements a concurrent queue on top of RaftDB primitives.
Design: https://docs.google.com/document/d/1-VtCv94WxkF526F81KHui9Oyf0IRXc7NLyRMFjCy_mA/edit
*/

func entryId(name string, index int64) string {
	return fmt.Sprintf("%s_E%012d", name, index)
}

func rposId(name string) string {
	return name + "_RPOS"
}

func aposId(name string) string {
	return name + "_APOS"
}

type Queue struct {
	cl      Client
	max_len int
	name    string
}

func NewQueue(name string, cl Client) *Queue {
	return &Queue{cl: cl, name: name}
}

func NewBoundedQueue(name string, maxlen int, cl Client) *Queue {
	return &Queue{cl: cl, name: name, max_len: maxlen}
}

func (q *Queue) Append(entry []byte) error {
	if q.max_len > 0 {
		return q.boundedAppend(entry)
	} else {
		return q.append(entry)
	}
}

func (q *Queue) append(entry []byte) error {
	new_apos, err := q.cl.Count(aposId(q.name), 1)

	if err != nil {
		return err
	}

	_, err = q.cl.Set(entryId(q.name, new_apos-1), entry)

	return err
}

// the error is "FAILED" if someone else interfered. In that case, please retry
func (q *Queue) boundedAppend(entry []byte) error {
	results, err := q.cl.NewTransaction().GET(rposId(q.name)).GET(aposId(q.name)).Do()

	if err != nil {
		return err
	}

	rpos, apos := results[0], results[1]

	if apos.Counter()-rpos.Counter() >= int64(q.max_len) {
		return errors.New("Queue too long")
	}

	rposver, aposver := rpos.Ver(), apos.Ver()

	// Now set the entry

	results, err = q.cl.NewTransaction().
		CHECK_VERSION_EQ(rposId(q.name), rposver).
		CHECK_VERSION_EQ(aposId(q.name), aposver).
		COUNT(aposId(q.name)).
		SETB([]byte(entryId(q.name, apos.Counter())), entry).Do()

	if err != nil {
		return err
	}
	return nil
}

// The error reads "FAIL" if someone interfered with th operation.
// The data returned may be nil in case the entry at the position did not exist;
// this can occur if another writer has not yet written the entry.
func (q *Queue) Pop() ([]byte, error) {
	results, err := q.cl.NewTransaction().GET(rposId(q.name)).GET(aposId(q.name)).Do()

	if err != nil {
		return nil, err
	}

	rpos, apos := results[0], results[1]

	if apos.Counter()-rpos.Counter() < 1 {
		return nil, errors.New("No items left")
	}

	rposver, aposver := rpos.Ver(), apos.Ver()

	results, err = q.cl.NewTransaction().
		CHECK_VERSION_EQ(rposId(q.name), rposver).
		CHECK_VERSION_EQ(aposId(q.name), aposver).
		COUNT(rposId(q.name)).
		GET(entryId(q.name, rpos.Counter())).Do()

	if err != nil {
		return nil, err
	}

	return results[3].Data(), nil
}

// Change the position of the read cursor (R_POS) by delta.
// If delta is larger than what we could seek, seek as far as
// possible. E.g., if RPOS is 4 and delta is -10, RPOS will be 0.
func (q *Queue) Seek(delta int64) error {
	results, err := q.cl.NewTransaction().GET(rposId(q.name)).GET(aposId(q.name)).Do()

	if err != nil {
		return err
	}

	if delta < 0 {
		if results[0].Counter() > -delta {
			_, err = q.cl.Count(rposId(q.name), delta)
		} else {
			// Reset RPOS to 0.
			_, err = q.cl.SetCounter(rposId(q.name), 0)
		}
		return err
	} else if delta > 0 {
		// Legal seek
		if results[0].Counter()+delta < results[1].Counter() {
			_, err = q.cl.Count(rposId(q.name), delta)
		} else { // Set to one position before APOS
			_, err = q.cl.SetCounter(rposId(q.name), results[1].Counter()-1)
		}
		return err
	} else {
		return nil
	}
}
