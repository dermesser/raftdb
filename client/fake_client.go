package client

import (
	"errors"
	"fmt"
	"raftdb/proto"
	"time"

	pb "github.com/gogo/protobuf/proto"
)

// Implements an in-memory client that doesn't have any external dependencies.
// Can be used for unit tests. This module implements the Client, Result and TransactionBuilder interfaces.

// Does NOT implement the LockManager and Watcher types.
// Does not correctly implement transactions, especially not CHECK actions.

type entryType int

const (
	entry_undef entryType = iota
	entry_result_fail
	entry_result_emptyok
	entry_data
	entry_lock
	entry_counter
)

// database entry and what gets returned (implements Result) -- sometimes used only
// as Result without entry semantics. yeah yeah I know
type entry struct {
	t     entryType
	d     string
	c     int64
	owner string
	to    time.Time
	ver   uint64
}

func (e entry) Ok() bool          { return e.t != entry_result_fail }
func (e entry) CheckResult() bool { return true }
func (e entry) Data() []byte      { return []byte(e.d) }
func (e entry) Ver() uint64       { return e.ver }
func (e entry) Counter() int64    { return e.c }
func (e entry) Owner() string     { return e.owner }
func (e entry) Error() string {
	switch e.t {
	case entry_result_emptyok:
		return "SUCCESS"
	case entry_result_fail:
		return "ERROR"
	default:
		return "SUCCESS"
	}
}
func (e entry) String() string {
	return fmt.Sprintf("st=%s,r=%s,ix=%d,cnt=%d,o=%s", "SUCCESS", e.Data(), e.Ver(), e.Counter(), e.Owner())
}

type fakeClient struct {
	data map[string]entry
	exp  time.Duration
	cid  string
	ver  uint64
}

func NewFakeClient() Client {
	return &fakeClient{data: make(map[string]entry)}
}

func (c *fakeClient) Close()                            { c.data = nil }
func (c *fakeClient) SetClientId(s string)              { c.cid = s }
func (c *fakeClient) SetLkupSrv(string) error           { return nil }
func (c *fakeClient) SetLockExpiration(d time.Duration) { c.exp = d }
func (c *fakeClient) SetLockTimeout(time.Duration)      {}
func (c *fakeClient) SetRetryCount(int)                 {}
func (c *fakeClient) SetLoglevel(int)                   {}

func (c *fakeClient) Count(s string, d int64) (int64, error) {
	c.ver++
	c.data[s] = entry{t: entry_counter, c: c.data[s].c + d, ver: c.ver}
	return c.data[s].c, nil
}

func (c *fakeClient) SetCounter(s string, v int64) (int64, error) {
	c.ver++
	c.data[s] = entry{t: entry_counter, c: v, ver: c.ver}
	return v, nil
}

func (c *fakeClient) Delete(s string) (Result, error) {
	delete(c.data, s)
	c.ver++
	return entry{t: entry_result_emptyok}, nil
}

func (c *fakeClient) Set(k string, v []byte) (Result, error) {
	c.ver++
	c.data[k] = entry{t: entry_data, d: string(v), owner: c.cid, ver: c.ver}
	return entry{t: entry_result_emptyok, ver: c.ver, owner: c.cid}, nil
}

func (c *fakeClient) Get(k string) (Result, error) {
	if res, ok := c.data[k]; ok {
		return res, nil
	}
	return entry{t: entry_result_fail}, nil
}

// Not implemented.
func (c *fakeClient) GetLockManager(string, string) *LockManager { return nil }

func (c *fakeClient) Lock(k string, v []byte) (Result, error) {
	if (c.data[k].t == entry_lock || c.data[k].t == entry_undef) && (c.cid == c.data[k].owner || c.data[k].to.Before(time.Now())) {
		c.ver++
		c.data[k] = entry{t: entry_lock, d: string(v), owner: c.cid, to: time.Now().Add(c.exp), ver: c.ver}
		return entry{t: entry_result_emptyok, owner: c.cid, ver: c.ver}, nil
	} else {
		return entry{t: entry_result_fail, owner: c.data[k].owner}, nil
	}
}

// Doesn't hang, unlike the original.
func (c *fakeClient) LockWithTimeout(k string, v []byte, timeout time.Duration, expiration time.Duration) (Result, error) {
	if (c.data[k].t == entry_lock || c.data[k].t == entry_undef) &&
		(c.cid == c.data[k].owner || c.data[k].owner == "" || c.data[k].to.Before(time.Now())) {

		c.ver++
		c.data[k] = entry{t: entry_lock, d: string(v), owner: c.cid, to: time.Now().Add(expiration), ver: c.ver}

		return entry{t: entry_result_emptyok, owner: c.cid, ver: c.ver}, nil
	} else {
		return entry{t: entry_result_fail, owner: c.data[k].owner}, nil
	}
}

func (c *fakeClient) Unlock(k string) (Result, error) {
	if (c.data[k].t == entry_lock || c.data[k].t == entry_undef) &&
		(c.cid == c.data[k].owner || c.data[k].owner == "" || c.data[k].to.Before(time.Now())) {

		c.ver++
		c.Delete(k)
		return entry{t: entry_result_emptyok, ver: c.ver}, nil
	} else {
		return entry{t: entry_result_fail, owner: c.data[k].owner}, nil
	}
}

func (c *fakeClient) GetWatcher(string) (*Watcher, error) { return nil, nil }

// This type does not properly implement CHECK_ actions!!
type fakeTransactionBuilder struct {
	cl             *fakeClient
	strategy       proto.ClientRequest_Strategy
	actions        []*proto.Action
	current_action *proto.Action
}

func (cl *fakeClient) NewTransaction() TransactionBuilder {
	return &fakeTransactionBuilder{current_action: new(proto.Action),
		actions:  make([]*proto.Action, 0, 3),
		strategy: proto.ClientRequest_TRANSACTION,
		cl:       cl}
}

func (tb *fakeTransactionBuilder) next() {
	if tb.current_action.GetType() != proto.Action_UNDEFINED {
		tb.actions = append(tb.actions, tb.current_action)
		tb.current_action = new(proto.Action)
	}
}

func (tb *fakeTransactionBuilder) Done() TransactionBuilder {
	tb.next()
	return tb
}

// Returns nil here
func (tb *fakeTransactionBuilder) Build() *proto.ClientRequest {
	return nil
}

func (tb *fakeTransactionBuilder) Do() ([]Result, error) {
	tb.next()
	results := make([]Result, len(tb.actions))

	for i := range results {
		results[i] = entry{t: entry_result_emptyok}
	}

	if tb.strategy == proto.ClientRequest_BATCH {
		for i, a := range tb.actions {
			if a.GetOnVersion() > 0 && a.GetOnVersion() < tb.cl.data[string(a.GetKey())].ver {
				results[i] = entry{t: entry_result_fail}
				continue
			} else {
				switch a.GetType() {
				case proto.Action_COUNT:
					if a.GetCounterOp().GetType() == proto.CounterOp_COUNT {
						tb.cl.Count(string(a.GetKey()), a.GetCounterOp().GetVal())
						results[i] = entry{t: entry_result_emptyok, c: tb.cl.data[string(a.GetKey())].c, ver: tb.cl.ver}
					} else {
						tb.cl.SetCounter(string(a.GetKey()), a.GetCounterOp().GetVal())
						results[i] = entry{t: entry_result_emptyok, c: tb.cl.data[string(a.GetKey())].c, ver: tb.cl.ver}
					}
				case proto.Action_DELETE:
					tb.cl.Delete(string(a.GetKey()))
					results[i] = entry{t: entry_result_emptyok, ver: tb.cl.ver}
				case proto.Action_GET:
					results[i], _ = tb.cl.Get(string(a.GetKey()))
				case proto.Action_LOCK:
					results[i], _ = tb.cl.Lock(string(a.GetKey()), a.GetValue())
				case proto.Action_SET:
					results[i], _ = tb.cl.Set(string(a.GetKey()), a.GetValue())
				case proto.Action_UNLOCK:
					results[i], _ = tb.cl.Unlock(string(a.GetKey()))
				}
			}
		}
	} else if tb.strategy == proto.ClientRequest_TRANSACTION {
		for i, a := range tb.actions {
			//fmt.Println(tb.cl.cid, a)
			// First check that all actions succeed
			if a.GetOnVersion() > 0 && a.GetOnVersion() < tb.cl.data[string(a.GetKey())].ver {
				results[i] = entry{t: entry_result_fail, ver: tb.cl.data[string(a.GetKey())].ver}
				return results, errors.New("FAILED")
			} else {
				switch a.GetType() {
				case proto.Action_LOCK, proto.Action_UNLOCK:
					if (tb.cl.data[string(a.GetKey())].t == entry_lock || tb.cl.data[string(a.GetKey())].t == entry_undef) &&
						(tb.cl.cid == tb.cl.data[string(a.GetKey())].owner || tb.cl.data[string(a.GetKey())].owner == "" || tb.cl.data[string(a.GetKey())].to.Before(time.Now())) {
						//
					} else if tb.cl.data[string(a.GetKey())].t == entry_lock {
						results[i] = entry{t: entry_result_fail, owner: tb.cl.data[string(a.GetKey())].owner}
						return results, errors.New("FAILED")
					}
				default:
					//
				}

			}
		}
		// If we've come here all actions should have succeeded. Apply them
		for i, a := range tb.actions {
			switch a.GetType() {
			case proto.Action_COUNT:
				if a.GetCounterOp().GetType() == proto.CounterOp_COUNT {
					tb.cl.Count(string(a.GetKey()), a.GetCounterOp().GetVal())
					results[i] = entry{t: entry_result_emptyok, c: tb.cl.data[string(a.GetKey())].c, ver: tb.cl.ver}
				} else {
					tb.cl.SetCounter(string(a.GetKey()), a.GetCounterOp().GetVal())
					results[i] = entry{t: entry_result_emptyok, c: tb.cl.data[string(a.GetKey())].c, ver: tb.cl.ver}
				}
			case proto.Action_DELETE:
				tb.cl.Delete(string(a.GetKey()))
				results[i] = entry{t: entry_result_emptyok, ver: tb.cl.ver}
			case proto.Action_GET:
				results[i], _ = tb.cl.Get(string(a.GetKey()))
			case proto.Action_LOCK:
				results[i], _ = tb.cl.Lock(string(a.GetKey()), a.GetValue())
			case proto.Action_SET:
				results[i], _ = tb.cl.Set(string(a.GetKey()), a.GetValue())
			case proto.Action_UNLOCK:
				results[i], _ = tb.cl.Unlock(string(a.GetKey()))
			}
		}
	}
	return results, nil
}

func (tb *fakeTransactionBuilder) SetBatch() TransactionBuilder {
	tb.strategy = proto.ClientRequest_BATCH
	return tb
}

func (tb *fakeTransactionBuilder) SetTransaction() TransactionBuilder {
	tb.strategy = proto.ClientRequest_TRANSACTION
	return tb
}

// Finalizes itself
func (tb *fakeTransactionBuilder) GET(key string) TransactionBuilder {
	tb.next()
	tb.current_action.Type = proto.Action_GET.Enum()
	tb.current_action.Key = []byte(key)
	tb.next()
	return tb
}

// Use .On(), .Expire() to set details.
// Finalizes previous actions.
func (tb *fakeTransactionBuilder) SET(key, value string) TransactionBuilder {
	return tb.SETB([]byte(key), []byte(value))
}

// Use .On(), .Expire() to set details.
// Finalizes previous actions.
func (tb *fakeTransactionBuilder) SETB(key, value []byte) TransactionBuilder {
	tb.next()
	tb.current_action.Type = proto.Action_SET.Enum()
	tb.current_action.Key = key
	tb.current_action.Value = value
	return tb
}

// Use .On(), .Expire() to set details.
// Finalizes previous actions.
func (tb *fakeTransactionBuilder) DELETE(key string) TransactionBuilder {
	tb.next()
	tb.current_action.Type = proto.Action_DELETE.Enum()
	tb.current_action.Key = []byte(key)
	return tb
}

// The result of this returns true on CheckResult() if the entry equals value.
func (tb *fakeTransactionBuilder) CHECK_EQ(key, value string) TransactionBuilder {
	tb.next()
	tb.current_action.Type = proto.Action_CHECK_EQ.Enum()
	tb.current_action.Key = []byte(key)
	tb.current_action.Value = []byte(value)
	return tb
}

// The result of this returns true on CheckResult() if the entry does not equal value.
func (tb *fakeTransactionBuilder) CHECK_COUNTER_EQ(key string, value int64) TransactionBuilder {
	tb.next()
	tb.current_action.Type = proto.Action_CHECK_COUNTER_EQ.Enum()
	tb.current_action.Key = []byte(key)
	tb.current_action.CounterOp = &proto.CounterOp{Val: pb.Int64(value)}
	return tb
}

// The result of this returns true on CheckResult() if the entry is at exactly `version`.
func (tb *fakeTransactionBuilder) CHECK_VERSION_EQ(key string, version uint64) TransactionBuilder {
	tb.next()
	tb.current_action.Type = proto.Action_CHECK_VERSION_EQ.Enum()
	tb.current_action.Key = []byte(key)
	tb.current_action.OnVersion = pb.Uint64(version)
	return tb
}

// The result of this returns true on CheckResult() if the entry is at `version` or any later version.
func (tb *fakeTransactionBuilder) CHECK_VERSION_LE(key string, version uint64) TransactionBuilder {
	tb.next()
	tb.current_action.Type = proto.Action_CHECK_VERSION_LE.Enum()
	tb.current_action.Key = []byte(key)
	tb.current_action.OnVersion = pb.Uint64(version)
	return tb
}

// Use .On(), .Expire() to set details.
// Finalizes previous actions.
func (tb *fakeTransactionBuilder) LOCK(key, value string) TransactionBuilder {
	tb.next()
	tb.current_action.Type = proto.Action_LOCK.Enum()
	tb.current_action.Key = []byte(key)
	tb.current_action.Value = []byte(value)
	return tb
}

// Use .On(), .Expire() to set details.
// Finalizes previous actions.
func (tb *fakeTransactionBuilder) UNLOCK(key string) TransactionBuilder {
	tb.next()
	tb.current_action.Type = proto.Action_UNLOCK.Enum()
	tb.current_action.Key = []byte(key)
	return tb
}

// By default adds 1 to the counter. Use .Add(), .Sub() and .Set() to change the behavior.
// Finalizes previous actions.
func (tb *fakeTransactionBuilder) COUNT(key string) TransactionBuilder {
	tb.next()
	tb.current_action.Type = proto.Action_COUNT.Enum()
	tb.current_action.Key = []byte(key)
	tb.current_action.CounterOp = &proto.CounterOp{Type: proto.CounterOp_COUNT.Enum(), Val: pb.Int64(1)}
	return tb
}

// Sets the interval by which to count in a count operation.
func (tb *fakeTransactionBuilder) Add(delta int64) TransactionBuilder {
	if tb.current_action.Key == nil {
		return tb
	}
	tb.current_action.Type = proto.Action_COUNT.Enum()
	tb.current_action.CounterOp = &proto.CounterOp{Type: proto.CounterOp_COUNT.Enum(), Val: pb.Int64(delta)}
	return tb
}

// Sets the interval by which to count in a count operation.
func (tb *fakeTransactionBuilder) Sub(delta int64) TransactionBuilder {
	if tb.current_action.Key == nil {
		return tb
	}
	tb.current_action.Type = proto.Action_COUNT.Enum()
	tb.current_action.CounterOp = &proto.CounterOp{Type: proto.CounterOp_COUNT.Enum(), Val: pb.Int64(-delta)}
	return tb
}

// Sets the absolute value to set in a count operation.
func (tb *fakeTransactionBuilder) Set(value int64) TransactionBuilder {
	if tb.current_action.Key == nil {
		return tb
	}
	tb.current_action.Type = proto.Action_COUNT.Enum()
	tb.current_action.CounterOp = &proto.CounterOp{Type: proto.CounterOp_SET.Enum(), Val: pb.Int64(value)}
	return tb
}

// Set the maximum version to accept for the entry to operate on.
// e.g. if you set .On(100), but the entry has a current version of 101,
// the operation will fail (and the complete transaction as well,
// if strategy is TRANSACTION)
func (tb *fakeTransactionBuilder) On(max_version uint64) TransactionBuilder {
	tb.current_action.OnVersion = pb.Uint64(max_version)
	return tb
}

// Set the expiration of the entry
func (tb *fakeTransactionBuilder) Until(d time.Duration) TransactionBuilder {
	tb.current_action.Expiration = pb.Uint64(uint64(d.Nanoseconds()))
	return tb
}
