package client

// Convenience object to simplify using raftdb counters.
type Counter struct {
	cl  *realClient
	key string
}

func (cl *realClient) GetCounter(key string) *Counter {
	return &Counter{cl: cl, key: key}
}

// Get current counter value
func (cnt *Counter) GetValue() (int64, error) {
	return cnt.cl.Count(cnt.key, 0)
}

// Increments counter by one and returns the new value
func (cnt *Counter) Inc() (int64, error) {
	return cnt.cl.Count(cnt.key, 1)
}

// Decrements counter by one and returns the new value
func (cnt *Counter) Dec() (int64, error) {
	return cnt.cl.Count(cnt.key, -1)
}

// Adds delta to the counter and returns the new value
func (cnt *Counter) Add(delta int64) (int64, error) {
	return cnt.cl.Count(cnt.key, delta)
}

// Resets the counter to the given value, returns that value
func (cnt *Counter) Set(val int64) (int64, error) {
	return cnt.cl.SetCounter(cnt.key, val)
}
