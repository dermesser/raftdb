package client

import (
	"raftdb/proto"
	"time"

	pb "github.com/gogo/protobuf/proto"
)

type TransactionBuilder interface {
	Build() *proto.ClientRequest
	Done() TransactionBuilder
	Do() ([]Result, error)
	SetBatch() TransactionBuilder
	SetTransaction() TransactionBuilder
	GET(string) TransactionBuilder
	SET(string, string) TransactionBuilder
	SETB([]byte, []byte) TransactionBuilder
	DELETE(string) TransactionBuilder
	LOCK(string, string) TransactionBuilder
	UNLOCK(string) TransactionBuilder
	COUNT(string) TransactionBuilder
	CHECK_EQ(string, string) TransactionBuilder
	CHECK_COUNTER_EQ(string, int64) TransactionBuilder
	CHECK_VERSION_EQ(string, uint64) TransactionBuilder
	CHECK_VERSION_LE(string, uint64) TransactionBuilder
	Add(int64) TransactionBuilder
	Sub(int64) TransactionBuilder
	Set(int64) TransactionBuilder
	On(uint64) TransactionBuilder
	Until(time.Duration) TransactionBuilder
}

type realTransactionBuilder struct {
	cl             *realClient
	strategy       proto.ClientRequest_Strategy
	actions        []*proto.Action
	current_action *proto.Action
}

func (cl *realClient) NewTransaction() TransactionBuilder {
	return &realTransactionBuilder{current_action: new(proto.Action),
		actions:  make([]*proto.Action, 0, 3),
		strategy: proto.ClientRequest_TRANSACTION,
		cl:       cl}
}

func (tb *realTransactionBuilder) next() {
	if tb.current_action.GetType() != proto.Action_UNDEFINED {
		tb.actions = append(tb.actions, tb.current_action)
		tb.current_action = new(proto.Action)
	}
}

func (tb *realTransactionBuilder) Done() TransactionBuilder {
	tb.next()
	return tb
}

func (tb *realTransactionBuilder) Build() *proto.ClientRequest {
	tb.next()
	return &proto.ClientRequest{Actions: tb.actions,
		ClientId:  pb.String(tb.cl.client_id),
		ClusterId: pb.String(tb.cl.cluster_id),
		Strategy:  tb.strategy.Enum()}
}

// The result vector is shorter or equal to the number of transactions.
// The ordering of transactions is preserved.
// error is "FAILED" if a transaction failed, "SOME_FAILED" if some
// actions in a batch failed, NO_CLUSTER if the cluster doesn't exist,
// any string for RPC errors (etc.) and nil on success.
func (tb *realTransactionBuilder) Do() ([]Result, error) {
	rq := tb.Build()

	rp, err := tb.cl.doClientRequest(rq)

	results := make([]Result, len(rp.GetResults()))

	for i := range rp.GetResults() {
		results[i] = (*actionResult)(rp.GetResults()[i])
	}

	return results, err
}

// In batch mode all actions are attempted. CHECK_ actions just return whether
// the condition was true or not.
func (tb *realTransactionBuilder) SetBatch() TransactionBuilder {
	tb.strategy = proto.ClientRequest_BATCH
	return tb
}

// In transaction mode either all actions succeed or none.
// You can use CHECK_ actions to guard on versions or contents. If
// any action fails, no action is applied.
// Compare and swap: tb.SetTransaction().CHECK_EQ("a", "b").SET("a", "c")
// -> results if successful: [ {SUCCESS, CheckOk: true}, {SUCCESS, CommitIx: 123} ], err=nil
// -> results if unsuccessful: [ {SUCCESS, CheckOk: false}, {UNDEFINED}], err="FAILED"
func (tb *realTransactionBuilder) SetTransaction() TransactionBuilder {
	tb.strategy = proto.ClientRequest_TRANSACTION
	return tb
}

// Finalizes itself
func (tb *realTransactionBuilder) GET(key string) TransactionBuilder {
	tb.next()
	tb.current_action.Type = proto.Action_GET.Enum()
	tb.current_action.Key = []byte(key)
	tb.next()
	return tb
}

// Use .On(), .Expire() to set details.
// Finalizes previous actions.
func (tb *realTransactionBuilder) SET(key, value string) TransactionBuilder {
	return tb.SETB([]byte(key), []byte(value))
}

// Use .On(), .Expire() to set details.
// Finalizes previous actions.
func (tb *realTransactionBuilder) SETB(key, value []byte) TransactionBuilder {
	tb.next()
	tb.current_action.Type = proto.Action_SET.Enum()
	tb.current_action.Key = key
	tb.current_action.Value = value
	return tb
}

// Use .On(), .Expire() to set details.
// Finalizes previous actions.
func (tb *realTransactionBuilder) DELETE(key string) TransactionBuilder {
	tb.next()
	tb.current_action.Type = proto.Action_DELETE.Enum()
	tb.current_action.Key = []byte(key)
	return tb
}

// The result of this returns true on CheckResult() if the entry equals value.
func (tb *realTransactionBuilder) CHECK_EQ(key, value string) TransactionBuilder {
	tb.next()
	tb.current_action.Type = proto.Action_CHECK_EQ.Enum()
	tb.current_action.Key = []byte(key)
	tb.current_action.Value = []byte(value)
	return tb
}

// The result of this returns true on CheckResult() if the entry does not equal value.
func (tb *realTransactionBuilder) CHECK_COUNTER_EQ(key string, value int64) TransactionBuilder {
	tb.next()
	tb.current_action.Type = proto.Action_CHECK_COUNTER_EQ.Enum()
	tb.current_action.Key = []byte(key)
	tb.current_action.CounterOp = &proto.CounterOp{Type: proto.CounterOp_CHECK.Enum(), Val: pb.Int64(value)}
	return tb
}

// The result of this returns true on CheckResult() if the entry is at exactly `version`.
func (tb *realTransactionBuilder) CHECK_VERSION_EQ(key string, version uint64) TransactionBuilder {
	tb.next()
	tb.current_action.Type = proto.Action_CHECK_VERSION_EQ.Enum()
	tb.current_action.Key = []byte(key)
	tb.current_action.OnVersion = pb.Uint64(version)
	return tb
}

// The result of this returns true on CheckResult() if the entry is at `version` or any earlier version.
func (tb *realTransactionBuilder) CHECK_VERSION_LE(key string, version uint64) TransactionBuilder {
	tb.next()
	tb.current_action.Type = proto.Action_CHECK_VERSION_LE.Enum()
	tb.current_action.Key = []byte(key)
	tb.current_action.OnVersion = pb.Uint64(version)
	return tb
}

// Use .On(), .Expire() to set details.
// Finalizes previous actions.
func (tb *realTransactionBuilder) LOCK(key, value string) TransactionBuilder {
	tb.next()
	tb.current_action.Type = proto.Action_LOCK.Enum()
	tb.current_action.Key = []byte(key)
	tb.current_action.Value = []byte(value)
	return tb
}

// Use .On(), .Expire() to set details.
// Finalizes previous actions.
func (tb *realTransactionBuilder) UNLOCK(key string) TransactionBuilder {
	tb.next()
	tb.current_action.Type = proto.Action_UNLOCK.Enum()
	tb.current_action.Key = []byte(key)
	return tb
}

// By default adds 1 to the counter. Use .Add(), .Sub() and .Set() to change the behavior.
// Finalizes previous actions.
func (tb *realTransactionBuilder) COUNT(key string) TransactionBuilder {
	tb.next()
	tb.current_action.Type = proto.Action_COUNT.Enum()
	tb.current_action.Key = []byte(key)
	tb.current_action.CounterOp = &proto.CounterOp{Type: proto.CounterOp_COUNT.Enum(), Val: pb.Int64(1)}
	return tb
}

// Sets the interval by which to count in a count operation.
func (tb *realTransactionBuilder) Add(delta int64) TransactionBuilder {
	if tb.current_action.Key == nil || tb.current_action.GetType() != proto.Action_COUNT {
		return tb
	}
	tb.current_action.CounterOp = &proto.CounterOp{Type: proto.CounterOp_COUNT.Enum(), Val: pb.Int64(delta)}
	return tb
}

// Sets the interval by which to count in a count operation.
func (tb *realTransactionBuilder) Sub(delta int64) TransactionBuilder {
	if tb.current_action.Key == nil || tb.current_action.GetType() != proto.Action_COUNT {
		return tb
	}
	tb.current_action.CounterOp = &proto.CounterOp{Type: proto.CounterOp_COUNT.Enum(), Val: pb.Int64(-delta)}
	return tb
}

// Sets the absolute value to set in a count operation.
func (tb *realTransactionBuilder) Set(value int64) TransactionBuilder {
	if tb.current_action.Key == nil || tb.current_action.GetType() != proto.Action_COUNT {
		return tb
	}
	tb.current_action.CounterOp = &proto.CounterOp{Type: proto.CounterOp_SET.Enum(), Val: pb.Int64(value)}
	return tb
}

// Set the maximum version to accept for the entry to operate on.
// e.g. if you set .On(100), but the entry has a current version of 101,
// the operation will fail (and the complete transaction as well,
// if strategy is TRANSACTION)
func (tb *realTransactionBuilder) On(max_version uint64) TransactionBuilder {
	tb.current_action.OnVersion = pb.Uint64(max_version)
	return tb
}

// Set the expiration of the entry
func (tb *realTransactionBuilder) Until(d time.Duration) TransactionBuilder {
	tb.current_action.Expiration = pb.Uint64(uint64(d.Nanoseconds()))
	return tb
}
