package client

import (
	"errors"
	"raftdb/proto"
	"time"

	pb "github.com/gogo/protobuf/proto"
)

func (cl *realClient) lock_internal(key string, value []byte, client_id string, expiration time.Duration, timeout time.Duration) (Result, error) {
	rq := new(proto.ClientHangingRequest)

	rq.Type = proto.ClientHangingRequest_LOCK.Enum()
	rq.LockAction = &proto.Action{Type: proto.Action_LOCK.Enum(), Key: []byte(key), Value: value, Expiration: pb.Uint64(uint64(expiration.Nanoseconds()))}
	rq.ClientId = pb.String(client_id)
	rq.ClusterId = pb.String(cl.cluster_id)
	rq.TimeoutNsec = pb.Uint64(uint64(timeout.Nanoseconds()))

	rp, err := cl.doHangingClientRequest(rq)

	if err != nil {
		return nil, err
	}

	switch rp.GetStatus() {
	case proto.ClientHangingResponse_ERROR, proto.ClientHangingResponse_NO_CLUSTER, proto.ClientHangingResponse_NOT_MASTER, proto.ClientHangingResponse_ERROR_NO_RESULTS, proto.ClientHangingResponse_TIMEOUT:
		return nil, errors.New(rp.GetStatus().String())
	case proto.ClientHangingResponse_SUCCESS:
	}

	switch rp.GetResult().GetStatus() {
	case proto.ActionResult_ERROR, proto.ActionResult_ERROR_LOCKED, proto.ActionResult_ERROR_WRONG_VERSION, proto.ActionResult_NOT_FOUND:
		return nil, errors.New(rp.GetResult().GetStatus().String())
	case proto.ActionResult_SUCCESS:
	}

	return (*actionResult)(rp.GetResult()), err
}

// error.Error() is "ERROR_LOCKED" if the lock could not be acquired
func (cl *realClient) Lock(key string, value []byte) (Result, error) {
	rp, err := cl.lock_internal(key, value, cl.client_id, cl.lock_expiration, cl.lock_timeout)
	return rp, err
}

func (cl *realClient) LockWithTimeout(key string, value []byte, timeout, expiration time.Duration) (Result, error) {
	rp, err := cl.lock_internal(key, value, cl.client_id, cl.lock_expiration, timeout)
	return rp, err
}

// error.Error() is "ERROR_LOCKED" if the lock could not be released.
// For other codes, see raftdb/proto/client.proto:ClientResponse
func (cl *realClient) Unlock(key string) (Result, error) {
	rq := cl.NewTransaction().UNLOCK(key).Build()

	rp, err := cl.doClientRequest(rq)

	if err != nil || len(rp.GetResults()) == 0 {
		return nil, err
	}

	return (*actionResult)(rp.GetResults()[0]), err
}
