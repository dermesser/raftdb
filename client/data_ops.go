package client

import "raftdb/proto"
import "fmt"

type Result interface {
	Ok() bool
	Error() string
	Data() []byte
	Ver() uint64
	Counter() int64
	Owner() string
	CheckResult() bool

	String() string
}

type actionResult proto.ActionResult

// Returns true if the action was successful. Returns false
// if a lock/unlock were not successful or the OnVersion
// constraint was not fulfilled.
func (r *actionResult) Ok() bool {
	return (*proto.ActionResult)(r).GetStatus() == proto.ActionResult_SUCCESS
}

// Returns the error as string.
func (r *actionResult) Error() string {
	return (*proto.ActionResult)(r).GetStatus().String()
}

func (r *actionResult) Data() []byte {
	return (*proto.ActionResult)(r).GetResult()
}

func (r *actionResult) Ver() uint64 {
	return (*proto.ActionResult)(r).GetCommitIx()
}

func (r *actionResult) Counter() int64 {
	return (*proto.ActionResult)(r).GetCounterResult()
}

func (r *actionResult) Owner() string {
	return (*proto.ActionResult)(r).GetHolder()
}

// Result of a CHECK_ action. Returns false for all other actions.
func (r *actionResult) CheckResult() bool {
	return (*proto.ActionResult)(r).GetCheckOk()
}

func (r *actionResult) String() string {
	r_ := (*proto.ActionResult)(r)
	return fmt.Sprintf("st=%s,r=%s,ix=%d,cnt=%d,o=%s", r_.GetStatus().String(), string(r_.GetResult()), r_.GetCommitIx(), r_.GetCounterResult(), r_.GetHolder())
}

func (cl *realClient) Get(key string) (Result, error) {
	rq := cl.NewTransaction().GET(key).Build()

	rp, err := cl.doClientRequest(rq)

	if err != nil || len(rp.GetResults()) == 0 {
		return nil, err
	}

	return (*actionResult)(rp.GetResults()[0]), err
}

func (cl *realClient) Set(key string, value []byte) (Result, error) {
	rq := cl.NewTransaction().SET(key, string(value)).Build()

	rp, err := cl.doClientRequest(rq)

	if err != nil || len(rp.GetResults()) == 0 {
		return nil, err
	}

	return (*actionResult)(rp.GetResults()[0]), err
}

func (cl *realClient) Delete(key string) (Result, error) {
	rq := cl.NewTransaction().DELETE(key).Build()

	rp, err := cl.doClientRequest(rq)

	if err != nil || len(rp.GetResults()) == 0 {
		return nil, err
	}

	return (*actionResult)(rp.GetResults()[0]), err
}
