package main

import (
	clcl "clusterlookup/client"
	rpcc "clusterrpc/client"
	"errors"
	"fmt"
	"raftdb/server/constants"
	"strings"
)

var lookup_client *clcl.LookupClient

type address struct {
	address, pubkey string
	port            uint
}

func connect_clusterlookup_client() error {
	if lookup_client != nil {
		lookup_client.Close()
	}

	var err error
	lookup_client, err = clcl.NewLookupClient(FLAG_lkupsrv)

	return err
}

func resolve_clusterlookup_name(name string) (address, error) {
	if lookup_client == nil {
		err := connect_clusterlookup_client()

		if err != nil {
			return address{}, err
		}
	}

	tasks, _, err := lookup_client.LookupService(name, 1)

	if err != nil {
		return address{}, err
	}

	if len(tasks) > 0 {
		return address{address: tasks[0].GetTaskName(), pubkey: tasks[0].GetPublicKey(), port: uint(tasks[0].GetPort())}, nil
	}

	return address{}, errors.New("no tasks")
}

func resolve_name(name string) (address, error) {
	if strings.HasSuffix(name, constants.CLKUP_MEMBER_SUFFIX) ||
		strings.HasSuffix(name, constants.CLKUP_SUFFIX) {
		// Resolve by means of clusterlookup

		return resolve_clusterlookup_name(name)
	}

	return address{}, errors.New("Unknown pattern")
}

func connectTo(server, name string) (*rpcc.Client, error) {
	addr, err := resolve_name(server)

	if err != nil {
		fmt.Println("Couldn't resolve address of server:", err.Error())
		return nil, err
	}

	SEC_MGR.SetServerPubkey(addr.pubkey)
	ch, err := rpcc.NewRpcChannel(SEC_MGR)

	if err != nil {
		fmt.Println("Could not open channel:", err.Error())
		return nil, err
	}

	err = ch.Connect(rpcc.Peer(addr.address, addr.port))

	if err != nil {
		fmt.Println("Could not connect to server:", err.Error())
		return nil, err
	}

	cl := rpcc.NewClient("rdbcl."+name, ch)
	return &cl, nil
}
