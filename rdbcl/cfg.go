package main

import (
	"fmt"
	"os"
	"raftdb/proto"
	"raftdb/server/constants"
	"time"

	pb "github.com/gogo/protobuf/proto"
)

func initcfg(filename string) {

	exampleproto := new(proto.Configuration)
	exampleproto.ConfigVersion = pb.Uint64(1)
	exampleproto.Members = []*proto.Member{new(proto.Member), new(proto.Member), new(proto.Member)}
	exampleproto.GetMembers()[0].Id = pb.String("<clusterlookup raft member name #1>")
	exampleproto.GetMembers()[1].Id = pb.String("<clusterlookup raft member name #2>")
	exampleproto.GetMembers()[2].Id = pb.String("<clusterlookup raft member name #3>")

	f, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE, 0644)

	if err != nil {
		fmt.Println("Could not open file", filename, "for writing:", err.Error())
		return
	}

	err = pb.MarshalText(f, exampleproto)

	if err != nil {
		fmt.Println("Could not write example config to file:", err.Error())
		return
	}

	fmt.Println("Wrote empty config to file", filename)
}

func make_initial_config_request(cfg *proto.Configuration) *proto.ConfigRequest {
	cfgrq := new(proto.ConfigRequest)
	cfgrq.Type = proto.ConfigRequest_ADD_MEMBER.Enum()
	cfgrq.Members = make([]string, len(cfg.GetMembers()))

	for i, m := range cfg.GetMembers() {
		cfgrq.GetMembers()[i] = m.GetId()
	}

	return cfgrq
}

func sendcfg(filename, cluster_id, server string) {
	f, err := os.OpenFile(filename, os.O_RDONLY, 0)

	if err != nil {
		fmt.Println("Could not open file", filename, "for reading:", err.Error())
		return
	}

	fi, err := f.Stat()

	if err != nil {
		fmt.Println("Could not stat file:", err.Error())
		return
	}

	buf := make([]byte, fi.Size())

	n, err := f.Read(buf)

	if err != nil || int64(n) < fi.Size() {
		fmt.Println("Error or could not read all bytes:", err, n)
		return
	}

	cfg := new(proto.Configuration)

	err = pb.UnmarshalText(string(buf), cfg)

	if err != nil {
		fmt.Println("Could not unmarshal text proto:", err.Error())
		return
	}

	fmt.Println("Sending members to server...")
	for _, m := range cfg.GetMembers() {
		fmt.Println("Member with ID", m.GetId())
	}

	fmt.Print("Do you want to continue? y/N > ")
	n, err = os.Stdin.Read(buf)

	if (n < 2 || err != nil) || (buf[0] != 'y' && buf[1] != 'Y') {
		fmt.Println("Aborting...", n, string(buf))
		return
	}

	conn, err := connectTo(server, "sendcfg")

	if err != nil {
		return
	}

	conn.SetTimeout(2*time.Minute, true)

	cfgrq := make_initial_config_request(cfg)
	cfgrp := new(proto.ConfigResponse)
	cfgrq.ClusterId = pb.String(cluster_id)

	conn.RequestProtobuf(cfgrq, cfgrp, constants.RPC_SERVICE, "ConfigChange", nil)

	fmt.Println("Status:", cfgrp.GetStatus().String())
}

func getcfg(cluster_id string) {

	conn, err := connectTo(cluster_id, "getcfg")

	if err != nil {
		fmt.Println("Could not connect to RPC server", err)
		return
	}

	conn.SetTimeout(2*time.Minute, true)

	cfgrq := &proto.ConfigRequest{Type: proto.ConfigRequest_GET_CONFIG.Enum(), ClusterId: pb.String(cluster_id)}
	cfgrp := new(proto.ConfigResponse)

	err = conn.RequestProtobuf(cfgrq, cfgrp, constants.RPC_SERVICE, "ConfigChange", nil)
	conn.Destroy()

	if err != nil {
		fmt.Println("Request failed:", err)
		return
	}

	fmt.Println("Status:", cfgrp.GetStatus().String())
	pb.MarshalText(os.Stdout, cfgrp.GetConfig())
}

func rmmember(cluster_id, member_id string) {
	cfgrq, cfgrp := new(proto.ConfigRequest), new(proto.ConfigResponse)

	cfgrq.Type = proto.ConfigRequest_REMOVE_MEMBER.Enum()
	cfgrq.ClusterId = pb.String(cluster_id)
	cfgrq.Members = []string{member_id}

	cl, err := connectTo(cluster_id+constants.CLKUP_SUFFIX, "rmmember")

	if err != nil {
		fmt.Println("Could not connect to cluster leader:", err)
		return
	}

	err = cl.RequestProtobuf(cfgrq, cfgrp, constants.RPC_SERVICE, "ConfigChange", nil)
	cl.Destroy()

	if err != nil {
		fmt.Println("Request to leader failed:", err)
		return
	}

	if cfgrp.GetStatus() != proto.ConfigResponse_OK {
		fmt.Println("Leader returned error:", cfgrp.GetStatus().String())
		return
	}

	// Member is now unregistered from the cluster. Shut it down now
	sdrq, sdrp := new(proto.ConfigRequest), new(proto.ConfigResponse)

	sdrq.Type = proto.ConfigRequest_SHUTDOWN.Enum()
	sdrq.ClusterId = pb.String(cluster_id)

	cl, err = connectTo(member_id+constants.CLKUP_MEMBER_SUFFIX, "rmmember.sd")

	if err != nil {
		fmt.Println("Could not connect to member:", err)
		return
	}

	err = cl.RequestProtobuf(sdrq, sdrp, constants.RPC_SERVICE, "ConfigChange", nil)
	cl.Destroy()

	if err != nil {
		fmt.Println("Shutdown request failed:", err)
		return
	}

	if sdrp.GetStatus() != proto.ConfigResponse_OK {
		fmt.Println("Member shutdown failed:", sdrp.GetStatus().String())
		return
	}
}

func addmember(cluster_id, member_id string) {
	cfgrq, cfgrp := new(proto.ConfigRequest), new(proto.ConfigResponse)

	cfgrq.Type = proto.ConfigRequest_ADD_MEMBER.Enum()
	cfgrq.ClusterId = pb.String(cluster_id)
	cfgrq.Members = []string{member_id}

	cl, err := connectTo(cluster_id+constants.CLKUP_SUFFIX, "addmember")

	if err != nil {
		fmt.Println("Could not connect to cluster leader:", err)
		return
	}

	err = cl.RequestProtobuf(cfgrq, cfgrp, constants.RPC_SERVICE, "ConfigChange", nil)
	cl.Destroy()

	if err != nil {
		fmt.Println("Request to leader failed:", err)
		return
	}

	if cfgrp.GetStatus() != proto.ConfigResponse_OK {
		fmt.Println("Leader returned error:", cfgrp.GetStatus().String())
		return
	} else {
		fmt.Println("OK")
	}
}
