package main

import (
	rpclog "clusterrpc/log"
	"clusterrpc/securitymanager"
	"flag"
	"fmt"
	"os"
	"raftdb/client"
	"strings"
	"time"
)

var SEC_MGR *securitymanager.ClientSecurityManager

var FLAG_timeout time.Duration
var FLAG_expiration time.Duration
var FLAG_lkupsrv, FLAG_clientid string

func printHelp() {
	fmt.Println(`Provides a command line interface to RaftDB clusters.

Supported commands:

initcfg <file> -- Writes an empty/sample cluster configuration to <file>
sendcfg <file> <cluster> <server> -- Bootstrap a new cluster: Sends a configuration from <file> to <server>.
	<server> is a clusterlookup name.

addmember <cluster> <membername> -- Adds the server with member id <membername> to the cluster <cluster>
rmmember <cluster> <membername> -- Removes the server with member id <membername> from the cluster <cluster>
	(sends config update to remaining cluster and shuts down that member afterwards)

set <cluster> <key> <value>
get <cluster> <key>
lock <cluster> <key> <value>
[lmdebug <cluster> <key> <value>] -- uses lockmanager to keep a lock.
unlock <cluster> <key>
count <cluster> <key> <delta> -- adds <delta> to the counter at <key>; <delta> can be positive or negative
watch <cluster> <key> -- watches a value indefinitely and displays events happening on that key (e.g., Locked, Set)

<cluster> always means a clusterid, i.e. testcluster1 but not testcluster1.RAFTDB
<membername> is a memberid, i.e. member1 but not member1.RAFTMEMBER
<server> is a clusterlookup id, i.e. member1.RAFTMEMBER but NOT member1! (if using the entry registered by raftdb instances at startup)
`)

}

func setUpSecurity() {
	SEC_MGR = securitymanager.NewClientSecurityManager()
}

func parseFlags() {
	flag.DurationVar(&FLAG_timeout, "timeout", 90*time.Second, "Timeout used for (hanging) lock calls")
	flag.DurationVar(&FLAG_expiration, "expiration", 60*time.Second, "Lock expiration duration")
	flag.StringVar(&FLAG_lkupsrv, "lkupsrv", "", "clusterlookup instance to use")
	flag.StringVar(&FLAG_clientid, "clientid", "rdbcl", "Client ID to use.")
	flag.Parse()
}

func main() {
	parseFlags()
	setUpSecurity()
	rpclog.SetLoglevel(rpclog.LOGLEVEL_DEBUG)

	if len(flag.Args()) < 1 {
		printHelp()
		fmt.Println("\n")
		repl()
	} else {
		cmd := flag.Args()[0]

		var args []string

		if len(flag.Args()) > 1 {
			args = flag.Args()[1:]
		} else {
			args = []string{}
		}

		dispatch(cmd, args)
	}
}

func getClient(cluster string) client.Client {
	cl, err := client.NewClientLK(cluster, FLAG_lkupsrv)

	if err != nil {
		fmt.Println(err.Error())
		return nil
	}

	cl.SetClientId(FLAG_clientid)
	cl.SetLockExpiration(FLAG_expiration)
	cl.SetLockTimeout(FLAG_timeout)
	return cl
}

func dispatch(cmd string, args []string) {
	switch cmd {
	case "initcfg":
		if len(args) < 1 {
			fmt.Println("Need 1 argument for initcfg: file")
			return
		}
		initcfg(args[0])
	case "sendcfg":
		if len(args) < 3 {
			fmt.Println("Need 3 arguments for sendcfg: file cluster server")
			return
		}
		sendcfg(args[0], args[1], args[2])
	case "getcfg":
		if len(args) < 1 {
			fmt.Println("Need 1 argument for getcfg: <clkup id> (e.g. testcluster.RAFTDB)")
			return
		}
		getcfg(args[0])
	case "rmmember":
		if len(args) < 2 {
			fmt.Println("Need 2 arguments for rmmember: cluster member")
			return
		}
		rmmember(args[0], args[1])
	case "addmember":
		if len(args) < 2 {
			fmt.Println("Need 2 arguments for addmember: cluster member")
			return
		}
		addmember(args[0], args[1])
	case "set":
		if len(args) < 3 {
			fmt.Println("Need 3 arguments for set: cluster key value")
			return
		}
		cl := getClient(args[0])
		if cl != nil {
			set(cl, args[1], args[2])
		}
	case "get":
		if len(args) < 2 {
			fmt.Println("Need 2 arguments for get: cluster key")
			return
		}
		cl := getClient(args[0])
		if cl != nil {
			get(cl, args[1])
		}
	case "lock":
		if len(args) < 3 {
			fmt.Println("Need 3 arguments for lock: cluster key value")
			return
		}
		cl := getClient(args[0])
		if cl != nil {
			lock(cl, args[1], args[2])
		}
	case "lmdebug":
		if len(args) < 3 {
			fmt.Println("Need 3 arguments for lmdebug: cluster key value")
			return
		}
		cl := getClient(args[0])
		if cl != nil {
			lockmanager_debug(cl, args[1], args[2])
		}
	case "unlock":
		if len(args) < 2 {
			fmt.Println("Need 2 arguments for unlock: cluster key")
			return
		}
		cl := getClient(args[0])
		if cl != nil {
			unlock(cl, args[1])
		}
	case "watch":
		if len(args) < 2 {
			fmt.Println("Need 2 arguments for watch: cluster key")
			return
		}
		cl := getClient(args[0])
		if cl != nil {
			watch(cl, args[1])
		}
	case "count":
		if len(args) < 3 {
			fmt.Println("Need 3 arguments for count: cluster key delta")
			return
		}
		cl := getClient(args[0])
		if cl != nil {
			count(cl, args[1], args[2])
		}
	default:
		fmt.Println("Unrecognized command", cmd)
		return
	}
}

func repl() {
	for {
		fmt.Print("> ")

		buf := make([]byte, 128)

		n, err := os.Stdin.Read(buf)

		if err != nil {
			fmt.Println(err.Error())
			return
		}

		tokens := strings.Split(strings.Trim(string(buf[0:n]), "\n"), " ")
		var args []string

		if len(tokens) < 1 {
			fmt.Println("Please use a command.")
		}

		if len(tokens) > 1 {
			args = tokens[1:]
		}

		dispatch(tokens[0], args)

	}
}
