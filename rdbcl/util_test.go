package main

import (
	"testing"
)

func TestResolve(t *testing.T) {
	addr, err := resolve_clusterlookup_name("rpib-1.RAFTMEMBER")

	if err != nil {
		t.Fatal("Lookup returned an error")
	}

	if addr.address != "rpib-1.borgac.net" {
		t.Error("Wrong name returned by resolve():", addr.address)
	}

	if len(addr.pubkey) != 40 {
		t.Error("Pubkey incorrect:", addr.pubkey)
	}
}
