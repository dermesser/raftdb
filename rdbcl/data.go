package main

import (
	"fmt"
	"raftdb/client"
	"strconv"
	"time"
)

func set(cl client.Client, key, val string) {
	rp, err := cl.Set(key, []byte(val))

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println("OK:", rp.String())
}

func count(cl client.Client, key, delta string) {
	idelta, err := strconv.Atoi(delta)

	if err != nil {
		fmt.Println(err)
		return
	}

	cnt, err := cl.Count(key, int64(idelta))

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println("Ok.", cnt)
}

func get(cl client.Client, key string) {
	rep, err := cl.Get(key)

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println("OK:", rep.String())
}

func lock(cl client.Client, key, value string) {
	rep, err := cl.Lock(key, []byte(value))

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println("OK:", rep.String())
}

func lockmanager_debug(cl client.Client, key, value string) {
	cl.SetLockExpiration(5 * time.Second)
	cl.SetLockTimeout(12 * time.Second)
	lm := cl.GetLockManager(key, value)

	acq := func(k string) {
		fmt.Println("Acquired lock on", k)
	}
	lost := func(k string) {
		fmt.Println("Lost lock on", k)
	}
	failed := func(k string) {
		fmt.Println("Locking failed on", k)
	}

	lm.OnAcquired(acq)
	lm.OnLost(lost)
	lm.OnFailed(failed)

	err := lm.Run()

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	//fmt.Println("OK:", rep.String())
}

func unlock(cl client.Client, key string) {
	rp, err := cl.Unlock(key)

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println("OK:", rp.String())
}

func watch(cl client.Client, key string) {
	w, err := cl.GetWatcher(key)
	defer w.Stop()

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	for {
		ev := w.Wait(90 * time.Second)

		if ev != "Timeout" {
			fmt.Println(key, ":", ev)
		}
	}
}
