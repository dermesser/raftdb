
syntax = "proto2";
package proto;

import "proto/raft.proto";

message CounterOp {
    enum Type {
        UNDEFINED = 0;
        SET = 1;
        COUNT = 2;
        CHECK = 3;
    }
    required Type type = 1;
    // Absolute value (SET) or delta (COUNT)
    optional int64 val = 2;
}

// A request that may be hanging, i.e. be in flight for a longer period of time
message ClientHangingRequest {
    enum Type {
        UNDEFINED = 0;
        LOCK = 1;
        WATCH = 2;
    }
    required Type type = 1;
    optional Action lock_action = 2;
    optional bytes watch_key = 3;
    required string cluster_id = 4;
    optional string client_id = 5;
    optional uint64 timeout_nsec = 6;
}

message ClientHangingResponse {
    enum Status {
        UNDEFINED = 0;
        SUCCESS = 1;
        ERROR = 2;
        NO_CLUSTER = 3;
        TIMEOUT = 4;
        NOT_MASTER = 5;
        ERROR_INTERNAL = 6;
        ERROR_NO_RESULTS = 7;
    }
    required Status status = 1;
    optional int32 event = 2;
    optional ActionResult result = 3;
}

message ClientRequest {
    repeated Action actions = 1;

    enum Strategy {
        UNDEFINED = 0;
        // Only apply all actions if none would fail. For example,
        // (LOCK A, SET B) only sets B if the lock on A succeeds.
        // Conversely, (LOCK A, SET B [123]) only locks A if B had an
        // index of 123 or lower and therefore could be set. (The
        // order of operations doesn't matter)/
        // Checks (CHECK_) actions don't do things, but if a check fails
        // the transaction is aborted as well.
        // SET/GET/DELETE actions fail if the OnVersion constraint is not fullfilled.
        // LOCK/UNLOCK actions fail if the lock is not held or could not be acquired.
        TRANSACTION = 1;
        // Apply all, even if some fail
        BATCH = 2;
    }

    optional Strategy strategy = 2;
    optional string client_id = 3;
    required string cluster_id = 4;
}

message ClientResponse {
    enum Status {
        UNDEFINED = 0;
        SUCCESS = 1;
        SOME_FAILED = 2;
        FAILED = 3;
        ERROR_NOT_MASTER = 4;
        NO_CLUSTER = 5;
        ERROR_INTERNAL = 6;
    }
    required Status status = 1;
    repeated ActionResult results = 2;
}

message Action {
    enum Type {
        UNDEFINED = 0;
        // These honor the OnVersion restriction:
        SET = 1;
        DELETE = 3;
        LOCK = 4;
        UNLOCK = 5;
        COUNT = 6;

        // These don't honor any restrictions:
        GET = 2;

        // CHECK_ actions only check certain things.
        // They don't change the database itself but
        // can be used in transactions as guards.
        // CHECK_ actions also always yield SUCCESS. The actual
        // result is in the check_ok field.
        CHECK_EQ = 7; // If value of Key is Value.
        CHECK_COUNTER_EQ = 8;
        CHECK_VERSION_EQ = 9; // If Key's version equals OnVersion
        CHECK_VERSION_LE = 10; // If Key's version is equal or lower than OnVersion
    }
    required Type type = 1;
    required bytes key = 2;
    optional bytes value = 3;

    // nsec/duration; when to expire the lock from locking
    optional uint64 expiration = 4;

    optional CounterOp counter_op = 5;

    // Let the request fail with ERROR_WRONG_VERSION if the
    // entry doesn't have this or any older version.
    optional uint64 on_version = 6;
}

message ActionResult {
    enum Status {
        UNDEFINED = 0;
        SUCCESS = 1;
        ERROR = 2;
        ERROR_LOCKED = 3;
        ERROR_WRONG_VERSION = 4;
        NOT_FOUND = 5;
    }
    required Status status = 1;
    optional uint64 commit_ix = 2;
    optional bytes result = 3;
    // On ERROR_LOCKED and GETs on locks
    optional string holder = 5;

    // If the entry is a counter, result is empty and this field has the current
    // value of the counter.
    optional int64 counter_result = 6;

    // If the action was a check: True if check was successful.
    optional bool check_ok = 7;
}

message ConfigRequest {
    enum Type {
        UNDEFINED = 0;
        // Sent to leader
        ADD_MEMBER = 1;
        // Sent to leader
        REMOVE_MEMBER = 2;
        SHUTDOWN = 3;
        GET_CONFIG = 4;
    }
    required Type type = 1;
    required string cluster_id = 2;
    repeated string members = 3;
}

message ConfigResponse {
    enum Status {
        UNDEFINED = 0;
        OK = 1;
        ERROR = 2;
        ERROR_NO_CLUSTERID = 3;
        ERROR_NOT_MASTER = 4;
    }
    required Status status = 1;
    optional Configuration config = 2;
}

