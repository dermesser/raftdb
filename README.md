# RaftDB

Simple-ish implementation of the Raft consensus algorithm
in order to have a strongly consistent and safe replicated
key-value database (nothing fancy).

Supports such diverse features as

    - Automatic master fail-over
    - Snapshot transmission on re-join
    - Dynamic adding and removing of cluster members
    - Transactions and batching
