package main

import (
	"log"
	"raftdb/client"
	"time"
)

func benchSet(cl client.Client) {
	log.Println("benchSet")

	tb := cl.NewTransaction().SET("abc", "def").SET("xyz", "123")

	N := 50

	before := time.Now()

	for i := 0; i < N; i++ {
		r, err := tb.Do()

		if err != nil {
			log.Println(err)
		}

		if len(r) != 2 {
			log.Println("Wrong len(results)")
		}
	}

	log.Println("Took", time.Now().Sub(before)/time.Duration(N), "per action")
}

func benchRead(cl client.Client) {
	log.Println("benchRead")

	tb := cl.NewTransaction().GET("abc").GET("xyz")

	N := 50

	before := time.Now()

	for i := 0; i < N; i++ {
		r, err := tb.Do()

		if err != nil {
			log.Println(err)
		}

		if len(r) != 2 {
			log.Println("Wrong len(results)")
		}
	}

	log.Println("Took", time.Now().Sub(before)/time.Duration(N), "per action")
}
