package main

import (
	"flag"
	"log"
	"raftdb/client"
	"time"
)

var FLAG_lkupsrv, FLAG_cluster string
var FLAG_mock bool

func parseFlags() {
	flag.StringVar(&FLAG_lkupsrv, "lkupsrv", "", "Lkupsrv to use")
	flag.StringVar(&FLAG_cluster, "cluster", "testcluster1", "RaftDB cluster to use")
	flag.BoolVar(&FLAG_mock, "mock", false, "Use a fakeClient (does not send RPCs")
	flag.Parse()
}

func getClient() client.Client {
	var cl client.Client
	var err error

	if FLAG_mock {
		cl = client.NewFakeClient()
	} else {
		cl, err = client.NewClientLK(FLAG_cluster, FLAG_lkupsrv)
		cl.SetLoglevel(0)
	}

	if err != nil {
		log.Fatal(err)
	}

	cl.SetLockTimeout(2 * time.Second)

	return cl
}

func doTests() {
	cl := getClient()

	testSimpleSet(cl)
	testCompareSwapOk(cl)
	testCompareSwapNotOk(cl)
	testVersionCheck(cl)
	testCounterCheck(cl)
	testQueue1(cl)

	cl.Close()

	cl = getClient()
	benchSet(cl)

	cl.Close()
	cl = getClient()

	benchRead(cl)
}

func main() {
	parseFlags()

	doTests()
}
