package main

import (
	"log"
	"raftdb/client"
)

func testSimpleSet(cl client.Client) {
	log.Println("SimpleSet")

	tb := cl.NewTransaction()

	tb.SET("abc", "def")

	log.Println(tb.Build())

	r, err := tb.Do()

	if err != nil {
		log.Fatal(err)
	}

	if len(r) != 1 {
		log.Fatal("Wrong result length")
	}
}

func testCompareSwapOk(cl client.Client) {
	log.Println("CompareSwapOk")

	tb := cl.NewTransaction()

	r, err := tb.SET("abc", "def").Do()

	if err != nil {
		log.Fatal(err)
	}

	if len(r) != 1 {
		log.Fatal("Wrong result length")
	}

	tb = cl.NewTransaction()

	r, err = tb.CHECK_EQ("abc", "def").SET("abc", "deg").Do()

	if err != nil {
		log.Fatal(err)
	}

	if len(r) != 2 {
		log.Println(r)
		log.Fatal("Wrong result length")
	}
}

func testCompareSwapNotOk(cl client.Client) {
	log.Println("CompareSwapNotOk")

	tb := cl.NewTransaction()

	r, err := tb.SET("abc", "def").Do()

	if err != nil {
		log.Fatal(err)
	}

	if len(r) != 1 {
		log.Fatal("Wrong result length")
	}

	tb = cl.NewTransaction()

	r, err = tb.CHECK_EQ("abc", "XXX").SET("abc", "deg").Do()

	if err == nil {
		log.Fatal("Transaction succeeded")
	}

	if len(r) != 2 {
		log.Println(r)
		log.Fatal("Wrong result length")
	}
}

func testVersionCheck(cl client.Client) {
	log.Println("VersionCheck")

	r, err := cl.NewTransaction().SET("abc", "def").Do()

	if err != nil {
		log.Fatal(err)
	}

	if len(r) != 1 {
		log.Fatal("Wrong result length")
	}

	version := r[0].Ver()

	r, err = cl.NewTransaction().CHECK_VERSION_EQ("abc", version).SET("xyz", "123").Do()

	if err != nil {
		log.Fatal(err)
	}

	if len(r) != 2 {
		log.Fatal("Wrong result length")
	}

	// will fail because we use an outdated version
	r, err = cl.NewTransaction().CHECK_VERSION_LE("abc", version-1).SET("xyz", "123").Do()

	if err == nil {
		log.Fatal("Transaction succeeded")
	}

	if len(r) != 2 {
		log.Fatal("Wrong result length")
	}
}

func testCounterCheck(cl client.Client) {
	log.Println("CounterCheck")

	r, err := cl.NewTransaction().COUNT("CNT_XYZ").Set(23).Do()

	if err != nil {
		log.Fatal(err)
	}

	if len(r) != 1 {
		log.Fatal("Wrong result length")
	}

	if !r[0].Ok() {
		log.Fatal("Not ok", r[0].Error())
	}

	r, err = cl.NewTransaction().CHECK_COUNTER_EQ("CNT_XYZ", 23).Do()

	if err != nil {
		log.Fatal(err)
	}

	if len(r) != 1 {
		log.Fatal("Wrong result length")
	}

	if !r[0].Ok() || !r[0].CheckResult() {
		log.Fatal("Not ok", r[0].Error())
	}
}

func testQueue1(cl client.Client) {
	log.Println("Queue 1")

	q := client.NewQueue("testq1", cl)

	err := q.Append([]byte("abc"))

	if err != nil {
		log.Fatal(err)
	}

	err = q.Append([]byte("def"))

	if err != nil {
		log.Fatal(err)
	}

	e1, err := q.Pop()

	if err != nil {
		log.Fatal(err)
	}

	e2, err := q.Pop()

	if err != nil {
		log.Fatal(err)
	}

	if string(e1) != "abc" || string(e2) != "def" {
		log.Fatal("Wrong entries:", string(e1), string(e2))
	}
}
