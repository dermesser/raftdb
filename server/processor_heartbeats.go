package server

import (
	"raftdb/proto"
	"time"

	pb "github.com/gogo/protobuf/proto"
)

// Runs in FOLLOWERs and triggers conversion to CANDIDATE
func (p *processor) timeOutWaiter() {
	for p.processor_state == state_FOLLOWER || p.processor_state == state_FOLLOWER_SNAPSHOTTING {
		raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "HEARTBEAT: Waiting...")

		timeout := p.timeout
		before := time.Now()

		t := time.NewTimer(timeout)
		select {
		case <-p.heartbeat_channel:
			raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "HEARTBEAT:", p.timeout-time.Now().Sub(before), "left")
			t.Stop()
			continue
		case <-t.C:
			// Check if we haven't become leader by now.
			if p.processor_state == state_FOLLOWER {
				if time.Now().Before(p.leader_lease) {
					// Wait for another timeout until we are out of the current lease.
					continue
				} else {
					raftdb_log(LOGLEVEL_INFO, "HEARTBEAT: Timed out, converting to CANDIDATE")
					p.convertCandidate()
					p.requests <- &RpcRequest{ignore: true}
				}
			} else if p.processor_state == state_FOLLOWER_SNAPSHOTTING {
				raftdb_log(LOGLEVEL_INFO, "HEARTBEAT: Timed out, but snapshotting. Waiting longer")
				continue
			}
			return
		}
	}
}

func (p *processor) heartBeatSender() {
	empty_ae := &proto.AppendEntriesRequest{LeaderId: pb.String(p.member_id),
		Entries: []*proto.Entry{}, ClusterId: pb.String(p.cluster.getClusterId())}

	for p.processor_state == state_LEADER {
		term := p.st.Term()
		nextix, commitix, _ := p.st.Index()
		previous_entry := p.st.GetLogEntry(nextix - 1)

		new_lease := time.Now().Add(FLAG_lease)

		empty_ae.PreviousLogVersion = previous_entry.GetEntry().Version

		empty_ae.Term = pb.Uint64(term)
		empty_ae.LeaderCommitIx = pb.Uint64(commitix)
		empty_ae.LeaderLease = pb.Uint64(uint64(new_lease.UnixNano()))

		raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "HEARTBEAT: Sending heartbeat to", len(p.cluster.getCoordinators()), "followers")

		external_majority := p.getExternalMajority()
		rps := p.cluster.sendToAll(&RpcRequest{ae: empty_ae}, external_majority)

		var valid_rps int
		for _, rp := range rps {
			if rp.err == nil && rp.ae.GetSuccess() {
				valid_rps++
			}
		}

		if valid_rps >= external_majority {
			p.leader_lease = new_lease
		}

		time.Sleep(FLAG_heartbeat_interval)
	}
}
