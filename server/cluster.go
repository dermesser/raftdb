package server

import (
	"errors"
	"raftdb/proto"
	"time"

	pb "github.com/gogo/protobuf/proto"
)

/*
* Manages a cluster's configuration and peers.
 */

type clustermember struct {
	name string
}

func (m *clustermember) toMember() *proto.Member {
	return &proto.Member{Id: pb.String(m.name)}
}

type clusterconfig struct {
	version uint64
	members map[string]*clustermember
}

func (c *clusterconfig) toConfig() *proto.Configuration {
	cfg := new(proto.Configuration)

	cfg.ConfigVersion = pb.Uint64(c.version)
	cfg.Members = make([]*proto.Member, len(c.members))

	var i int
	for _, m := range c.members {
		cfg.Members[i] = m.toMember()
		i++
	}

	return cfg
}

func (c *clusterconfig) fromConfig(cfg_buf []byte) error {
	cfg := new(proto.Configuration)

	err := cfg.Unmarshal(cfg_buf)

	if err != nil {
		return err
	}

	if cfg.GetConfigVersion() > c.version {
		c.version = cfg.GetConfigVersion()
		c.members = make(map[string]*clustermember)

		for _, m := range cfg.GetMembers() {
			c.members[m.GetId()] = &clustermember{name: m.GetId()}
		}

		return nil
	} else {
		return errors.New("Version too old")
	}
}

func (c *clusterconfig) apply(cr *proto.ConfigRequest) error {
	if c.members == nil {
		c.members = make(map[string]*clustermember)
	}
	if c.version == 0 {
		c.version = 1
	}

	if cr.GetType() == proto.ConfigRequest_ADD_MEMBER {
		for _, new_member := range cr.GetMembers() {
			c.members[new_member] = &clustermember{name: new_member}
		}
		c.version++
	} else if cr.GetType() == proto.ConfigRequest_REMOVE_MEMBER {
		for _, new_member := range cr.GetMembers() {
			delete(c.members, new_member)
		}
		c.version++
	} else if cr.GetType() == proto.ConfigRequest_GET_CONFIG {
		return nil
	}
	return nil
}

func (c *clusterconfig) getMembers() []string {
	if c.members == nil {
		return nil
	}

	ms := make([]string, len(c.members))

	var i int
	for n, _ := range c.members {
		ms[i] = n
		i++
	}
	return ms
}

type cluster struct {
	cluster_id string
	config     *clusterconfig
	// member id -> channel
	coordinators map[string]*internalMember
}

func newCluster(id string) *cluster {
	return &cluster{cluster_id: id, coordinators: make(map[string]*internalMember), config: new(clusterconfig)}
}

func (c *cluster) getClusterId() string {
	return c.cluster_id
}

func (c *cluster) getMember(name string) *internalMember {
	return c.coordinators[name]
}

func (c *cluster) getCoordinators() []*internalMember {
	ims := make([]*internalMember, len(c.coordinators))
	var i int

	for _, im := range c.coordinators {
		ims[i] = im
		i++
	}
	return ims
}

// Only add to coordinators; this is separate from the cluster configuration
func (c *cluster) addMember(name string, coord *internalMember) error {
	_, ok := c.coordinators[name]

	if ok {
		return errors.New("Coordinator is already running")
	}

	c.coordinators[name] = coord

	return nil
}

// Shuts down a coordinator and deletes it from the coordinators map
// Doesn't delete it from the configuration (of course)!
func (c *cluster) removeMember(name string) error {
	m, ok := c.coordinators[name]
	if ok {
		m.ch <- &RpcRequest{shut_down: true}
		delete(c.coordinators, name)
	}
	return nil
}

// Send a request to all members and wait for at least a majority.
// If not enough members responded until the timeout, the returned vector is shorter than majority.
func (c *cluster) sendToAll(rq *RpcRequest, majority int) []*RpcResponse {
	rps := make([]*RpcResponse, 0, majority)
	rpchan := make(chan *RpcResponse, len(c.config.getMembers()))

	for id, m := range c.coordinators {
		if rq.ae != nil {
			aerq := *rq.ae

			go func(m member, id string) {
				own_rq := aerq
				rp, err := m.AppendEntries(&own_rq)

				if err != nil {
					raftdb_log(LOGLEVEL_WARNINGS, c.cluster_id, "Returned", err.Error(), "on AE from", id)
				}

				raftdb_log(LOGLEVEL_DEBUG, c.cluster_id, "Received response on AE from", id, rp)
				rpchan <- &RpcResponse{ae: rp, err: err}
			}(m, id)
		} else if rq.rqv != nil {
			rqv := *rq.rqv
			go func(m member) {
				own_rq := rqv
				own_rq.MemberId = pb.String(id)
				rp, err := m.RequestVote(&own_rq)

				if err != nil {
					raftdb_log(LOGLEVEL_WARNINGS, c.cluster_id, "Received", err.Error(), "on RV from", id)
				}

				raftdb_log(LOGLEVEL_DEBUG, c.cluster_id, "Received response on RV from", id, rp)

				rpchan <- &RpcResponse{v: rp, err: err}
			}(m)
		}
	}

	// Collect minimum number of votes
	var i int
	t := time.NewTimer(FLAG_rpc_timeout)

	for i = 0; i < majority; i++ {
		select {
		case rp := <-rpchan:
			rps = append(rps, rp)
		case <-t.C:
			raftdb_log(LOGLEVEL_DEBUG, c.cluster_id, "Waiting for responses timed out after", FLAG_rpc_timeout)
			break
		}
	}

	return rps
}
