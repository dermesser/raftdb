package constants

const RAFTDB_VERSION = "0.2"

const RPC_SERVICE = "RAFTDB"
const CLKUP_SUFFIX = ".RAFTDB"
const CLKUP_MEMBER_SUFFIX = ".RAFTMEMBER"
const CONFIG_ENTRY_KEY = "__RAFTDB_CONFIG"

const ERR_NOT_LEADER = "Not leader"
const ERR_NOT_FOLLOWER = "Not follower"
const ERR_NO_MAJORITY = "No majority"
