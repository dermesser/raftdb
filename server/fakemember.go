package server

import (
	"raftdb/proto"

	pb "github.com/gogo/protobuf/proto"
)

// Simulates a peer's state and responds accordingly (for testing)
// This is actually processor logic.
type fakeMember struct {
	last_applied, commit_index, next_index uint64
	term                                   uint64
	log                                    []*proto.Entry
	voted_for                              string
	leader                                 string
	state                                  map[string][]byte
	config                                 *proto.Configuration
}

// Allow to initialize internal state. Set to 0 if not interested
func newFakeMember(last_applied, commit_index, next_index, term uint64) *fakeMember {
	if next_index == 0 {
		next_index = 1
	}

	m := &fakeMember{log: make([]*proto.Entry, 1, 16), state: make(map[string][]byte),
		last_applied: last_applied, commit_index: commit_index, next_index: next_index,
		term: term}

	m.log[0] = &proto.Entry{Type: proto.Entry_UNDEFINED.Enum(), Key: []byte{},
		Entry: &proto.DBEntry{Value: []byte{}, Version: &proto.VersionInfo{Term: pb.Uint64(1), LogIx: (pb.Uint64(0))}}}

	return m
}

func (m *fakeMember) AppendEntries(rq *proto.AppendEntriesRequest) (*proto.AppendEntriesResponse, error) {
	rp := new(proto.AppendEntriesResponse)

	if rq.GetTerm() < m.term {
		rp.Term = &m.term
		rp.Success = pb.Bool(false)
		return rp, nil
	}

	if rq.GetTerm() > m.term {
		m.term = rq.GetTerm()
	}

	m.leader = rq.GetLeaderId()

	// Heartbeat
	if len(rq.GetEntries()) == 0 {
		if rq.GetPreviousLogVersion().GetTerm() < m.term || rq.GetPreviousLogVersion().GetLogIx() != uint64(len(m.log)-1) {
			rp.Success = pb.Bool(false)
			rp.LastIndex = pb.Uint64(m.next_index - 1)
		} else {
			rp.Success = pb.Bool(true)
		}
		return rp, nil
	}

	// We do have one or more entries here because of above condition

	// Check internal integrity/invariants of parameters
	if rq.GetPreviousLogVersion().GetLogIx() != rq.GetEntries()[0].GetEntry().GetVersion().GetLogIx()-1 {
		rp.Success = pb.Bool(false)

		rp.LastIndex = pb.Uint64(m.next_index - 1)
		return rp, nil
	}

	if rq.GetPreviousLogVersion().GetLogIx() < m.next_index {
		previous_entry := m.log[rq.GetPreviousLogVersion().GetLogIx()]

		// Not existing yet (not supposed to happen because of the index check above)
		if previous_entry == nil {
			rp.LastIndex = pb.Uint64(m.next_index - 1)
			rp.Success = pb.Bool(false)
		} else if previous_entry.GetEntry().GetVersion().GetTerm() != rq.GetPreviousLogVersion().GetTerm() {
			// Find log index of first entry in conflicting term
			rp.Success = pb.Bool(false)
			conflicting_term := previous_entry.GetEntry().GetVersion().GetTerm()

			// Find first
			for i := previous_entry.GetEntry().GetVersion().GetLogIx(); i > 0; i-- {
				if m.log[i-1].GetEntry().GetVersion().GetTerm() != conflicting_term {
					rp.LastIndex = pb.Uint64(m.log[i].GetEntry().GetVersion().GetLogIx())
					break
				}
			}

			return rp, nil
		} else { // Everything's alright here
			// Truncate if we have entries that the leader doesn't have
			m.log = m.log[0 : rq.GetPreviousLogVersion().GetLogIx()+1]
			m.next_index = rq.GetPreviousLogVersion().GetLogIx() + 1

			m.log = append(m.log, rq.GetEntries()...)
			m.next_index += uint64(len(rq.GetEntries()))

			rp.Success = pb.Bool(true)
		}
	} else { // Our next_index is smaller than the previous log version indicated by the RPC
		rp.LastIndex = pb.Uint64(m.next_index - 1)
		rp.Success = pb.Bool(false)
	}

	// Apply entries until latest commit
	if rq.GetLeaderCommitIx() > m.commit_index {
		// Only go until our next_index so we don't apply unreceived log entries.
		for i := m.commit_index + 1; i < m.next_index; i++ {
			m.state[string(m.log[i].GetKey())] = m.log[i].GetEntry().GetValue()
		}

		m.commit_index = rq.GetLeaderCommitIx()
		m.last_applied = m.commit_index
	}

	// Check for config updates
	for _, e := range rq.GetEntries() {
		if e.GetEntry().GetType() == proto.DBEntry_CONFIG {
			cfg := new(proto.Configuration)
			err := cfg.Unmarshal(e.GetEntry().GetValue())

			if err == nil {
				m.config = cfg
			}
		}
	}

	return rp, nil
}

func (m *fakeMember) RequestVote(rq *proto.RequestVote) (*proto.Vote, error) {
	rp := new(proto.Vote)

	// Term is newer than outs && last log entry is at least newer than ours &&
	// term of the candidate's most recent log entry is the same as in the corresponding on our side
	if rq.GetTerm() > m.term && rq.GetLogIx().GetLogIx() >= m.next_index-1 {

		// If candidate's log is as long as ours, check if term is correct
		// If candidate's log is shorter -- denied anyway
		// If longer -- don't really care, term conflicts in between will be resolved by AppendEntries
		if rq.GetLogIx().GetLogIx() == m.next_index-1 &&
			rq.GetLogIx().GetTerm() != m.log[m.next_index-1].GetEntry().GetVersion().GetTerm() { // might be < instead of !=
			// Conflicting term in entry
			rp.Term = &m.term
			rp.VoteGranted = pb.Bool(false)
			rp.FailType = proto.Vote_WRONG_ENTRY_TERM.Enum()

			return rp, nil
		}

		if rq.GetConfigVersion() < m.config.GetConfigVersion() {
			rp.Term = &m.term
			rp.VoteGranted = pb.Bool(false)
			rp.FailType = proto.Vote_WRONG_CONFIG_VERSION.Enum()

			return rp, nil
		}

		m.term = rq.GetTerm()
		rp.Term = rq.Term
		rp.VoteGranted = pb.Bool(true)

		return rp, nil
	} else {
		rp.VoteGranted = pb.Bool(false)
		rp.Term = &m.term
		rp.FailType = proto.Vote_WRONG_TERM.Enum()
	}

	return rp, nil
}

func (m *fakeMember) InstallSnapshot(rq *proto.InstallSnapshot) (*proto.InstallSnapshotResponse, error) {
	rp := new(proto.InstallSnapshotResponse)

	if rq.GetTerm() < m.term {
		rp.Term = &m.term
		return rp, nil
	}

	for _, e := range rq.GetSnapshot() {
		m.state[string(e.GetKey())] = e.GetEntry().GetValue()
	}

	if rq.GetDone() {
		m.next_index = rq.GetLastIncluded().GetEntry().GetVersion().GetLogIx() + 1
		m.last_applied = rq.GetLastIncluded().GetEntry().GetVersion().GetLogIx()
		m.commit_index = m.last_applied

		// Create log entry for the last snapshot entry
		// because the next AppendEntries RPC will want to know this.
		new_log := make([]*proto.Entry, rq.GetLastIncluded().GetEntry().GetVersion().GetLogIx()+1)
		copy(m.log, new_log)
		m.log = new_log
		m.log[m.next_index-1] = rq.GetLastIncluded()
	}

	return rp, nil
}
