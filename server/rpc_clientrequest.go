package server

import (
	rpcs "clusterrpc/server"
	"raftdb/proto"
	"raftdb/server/locking"
	"time"

	pb "github.com/gogo/protobuf/proto"
)

func (srv *RpcServer) waitLockReleased(ctx *rpcs.Context, wtr *locking.Waiter, rq *proto.ClientHangingRequest, proc_rp *proto.ClientResponse) {

	if rq.GetTimeoutNsec() == 0 {
		ctx.Return(proc_rp)
		return
	}

	timeout := time.Now().Add(time.Duration(rq.GetTimeoutNsec()) * time.Nanosecond)

	var rp *proto.ClientHangingResponse = &proto.ClientHangingResponse{Status: proto.ClientHangingResponse_SUCCESS.Enum()}

OUTER:
	for timeout.After(time.Now()) {
		evt := wtr.Wait(timeout.Sub(time.Now()))

		raftdb_log(LOGLEVEL_DEBUG, "Received event", evt.String())

		switch evt {
		case locking.Timeout:
			rp.Status = proto.ClientHangingResponse_TIMEOUT.Enum()
			ctx.Return(rp)
			return
		case locking.Locked, locking.Set, locking.Renewed:
			continue OUTER
		case locking.Released, locking.ReleasedTimedOut, locking.Deleted:
			// lock now
			crq := &proto.ClientRequest{Strategy: proto.ClientRequest_TRANSACTION.Enum(),
				Actions: []*proto.Action{rq.GetLockAction()}, ClusterId: rq.ClusterId, ClientId: rq.ClientId}
			rp := srv.sendRequest(rq.GetClusterId(), &RpcRequest{cr: crq})

			if rp.err != nil {
				ctx.Fail(rp.err.Error())
				return
			} else if rp.cr != nil {
				if len(rp.cr.GetResults()) > 0 {
					if rp.cr.GetResults()[0].GetStatus() == proto.ActionResult_ERROR_LOCKED {
						if timeout.After(time.Now()) {
							continue OUTER
						} else {
							ctx.Return(&proto.ClientHangingResponse{Status: proto.ClientHangingResponse_TIMEOUT.Enum()})
							return
						}
					} else {
						ctx.Return(&proto.ClientHangingResponse{Status: proto.ClientHangingResponse_SUCCESS.Enum(),
							Result: rp.cr.GetResults()[0]})
						return
					}
				} else {
					ctx.Return(&proto.ClientHangingResponse{Status: proto.ClientHangingResponse_ERROR_NO_RESULTS.Enum()})
					return
				}
			}
		case locking.MasterStepDown:
			rp = &proto.ClientHangingResponse{Status: proto.ClientHangingResponse_NOT_MASTER.Enum()}
			ctx.Return(rp)
			return
		default:
			raftdb_log(LOGLEVEL_WARNINGS, "Unhandled event on lock:", evt.String())
		}
	}
	ctx.Return(proc_rp)
}

func (srv *RpcServer) watch(ctx *rpcs.Context, wtr *locking.Waiter, rq *proto.ClientHangingRequest) {
	if rq.GetTimeoutNsec() == 0 {
		return
	}

	timeout := time.Duration(rq.GetTimeoutNsec()) * time.Nanosecond

	rp := &proto.ClientHangingResponse{Status: proto.ClientHangingResponse_SUCCESS.Enum()}
	rp.Event = pb.Int32(int32(wtr.Wait(timeout)))

	ctx.Return(rp)
}

func (srv *RpcServer) ClientRequestHandler(ctx *rpcs.Context) {
	rq := new(proto.ClientRequest)

	if nil != ctx.GetArgument(rq) {
		ctx.Fail("Could not unmarshal argument")
		return
	}

	MONVARS.AddMap("requests", "ClientRequest", 1)

	srv.mx.Lock()
	_, ok := srv.processors[rq.GetClusterId()]
	srv.mx.Unlock()

	if !ok {
		ctx.Return(&proto.ClientResponse{Status: proto.ClientResponse_NO_CLUSTER.Enum()})
		return
	}

	rp := srv.sendRequest(rq.GetClusterId(), &RpcRequest{cr: rq})

	if rp.err != nil {
		rp_ := &proto.ClientResponse{Status: proto.ClientResponse_ERROR_INTERNAL.Enum()}
		ctx.Return(rp_)
		return
	} else if rp.cr != nil {
		ctx.Return(rp.cr)
	}
}

func (srv *RpcServer) ClientHangingRequestHandler(ctx *rpcs.Context) {
	rq := new(proto.ClientHangingRequest)

	if nil != ctx.GetArgument(rq) {
		ctx.Fail("Could not unmarshal argument")
		return
	}

	MONVARS.AddMap("requests", "ClientHangingRequest", 1)
	MONVARS.Add("hanging_requests", 1)
	defer MONVARS.Add("hanging_requests", -1)

	srv.mx.Lock()
	proc, ok := srv.processors[rq.GetClusterId()]
	srv.mx.Unlock()

	if !ok {
		ctx.Return(&proto.ClientHangingResponse{Status: proto.ClientHangingResponse_NO_CLUSTER.Enum()})
		return
	}

	// Get waiter before making requests so there's no gap between
	// response from processor and starting to wait on a notification

	if rq.GetType() == proto.ClientHangingRequest_WATCH {
		wtr := proc.nmgr.Waiter(string(rq.GetWatchKey()))
		defer proc.nmgr.Unwait(wtr)

		srv.watch(ctx, wtr, rq)
	} else if rq.GetType() == proto.ClientHangingRequest_LOCK {
		wtr := proc.nmgr.Waiter(string(rq.GetLockAction().GetKey()))
		defer proc.nmgr.Unwait(wtr)

		crq := &proto.ClientRequest{ClusterId: rq.ClusterId, ClientId: rq.ClientId, Strategy: proto.ClientRequest_TRANSACTION.Enum(),
			Actions: []*proto.Action{rq.LockAction}}
		rp := srv.sendRequest(rq.GetClusterId(), &RpcRequest{cr: crq})

		if rp.err != nil {
			rp_ := &proto.ClientHangingResponse{Status: proto.ClientHangingResponse_ERROR_INTERNAL.Enum()}
			ctx.Return(rp_)
			return
		} else if rp.cr != nil {
			if len(rp.cr.GetResults()) > 0 {
				if rp.cr.GetResults()[0].GetStatus() == proto.ActionResult_ERROR_LOCKED {
					srv.waitLockReleased(ctx, wtr, rq, rp.cr)
				} else {
					rp_ := &proto.ClientHangingResponse{Status: proto.ClientHangingResponse_SUCCESS.Enum(),
						Result: rp.cr.GetResults()[0]}
					ctx.Return(rp_)
				}
			} else {
				rp_ := &proto.ClientHangingResponse{Status: proto.ClientHangingResponse_ERROR_NO_RESULTS.Enum()}

				switch rp.cr.GetStatus() {
				case proto.ClientResponse_ERROR_NOT_MASTER:
					rp_.Status = proto.ClientHangingResponse_NOT_MASTER.Enum()
				case proto.ClientResponse_ERROR_INTERNAL:
					rp_.Status = proto.ClientHangingResponse_ERROR_INTERNAL.Enum()
				}

				ctx.Return(rp_)
			}
		}
	}
}
