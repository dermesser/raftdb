package locking

import (
	"fmt"
	"sync"
	"time"
)

/*
The notification manager provides a simple slot notification
system. Slots can be sent to and waited on/received from. A message
to a slot is received by at most one receiver. Slots can be waited
on by multiple receivers. Slots are identified by a string.

The MultiplexedNotificationManager builds on the NotificationManager
and provides filtered pubsub functionality. You can configure
which type of notifications to send to only one respectively all
receivers. It uses a central thread to manage the routing of messages
which means that latency is not optimal, but good enough for most
use cases (<100 ms usually)
*/

type INotificationManager interface {
	Send(slot string, event NotificationType)
	Broadcast(event NotificationType)
	Wait(slot string, timeout time.Duration) NotificationType
	Waiter(slot string) *Waiter
}

type NotificationType int32

const (
	None             NotificationType = iota // Lock was released
	Locked                                   // Someone has acquired a lock
	Released                                 // Owner has released lock
	ReleasedTimedOut                         // Timeout on lock has passed, it can be acquired now
	Renewed                                  // Lock was renewed by someone else
	MasterStepDown                           // Master stepped down, stop watching
	ShutDown                                 // Member received shutdown command and left the cluster
	Timeout                                  // Timed out waiting for a notification
	Closed                                   // A notification channel is closed
	Set                                      // Set to a new value (for ordinary entries)
	Deleted                                  // Deleted entry
	// Reserved: 10 - 100
	// 101 - INT_MAX: Free for your use
)

func (n NotificationType) String() string {
	switch n {
	case None:
		return "None"
	case Locked:
		return "Locked"
	case Released:
		return "Released"
	case ReleasedTimedOut:
		return "ReleasedTimedOut"
	case Renewed:
		return "Renewed"
	case MasterStepDown:
		return "MasterStepDown"
	case ShutDown:
		return "ShutDown"
	case Timeout:
		return "Timeout"
	case Closed:
		return "Closed"
	case Set:
		return "Set"
	case Deleted:
		return "Deleted"
	default:
		return fmt.Sprintf("unknown-%d", n)
	}
}

type Waiter struct {
	ix   int
	slot string
	C    chan NotificationType
}

func (w *Waiter) Wait(timeout time.Duration) NotificationType {
	select {
	case event := <-w.C:
		if event != None {
			return event
		} else {
			return Closed
		}
	case <-time.After(timeout):
		return Timeout
	}
}

// A single 1:n subscription tracker
type multiplexer struct {
	chs []chan NotificationType
}

func (mpx *multiplexer) send(event NotificationType, one bool) {
LOOP:
	for _, c := range mpx.chs {
		if c != nil {
			select {
			case c <- event:
				if one {
					break LOOP
				}
			default:
			}
		}
	}
}

// Obtain a waiter that receives whitelisted notifications sent to the original slot.
func (mpx *multiplexer) add(slot string) *Waiter {
	ch := make(chan NotificationType, 3)

	for i, c := range mpx.chs {
		if c == nil {
			mpx.chs[i] = ch
			return &Waiter{C: ch, slot: slot, ix: i}
		}
	}

	mpx.chs = append(mpx.chs, ch)

	return &Waiter{C: ch, slot: slot, ix: len(mpx.chs) - 1}
}

// Remove a waiter obtained from a multiplexer. At this point no one may be waiting on that waiter anymore.
func (mpx *multiplexer) remove(w *Waiter) {
	mpx.chs[w.ix] = nil
	close(w.C)

	if mpx.isEmpty() {
		mpx.chs = nil
	}
}

// Returns true if there's no active waiters.
func (mpx *multiplexer) isEmpty() bool {
	var i int

	for _, c := range mpx.chs {
		if c != nil {
			i++
		}
	}

	return i == 0
}

// This manager sends notifications sent to one slot to several (and not only one)
// receivers, depending on the internal white/blacklist. This type is slightly more
// inefficient as it has to use a polling thread.
type MultiplexedNotificationManager struct {
	sync.Mutex
	mxs         map[string]*multiplexer
	multiplexed map[NotificationType]bool
}

// Sets up a new MultiplexedNotificationManager and runs RunNotifier() in a
// goroutine.
func NewMNManager() *MultiplexedNotificationManager {
	mnm := &MultiplexedNotificationManager{mxs: make(map[string]*multiplexer),
		multiplexed: make(map[NotificationType]bool)}

	mnm.AddBroadcastWhitelist(Closed, MasterStepDown,
		Renewed, Set, Deleted, Locked, Released, ReleasedTimedOut, ShutDown)
	return mnm
}

// Set which notifications are broadcasted to all receivers
func (mnm *MultiplexedNotificationManager) AddBroadcastWhitelist(evs ...NotificationType) {
	mnm.Lock()
	defer mnm.Unlock()
	for _, e := range evs {
		mnm.multiplexed[e] = true
	}
}

// Set which notifications are sent to only one receiver
func (mnm *MultiplexedNotificationManager) AddBroadcastBlacklist(evs ...NotificationType) {
	mnm.Lock()
	defer mnm.Unlock()
	for _, e := range evs {
		mnm.multiplexed[e] = false
	}
}

// Send to the given slot.
func (mnm *MultiplexedNotificationManager) Send(slot string, event NotificationType) {
	if mpx, ok := mnm.mxs[slot]; ok {
		mpx.send(event, !mnm.multiplexed[event])
	}
}

func (mnm *MultiplexedNotificationManager) Broadcast(event NotificationType) {
	var one bool
	if mnm.multiplexed[event] {
		one = false
	} else {
		one = true
	}
	for _, mx := range mnm.mxs {
		mx.send(event, one)
	}

}

// Messages that arrived before calling this function will not be delivered to you.
func (mnm *MultiplexedNotificationManager) Wait(slot string, timeout time.Duration) NotificationType {
	w := mnm.Waiter(slot)
	defer mnm.Unwait(w)

	return w.Wait(timeout)
}

// Obtain a waiter for the given slot. All messages delivered to the given slot
// are delivered to this waiter, provided they are read fast enough from it.
func (mnm *MultiplexedNotificationManager) Waiter(slot string) *Waiter {
	mnm.Lock()
	defer mnm.Unlock()

	if mnm.mxs[slot] == nil {
		mnm.mxs[slot] = &multiplexer{}
	}
	return mnm.mxs[slot].add(slot)
}

// Remove a waiter when it isn't used anymore. This is important to ensure delivery
// of blacklisted events (to other receivers)
func (mnm *MultiplexedNotificationManager) Unwait(w *Waiter) {
	mnm.Lock()
	defer mnm.Unlock()
	if mnm.mxs[w.slot] != nil {
		mnm.mxs[w.slot].remove(w)

		if len(mnm.mxs[w.slot].chs) == 0 {
			delete(mnm.mxs, w.slot)
		}
	}
}
