package locking

import (
	"testing"
	"time"
)

func TestMultiplexed1(t *testing.T) {
	mnm := NewMNManager()

	mnm.AddBroadcastWhitelist(42)

	w1 := mnm.Waiter("x")
	w2 := mnm.Waiter("x")

	mnm.Send("x", 42)

	var w1_r, w2_r bool

	for !w1_r || !w2_r {
		select {
		case <-w1.C:
			w1_r = true
		case <-w2.C:
			w2_r = true
		}
	}

	if !w1_r || !w2_r {
		t.Error("One notification was not received")
	}
}

func TestMultiplexed2(t *testing.T) {
	mnm := NewMNManager()

	mnm.AddBroadcastWhitelist(42)

	w1 := mnm.Waiter("x")
	w2 := mnm.Waiter("x")
	mnm.Unwait(w2)
	w3 := mnm.Waiter("x")

	mnm.Send("x", 42)

	var w1_r, w3_r bool

	for !w1_r || !w3_r {
		select {
		case <-w1.C:
			w1_r = true
		case <-w3.C:
			w3_r = true
		}
	}

	if !w1_r || !w3_r {
		t.Error("One notification was not received")
	}
}

func TestNonMultiplexed1(t *testing.T) {
	mnm := NewMNManager()

	mnm.AddBroadcastBlacklist(42)

	w1 := mnm.Waiter("x")
	w2 := mnm.Waiter("x")

	mnm.Send("x", 42)

	time.Sleep(100 * time.Millisecond)

	var w1_r, w2_r bool

	for i := 0; i < 2; i++ {
		select {
		case <-w1.C:
			w1_r = true
		case <-w2.C:
			w2_r = true
		default:
		}
	}

	if w1_r && w2_r {
		t.Error("One notification too many was received")
	}
}

func TestMultiplexerRemoved(t *testing.T) {
	mnm := NewMNManager()

	w := mnm.Waiter("abc")
	mnm.Unwait(w)

	if mnm.mxs["abc"] != nil {
		t.Error("Unused slot was not removed")
	}
}

func TestPlainWait(t *testing.T) {
	mnm := NewMNManager()

	go func() {
		time.Sleep(100 * time.Millisecond)
		mnm.Send("abc", Set)
	}()

	ev := mnm.Wait("abc", 200*time.Millisecond)

	if ev != Set {
		t.Error("Wait() returned wrong event:", ev.String())
	}
}

func TestInterfaceFulfilled(t *testing.T) {
	var nmi INotificationManager
	nmi = NewMNManager()

	nmi.Broadcast(ShutDown)
}
