package locking

import (
	"testing"
	"time"
)

func TestNormalTimeout(t *testing.T) {
	nmgr := NewMNManager()
	tomgr := NewTimeoutManager(nmgr)

	tomgr.SetPollInterval(20 * time.Millisecond)
	go tomgr.RunNotifier()
	defer tomgr.StopNotifier()

	waiter := nmgr.Waiter("to1")

	tomgr.SetTimeout("to1", time.Now().Add(500*time.Millisecond))

	time.Sleep(100 * time.Millisecond)

	notif := waiter.Wait(0)

	if notif != Timeout {
		t.Error("Unexpected notification:", notif)
	}

	time.Sleep(550 * time.Millisecond)

	notif = waiter.Wait(10)

	if notif != ReleasedTimedOut {
		t.Error("Unexpected notification:", notif)
	}

	tomgr.StopNotifier()
}

func TestExtendTimeout(t *testing.T) {
	nmgr := NewMNManager()
	tomgr := NewTimeoutManager(nmgr)

	tomgr.SetPollInterval(20 * time.Millisecond)
	go tomgr.RunNotifier()
	defer tomgr.StopNotifier()

	timeout := time.Now().Add(500 * time.Millisecond)

	waiter := nmgr.Waiter("to1")

	tomgr.SetTimeout("to1", timeout)

	time.Sleep(100 * time.Millisecond)

	notif := waiter.Wait(0)

	if notif != Timeout {
		t.Error("Unexpected notification:", notif)
	}

	tomgr.SetTimeout("to1", timeout.Add(500*time.Millisecond))

	time.Sleep(550 * time.Millisecond)

	notif = waiter.Wait(0)

	if notif != Timeout {
		t.Error("Unexpected notification:", notif)
	}

	time.Sleep(550 * time.Millisecond)

	notif = waiter.Wait(0)

	if notif != ReleasedTimedOut {
		t.Error("Unexpected notification:", notif)
	}

	tomgr.StopNotifier()
}

func TestMultipleTimeouts(t *testing.T) {
	nmgr := NewMNManager()
	tomgr := NewTimeoutManager(nmgr)
	now := time.Now()

	tomgr.SetPollInterval(20 * time.Millisecond)
	go tomgr.RunNotifier()
	defer tomgr.StopNotifier()

	tomgr.SetTimeout("to1", now.Add(200*time.Millisecond))
	tomgr.SetTimeout("to2", now.Add(400*time.Millisecond))
	tomgr.SetTimeout("to3", now.Add(600*time.Millisecond))

	var i int

	t.Log("now:", now)

OUTER:
	for {
		select {
		case n := <-nmgr.Waiter("to1").C:
			if i != 0 {
				t.Error("Called in wrong order:", 0, i)
			}
			if n != ReleasedTimedOut {
				t.Error("Wrong notification:", n)
			}
			t.Log(time.Now(), 1)
			i++
		case n := <-nmgr.Waiter("to2").C:
			if i != 1 {
				t.Error("Called in wrong order:", 1, i)
			}
			if n != ReleasedTimedOut {
				t.Error("Wrong notification:", n)
			}
			t.Log(time.Now(), 2)
			i++
		case n := <-nmgr.Waiter("to3").C:
			if i != 2 {
				t.Error("Called in wrong order:", 2, i)
			}
			if n != ReleasedTimedOut {
				t.Error("Wrong notification:", n)
			}
			t.Log(time.Now(), 3)
			break OUTER
		}
	}

	tomgr.StopNotifier()
}

func TestStopTimeout(t *testing.T) {
	nmgr := NewMNManager()
	tomgr := NewTimeoutManager(nmgr)
	now := time.Now()

	tomgr.SetPollInterval(20 * time.Millisecond)
	go tomgr.RunNotifier()
	defer tomgr.StopNotifier()

	tomgr.SetTimeout("to1", now.Add(200*time.Millisecond))
	tomgr.StopTimeout("to1")

	ev := nmgr.Wait("to1", 300*time.Millisecond)

	if ev != Timeout {
		t.Error("Timeout was not cancelled properly:", ev)
	}
}

func TestHasTimeout(t *testing.T) {
	nmgr := NewMNManager()
	tomgr := NewTimeoutManager(nmgr)
	now := time.Now()

	tomgr.SetPollInterval(20 * time.Millisecond)
	go tomgr.RunNotifier()
	defer tomgr.StopNotifier()

	tomgr.SetTimeout("to1", now.Add(200*time.Millisecond))

	if _, err := tomgr.HasTimeout("to1"); err != nil {
		t.Error("Should have timeout")
	}

	tomgr.StopTimeout("to1")

	if _, err := tomgr.HasTimeout("to1"); err == nil {
		t.Error("Should have timeout")
	}

}
