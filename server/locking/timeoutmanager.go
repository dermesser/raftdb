package locking

import (
	"container/heap"
	"errors"
	"sync"
	"time"
)

// This file implements a manager that notifies a notification manager
// when timeouts pass. It tries to does this in an efficient way.

type timeoutEntry struct {
	timeout time.Time
	id      string
}

type timeoutHeap []*timeoutEntry

// All functions are safe to call on nil; in that case, they return immediately
type TimeoutManager struct {
	sync.Mutex

	// We want be able to access timeouts both by next-first and by name.
	// Therefore we store them twice in different data structures.
	// If a timeout is present in the heap but not the map, it is considered
	// "stopped" (i.e., aborted); if the timeout in the map has a different value than that in
	// the heap, the value in the map is the truth (when a timeout was postponed)
	timeouts     *timeoutHeap
	timeouts_map map[string]time.Time

	ival time.Duration
	nmgr INotificationManager
}

// Creates a new timeout manager sending timeout notifications to nmgr.
// The default poll interval is 200ms. You can set it with SetPollInterval().
func NewTimeoutManager(nmgr INotificationManager) *TimeoutManager {
	h := make(timeoutHeap, 0, 64)
	heap.Init(&h)

	return &TimeoutManager{timeouts: &h,
		timeouts_map: make(map[string]time.Time),
		ival:         200 * time.Millisecond,
		nmgr:         nmgr}
}

// Default interval is 200ms
func (tm *TimeoutManager) SetPollInterval(ival time.Duration) {
	if tm == nil {
		return
	}

	tm.ival = ival
}

// Adds or renews a timeout, depending on whether one already exists for id.
// Renewing means that the timeout will be set to a point further in the future.
func (tm *TimeoutManager) SetTimeout(id string, when time.Time) {
	if tm == nil {
		return
	}

	tm.Lock()
	defer tm.Unlock()

	// Check if timeout already exists
	timeout, ok := tm.timeouts_map[id]

	if !ok {
		tm.timeouts_map[id] = when
		timeout := &timeoutEntry{timeout: when, id: id}
		heap.Push(tm.timeouts, timeout)
	} else if timeout.Sub(when) < 0 { // only set if new timeout is further in the future
		tm.timeouts_map[id] = when
	}
}

func (tm *TimeoutManager) HasTimeout(id string) (time.Time, error) {
	if tm == nil {
		return time.Unix(0, 0), errors.New("manager is nil")
	}

	tm.Lock()
	defer tm.Unlock()

	when, ok := tm.timeouts_map[id]

	if ok {
		return when, nil
	} else {
		return time.Unix(0, 0), errors.New("Timeout not found")
	}

}

// This will stop the timeout manager from sending any notifications for the given
// timeout.
func (tm *TimeoutManager) StopTimeout(id string) {
	if tm == nil {
		return
	}

	tm.Lock()
	defer tm.Unlock()

	delete(tm.timeouts_map, id)
}

// Checks for newly expired timeouts.
// Run in goroutine
func (tm *TimeoutManager) RunNotifier() {
	if tm == nil {
		return
	}

	for tm.ival > 0 {
		time.Sleep(tm.ival)

		tm.sendExpiredNotifications()
	}
}

// Stop the goroutine running the notifier
func (tm *TimeoutManager) StopNotifier() {
	if tm == nil {
		return
	}

	tm.ival = 0
}

// Send MasterStepDown to all ids and stop the notifier thread.
func (tm *TimeoutManager) StepDownClose() {
	if tm == nil {
		return
	}

	tm.Lock()
	defer tm.Unlock()

	tm.StopNotifier()

	for tm.timeouts.Len() > 0 {
		timeout := heap.Pop(tm.timeouts).(*timeoutEntry)
		tm.nmgr.Send(timeout.id, MasterStepDown)
	}
}

func (tm *TimeoutManager) sendExpiredNotifications() {
	if tm == nil {
		return
	}

	tm.Lock()
	defer tm.Unlock()

	// Use one reference time for evaluating all timeouts.
	now := time.Now()

	// quickly check if the min element has already expired; if not, return
	if tm.timeouts.Len() > 0 && (*tm.timeouts)[0].timeout.Before(now) {
		for tm.timeouts.Len() > 0 {
			// get the timeout that is expiring next
			timeout := heap.Pop(tm.timeouts).(*timeoutEntry)
			new_timeout, ok := tm.timeouts_map[timeout.id]

			// The timeout has no entry in the map because it was stopped
			if !ok {
				continue
			}

			// Timeout hasn't occurred yet. We can stop here, all others in the
			// heap will occur later
			if timeout.timeout.After(now) {

				if ok {
					timeout.timeout = new_timeout
				}
				heap.Push(tm.timeouts, timeout)
				break

			} else { // timeout has occurred
				// If the timeout has been renewed in the meantime,
				// set the new timeout and re-insert.
				if new_timeout.After(now) {
					timeout.timeout = new_timeout
					heap.Push(tm.timeouts, timeout)
				} else {
					tm.nmgr.Send(timeout.id, ReleasedTimedOut)
					delete(tm.timeouts_map, timeout.id)
				}
			}
		}
	}
}

// heap.Interface implementation
func (h *timeoutHeap) Push(e interface{}) {
	if nil == e.(*timeoutEntry) {
		panic("Unsupported type given to heap")
	}

	*h = append(*h, e.(*timeoutEntry))

}

func (h *timeoutHeap) Pop() interface{} {
	elem := (*h)[len(*h)-1]
	*h = (*h)[0 : len(*h)-1]
	return elem
}

func (h *timeoutHeap) Len() int {
	return len(*h)
}

func (h *timeoutHeap) Less(i, j int) bool {
	return (*h)[i].timeout.Sub((*h)[j].timeout) < 0
}

func (h *timeoutHeap) Swap(i, j int) {
	(*h)[i], (*h)[j] = (*h)[j], (*h)[i]
}
