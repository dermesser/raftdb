package server

import (
	"bytes"
	"fmt"
	"os"
	"raftdb/proto"
	"raftdb/server/constants"
	"raftdb/server/locking"
	"raftdb/server/storage"
	"raftdb/server/versionedstorage"
	"testing"
	"time"

	pb "github.com/gogo/protobuf/proto"
)

func init() {
	setUpLkupClient()
	setUpSecurity()
}

func setPTUp() {
	cst := storage.NewTempStorage()

	vs = versionedstorage.NewIntegratedVersionedStorage(locking.NewMNManager(), cst)
}

func tearPTDown() {
	os.RemoveAll("testdb.db")
}

func TestConstructor(t *testing.T) {
	setPTUp()
	defer tearPTDown()

	proc, err := newProcessor(vs, "_testcluster1", "__test.member1")

	if err != nil {
		t.Error(err)
	}
	if proc == nil {
		t.Error("proc is nil")
	}

	if proc.cluster == nil {
		t.Error("cluster is nil")
	}

	if proc.cluster.config == nil {
		t.Error("cluster is nil")
	}
}

func TestApplyConfigRequest(t *testing.T) {
	setPTUp()
	defer tearPTDown()

	proc, err := newProcessor(vs, "cluster1", "__test.member1")

	proc.processor_state = state_LEADER

	if err != nil {
		t.Error(err)
	}

	crq := &proto.ConfigRequest{Type: proto.ConfigRequest_ADD_MEMBER.Enum(),
		ClusterId: pb.String("_testcluster1"),
		Members:   []string{"testmember1", "testmember2"},
	}

	entry, _, err := proc.prepareConfigRequest(crq)

	if err != nil {
		t.Error(err)
	}

	proc.commitConfigRequest(crq)

	if len(proc.cluster.config.getMembers()) != 2 {
		t.Error("Internal config not updated")
	}

	if entry == nil {
		t.Error("entry is nil")
	}

	if !bytes.Equal(entry.GetKey(), []byte(constants.CONFIG_ENTRY_KEY)) {
		t.Error("Wrong key")
	}

	if entry.GetEntry().GetValue() == nil {
		t.Error("No DB value present")
	}

	crq2 := &proto.ConfigRequest{Type: proto.ConfigRequest_REMOVE_MEMBER.Enum(),
		ClusterId: pb.String("_testcluster1"),
		Members:   []string{"testmember2"},
	}

	entry, _, err = proc.prepareConfigRequest(crq2)

	if err != nil {
		t.Error(err)
	}

	if entry == nil {
		t.Error("entry is nil")
	}

	proc.commitConfigRequest(crq2)

	if len(proc.cluster.config.getMembers()) != 1 {
		t.Error("Internal config not updated")
	}

	if !bytes.Equal(entry.GetKey(), []byte(constants.CONFIG_ENTRY_KEY)) {
		t.Error("Wrong key in entry")
	}

	if entry.GetEntry().GetValue() == nil {
		t.Error("No DB value present")
	}

	ix, _ := proc.st.Prepare(versionedstorage.Entry2Op(entry))
	proc.st.Commit(ix)

	proc.cluster.config.members = nil
	// so we don't have to write a complete new config proto
	proc.cluster.config.version--

	proc.setConfigFromPersistent()

	if len(proc.cluster.config.getMembers()) != 1 {
		t.Error("Config was not loaded")
	}
}

func TestSendAll(t *testing.T) {
	setPTUp()
	defer tearPTDown()

	proc, err := newProcessor(vs, "cluster1", "__test.member1")

	proc.processor_state = state_LEADER

	if err != nil {
		t.Error(err)
	}

	rqc1 := make(chan *RpcRequest, 1)
	m1_member := newFakeMember(0, 0, 1, 1)
	m1_coord := newCoordinator(rqc1, m1_member, "ldr", vs, "cluster1")
	go m1_coord.runCoordinator()
	m1_intmem := newInternalMember(rqc1, "m1")

	rqc2 := make(chan *RpcRequest, 1)
	m2_member := newFakeMember(0, 0, 1, 1)
	m2_coord := newCoordinator(rqc2, m2_member, "ldr", vs, "cluster1")
	go m2_coord.runCoordinator()
	m2_intmem := newInternalMember(rqc2, "m2")

	rqc3 := make(chan *RpcRequest, 1)
	m3_member := newFakeMember(0, 0, 1, 1)
	m3_coord := newCoordinator(rqc3, m3_member, "ldr", vs, "cluster1")
	go m3_coord.runCoordinator()
	m3_intmem := newInternalMember(rqc3, "m3")

	proc.cluster.addMember("m1", m1_intmem)
	proc.cluster.addMember("m2", m2_intmem)
	proc.cluster.addMember("m3", m3_intmem)

	var entry1 *proto.Entry = &proto.Entry{Type: proto.Entry_PUT.Enum(),
		Key: []byte("abc"),
		Entry: &proto.DBEntry{Type: proto.DBEntry_DATA.Enum(),
			Value:   []byte("def"),
			Version: &proto.VersionInfo{Term: pb.Uint64(1), LogIx: (pb.Uint64(1))},
		},
	}

	rq := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId: pb.String("1"),
		PreviousLogVersion: &proto.VersionInfo{Term: pb.Uint64(1),
			LogIx: pb.Uint64(0)},
		Entries: []*proto.Entry{entry1}}

	rp := proc.cluster.sendToAll(&RpcRequest{ae: rq}, 3)

	if len(rp) != 3 {
		t.Error("Did not receive all responses:", rp)
	}
}

func TestHeartbeatWaiter(t *testing.T) {
	setPTUp()
	defer tearPTDown()

	proc, err := newProcessor(vs, "cluster1", "__test.member1")

	proc.processor_state = state_FOLLOWER
	proc.timeout = 20 * time.Millisecond

	if err != nil {
		t.Error(err)
	}

	proc.timeOutWaiter()

	if proc.processor_state != state_CANDIDATE {
		t.Error("Wrong state.", proc.processor_state)
	}
}

func TestHeartbeatSender(t *testing.T) {
	setPTUp()
	defer tearPTDown()

	proc, err := newProcessor(vs, "cluster1", "__test.member1")

	proc.processor_state = state_LEADER
	proc.timeout = 500 * time.Millisecond

	proc.st.SetNextTerm("")
	proc.st.SetNextTerm("")
	proc.st.SetNextTerm("")

	if err != nil {
		t.Error(err)
	}

	rqc1 := make(chan *RpcRequest, 1)
	m1_member := newFakeMember(0, 0, 1, 1)
	m1_coord := newCoordinator(rqc1, m1_member, "ldr", vs, "cluster1")
	go m1_coord.runCoordinator()
	m1_intmem := newInternalMember(rqc1, "m1")

	rqc2 := make(chan *RpcRequest, 1)
	m2_member := newFakeMember(0, 0, 1, 1)
	m2_coord := newCoordinator(rqc2, m2_member, "ldr", vs, "cluster1")
	go m2_coord.runCoordinator()
	m2_intmem := newInternalMember(rqc2, "m2")

	rqc3 := make(chan *RpcRequest, 1)
	m3_member := newFakeMember(0, 0, 1, 1)
	m3_coord := newCoordinator(rqc3, m3_member, "ldr", vs, "cluster1")
	go m3_coord.runCoordinator()
	m3_intmem := newInternalMember(rqc3, "m3")

	proc.cluster.addMember("m1", m1_intmem)
	proc.cluster.addMember("m2", m2_intmem)
	proc.cluster.addMember("m3", m3_intmem)
	go proc.heartBeatSender()

	time.Sleep(100 * time.Millisecond)
	// Let the heartbeatsender exit
	proc.processor_state = state_FOLLOWER

	if m1_member.term != 3 || m2_member.term != 3 || m3_member.term != 3 {
		t.Error("Wrong term on one member:", m1_member.term, m2_member.term, m3_member.term)
	}
}

func TestRequestVoteSuccess(t *testing.T) {
	setPTUp()
	defer tearPTDown()

	proc, err := newProcessor(vs, "cluster1", "__test.member1")

	proc.processor_state = state_FOLLOWER
	proc.timeout = 20 * time.Millisecond

	if err != nil {
		t.Error(err)
	}

	rq := &proto.RequestVote{Term: pb.Uint64(2),
		CandidateId:   pb.String("2"),
		LogIx:         &proto.VersionInfo{Term: pb.Uint64(1), LogIx: pb.Uint64(1)},
		ConfigVersion: pb.Uint64(1),
	}

	rp, err := proc.RequestVote(rq)

	if !rp.GetVoteGranted() {
		t.Error("No vote granted,", rp)
	}
}

func TestRequestVoteFail(t *testing.T) {
	setPTUp()
	defer tearPTDown()

	proc, err := newProcessor(vs, "cluster1", "__test.member1")
	proc.st.SetNextTerm("")

	proc.processor_state = state_FOLLOWER
	proc.timeout = 20 * time.Millisecond

	if err != nil {
		t.Error(err)
	}

	rq := &proto.RequestVote{Term: pb.Uint64(1),
		CandidateId:   pb.String("2"),
		LogIx:         &proto.VersionInfo{Term: pb.Uint64(1), LogIx: pb.Uint64(1)},
		ConfigVersion: pb.Uint64(1),
	}

	rp, err := proc.RequestVote(rq)

	if rp.GetVoteGranted() {
		t.Error("Vote granted,", rp)
	}

	if rp.GetFailType() != proto.Vote_WRONG_TERM {
		t.Error("Wrong fail reason", rp.GetFailType())
	}
}

func TestRequestVoteFailIndex(t *testing.T) {
	setPTUp()
	defer tearPTDown()

	proc, err := newProcessor(vs, "cluster1", "__test.member1")

	if err != nil {
		t.Error(err)
	}

	proc.processor_state = state_FOLLOWER
	proc.timeout = 20 * time.Millisecond

	var entry1 *proto.Entry = &proto.Entry{Type: proto.Entry_PUT.Enum(),
		Key: []byte("abc"),
		Entry: &proto.DBEntry{Type: proto.DBEntry_DATA.Enum(),
			Value:   []byte("def"),
			Version: &proto.VersionInfo{Term: pb.Uint64(1), LogIx: (pb.Uint64(1))},
		},
	}

	ix, _ := proc.st.Prepare(versionedstorage.Entry2Op(entry1))
	proc.st.Commit(ix)
	ix, _ = proc.st.Prepare(versionedstorage.Entry2Op(entry1))
	proc.st.Commit(ix)

	rq := &proto.RequestVote{Term: pb.Uint64(2),
		CandidateId:   pb.String("2"),
		LogIx:         &proto.VersionInfo{Term: pb.Uint64(1), LogIx: pb.Uint64(1)},
		ConfigVersion: pb.Uint64(1),
	}

	rp, err := proc.RequestVote(rq)

	if rp.GetVoteGranted() {
		t.Error("Vote granted,", rp)
	}

	if rp.GetFailType() != proto.Vote_WRONG_LOGIX {
		t.Error("Wrong fail reason", rp.GetFailType())
	}
}

func TestAppendEntries1(t *testing.T) {
	setPTUp()
	defer tearPTDown()

	proc, err := newProcessor(vs, "cluster1", "__test.member1")

	if err != nil {
		t.Error(err)
	}

	proc.processor_state = state_FOLLOWER
	proc.timeout = 20 * time.Millisecond

	var entry1 *proto.Entry = &proto.Entry{Type: proto.Entry_PUT.Enum(),
		Key: []byte("abc"),
		Entry: &proto.DBEntry{Type: proto.DBEntry_DATA.Enum(),
			Value:   []byte("def"),
			Version: &proto.VersionInfo{Term: pb.Uint64(1), LogIx: (pb.Uint64(1))},
		},
	}

	rq := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId: pb.String("1"),
		PreviousLogVersion: &proto.VersionInfo{Term: pb.Uint64(1),
			LogIx: pb.Uint64(0)},
		Entries: []*proto.Entry{entry1},
	}

	rp, err := proc.AppendEntries(rq)

	if err != nil {
		t.Error(err)
	}

	if !rp.GetSuccess() {
		t.Error("No success", rp)
	}
}

func TestAppendEntriesFail1(t *testing.T) {
	setPTUp()
	defer tearPTDown()

	proc, err := newProcessor(vs, "cluster1", "__test.member1")

	if err != nil {
		t.Error(err)
	}

	proc.processor_state = state_FOLLOWER
	proc.timeout = 20 * time.Millisecond

	var entry1 *proto.Entry = &proto.Entry{Type: proto.Entry_PUT.Enum(),
		Key: []byte("abc"),
		Entry: &proto.DBEntry{Type: proto.DBEntry_DATA.Enum(),
			Value:   []byte("def"),
			Version: &proto.VersionInfo{Term: pb.Uint64(1), LogIx: (pb.Uint64(2))},
		},
	}

	rq := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId: pb.String("1"),
		PreviousLogVersion: &proto.VersionInfo{Term: pb.Uint64(1),
			LogIx: pb.Uint64(1)},
		Entries: []*proto.Entry{entry1},
	}

	rp, err := proc.AppendEntries(rq)

	if err != nil {
		t.Error(err)
	}

	if rp.GetSuccess() {
		t.Error("Success", rp)
	}

	if rp.GetLastIndex() != 0 {
		t.Error("Wrong last index", rp.GetLastIndex())
	}
}

func TestAppendEntriesFail2(t *testing.T) {
	setPTUp()
	defer tearPTDown()

	proc, err := newProcessor(vs, "cluster1", "__test.member1")

	if err != nil {
		t.Error(err)
	}

	proc.processor_state = state_FOLLOWER
	proc.timeout = 20 * time.Millisecond

	var entry1 *proto.Entry = &proto.Entry{Type: proto.Entry_PUT.Enum(),
		Key: []byte("abc"),
		Entry: &proto.DBEntry{Type: proto.DBEntry_DATA.Enum(),
			Value:   []byte("def"),
			Version: &proto.VersionInfo{Term: pb.Uint64(1), LogIx: (pb.Uint64(2))},
		},
	}

	rq := &proto.AppendEntriesRequest{Term: pb.Uint64(0),
		LeaderId: pb.String("1"),
		PreviousLogVersion: &proto.VersionInfo{Term: pb.Uint64(1),
			LogIx: pb.Uint64(1)},
		Entries: []*proto.Entry{entry1},
	}

	rp, err := proc.AppendEntries(rq)

	if err != nil {
		t.Error(err)
	}

	if rp.GetSuccess() {
		t.Error("Success", rp)
	}

	if rp.GetLastIndex() != 0 {
		t.Error("Wrong last index", rp.GetLastIndex())
	}
}

func TestConsecutiveAppendEntries(t *testing.T) {
	setPTUp()
	defer tearPTDown()

	proc, err := newProcessor(vs, "cluster1", "__test.member1")

	if err != nil {
		t.Error(err)
	}

	proc.processor_state = state_FOLLOWER
	proc.timeout = 20 * time.Millisecond

	var entry1 *proto.Entry = &proto.Entry{Type: proto.Entry_PUT.Enum(),
		Key: []byte("abc"),
		Entry: &proto.DBEntry{Type: proto.DBEntry_DATA.Enum(),
			Value:   []byte("def"),
			Version: &proto.VersionInfo{Term: pb.Uint64(1), LogIx: (pb.Uint64(1))},
		},
	}

	rq := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId: pb.String("1"),
		PreviousLogVersion: &proto.VersionInfo{Term: pb.Uint64(1),
			LogIx: pb.Uint64(0)},
		Entries: []*proto.Entry{entry1},
	}

	rp, err := proc.AppendEntries(rq)
	proc.st.Commit(1)

	if err != nil {
		t.Error(err)
	}

	if !rp.GetSuccess() {
		t.Error("No success", rp)
	}

	var entry2 *proto.Entry = &proto.Entry{Type: proto.Entry_PUT.Enum(),
		Key: []byte("123"),
		Entry: &proto.DBEntry{Type: proto.DBEntry_DATA.Enum(),
			Value:   []byte("456"),
			Version: &proto.VersionInfo{Term: pb.Uint64(1), LogIx: (pb.Uint64(2))},
		},
	}

	rq2 := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId: pb.String("1"),
		PreviousLogVersion: &proto.VersionInfo{Term: pb.Uint64(1),
			LogIx: pb.Uint64(1)},
		Entries: []*proto.Entry{entry2},
	}

	rp2, err2 := proc.AppendEntries(rq2)
	proc.st.Commit(2)

	if err2 != nil {
		t.Error(err2)
	}

	if !rp2.GetSuccess() {
		t.Error("No success", rp2)
	}

	if val, _ := proc.st.Get([]byte("abc")); val == nil {
		t.Error("entry2 is not in storage")
	}

	if val, _ := proc.st.Get([]byte("123")); val == nil {
		t.Error("entry2 is not in storage")
	}
}

func TestMakeEntry(t *testing.T) {
	setPTUp()
	defer tearPTDown()

	action := &proto.Action{Type: proto.Action_SET.Enum(),
		Key:   []byte("abc"),
		Value: []byte("def")}

	proc, err := newProcessor(vs, "cluster1", "__test.member1")

	if err != nil {
		t.Error(err)
	}

	aeop := versionedstorage.Actions2Op([]*proto.Action{action}, "clientid")
	_, entries := proc.st.Prepare(aeop)

	if len(entries) != 1 {
		t.Error("Wrong number of entries")
	}

	if !bytes.Equal(entries[0].GetKey(), []byte("abc")) {
		t.Error("Wrong key")
	}

	if !bytes.Equal(entries[0].GetEntry().GetValue(), []byte("def")) {
		t.Error("Wrong value")
	}
}

func TestInstallSnapshotProcessor(t *testing.T) {
	setPTUp()
	defer tearPTDown()

	proc, err := newProcessor(vs, "cluster1", "__test.member1")
	proc.st.SetNextTerm("")

	proc.processor_state = state_FOLLOWER_SNAPSHOTTING

	if err != nil {
		t.Error(err)
	}

	snapshot := make([]*proto.Entry, 500)

	for i := range snapshot {
		snapshot[i] = &proto.Entry{Type: proto.Entry_PUT.Enum(),
			Key: append([]byte(storage.STORAGE_KEY_PREFIX), []byte(fmt.Sprintf("%d", i))...),
			Entry: &proto.DBEntry{Type: proto.DBEntry_DATA.Enum(),
				Value:   []byte(fmt.Sprintf("%d%d", i, i+1)),
				Version: &proto.VersionInfo{Term: pb.Uint64(1), LogIx: (pb.Uint64(uint64(i + 1)))},
			},
		}
	}

	snapshotrq := &proto.InstallSnapshot{
		Term:         pb.Uint64(1),
		LeaderId:     pb.String("__test.member2"),
		LastIncluded: snapshot[499],
		Snapshot:     snapshot,
		Done:         pb.Bool(true),
		ClusterId:    pb.String("cluster1"),
		MemberId:     pb.String("__test.member1"),
	}

	rp, err := proc.InstallSnapshot(snapshotrq)

	if err != nil {
		t.Error(err)
	}

	if rp.GetStatus() != proto.InstallSnapshotResponse_OK || rp.GetTerm() != 1 {
		t.Error("Bad response:", rp)
	}

	next, commit, first := proc.st.Index()

	if next != 501 || commit != 500 || first != 500 {
		t.Error("Wrong indices:", next, commit, first)
	}

	// Prepend prefix so it's found in the TempStorage database
	if val, _ := proc.st.Get(append([]byte(storage.STORAGE_KEY_PREFIX), []byte("123")...)); val == nil {
		t.Error("Expected entry in DB not found")
	}
}

func setUpFakeMembers(proc *processor) {
	// Set up fake members
	rqc1 := make(chan *RpcRequest, 1)
	m1_member := newFakeMember(0, 0, 1, 1)
	m1_coord := newCoordinator(rqc1, m1_member, "ldr", vs, "cluster1")
	go m1_coord.runCoordinator()
	m1_intmem := newInternalMember(rqc1, "m1")

	rqc2 := make(chan *RpcRequest, 1)
	m2_member := newFakeMember(0, 0, 1, 1)
	m2_coord := newCoordinator(rqc2, m2_member, "ldr", vs, "cluster1")
	go m2_coord.runCoordinator()
	m2_intmem := newInternalMember(rqc2, "m2")

	rqc3 := make(chan *RpcRequest, 1)
	m3_member := newFakeMember(0, 0, 1, 1)
	m3_coord := newCoordinator(rqc3, m3_member, "ldr", vs, "cluster1")
	go m3_coord.runCoordinator()
	m3_intmem := newInternalMember(rqc3, "m3")

	proc.cluster.addMember("m1", m1_intmem)
	proc.cluster.addMember("m2", m2_intmem)
	proc.cluster.addMember("m3", m3_intmem)

}

func TestClientRequest1(t *testing.T) {
	setPTUp()
	defer tearPTDown()

	proc, err := newProcessor(vs, "cluster1", "__test.member1")

	proc.processor_state = state_LEADER

	if err != nil {
		t.Error(err)
	}

	clientrequest := &proto.ClientRequest{Strategy: proto.ClientRequest_TRANSACTION.Enum(),
		Actions: []*proto.Action{&proto.Action{Type: proto.Action_SET.Enum(),
			Key:   []byte("abc"),
			Value: []byte("def")}},
		ClusterId: pb.String("cluster1"),
		ClientId:  pb.String("client1")}

	setUpFakeMembers(proc)

	ch := make(chan *RpcResponse, 1)

	proc.runLeaderClientRequest(&RpcRequest{cr: clientrequest, rpchan: ch})

	select {
	case rp := <-ch:
		if rp.cr == nil || rp.cr.GetStatus() != proto.ClientResponse_SUCCESS {
			t.Error("Client response is wrong:", rp)
		}
	default:
		t.Error("Didn't receive response")
	}

	getrq := &proto.ClientRequest{Strategy: proto.ClientRequest_TRANSACTION.Enum(),
		Actions: []*proto.Action{&proto.Action{Type: proto.Action_GET.Enum(),
			Key: []byte("abc")}},
		ClusterId: pb.String("cluster1"),
		ClientId:  pb.String("client1")}

	proc.runLeaderClientRequest(&RpcRequest{cr: getrq, rpchan: ch})

	select {
	case rp := <-ch:
		if rp.cr == nil || rp.cr.GetStatus() != proto.ClientResponse_SUCCESS || !bytes.Equal(rp.cr.GetResults()[0].GetResult(), clientrequest.GetActions()[0].GetValue()) {
			t.Error("Response has unexpected properties:", rp, rp.cr)
		}
	default:
		t.Error("Didn't receive response")
	}
}

func TestClientRequestCounter(t *testing.T) {
	setPTUp()
	defer tearPTDown()

	proc, err := newProcessor(vs, "cluster1", "__test.member1")

	proc.processor_state = state_LEADER

	if err != nil {
		t.Error(err)
	}

	setUpFakeMembers(proc)

	clientrequest := &proto.ClientRequest{Strategy: proto.ClientRequest_TRANSACTION.Enum(),
		Actions: []*proto.Action{&proto.Action{Type: proto.Action_COUNT.Enum(),
			Key: []byte("abc"),
			CounterOp: &proto.CounterOp{Type: proto.CounterOp_COUNT.Enum(),
				Val: pb.Int64(1)}}},
		ClusterId: pb.String("cluster1"),
		ClientId:  pb.String("client1")}

	ch := make(chan *RpcResponse, 1)

	proc.runLeaderClientRequest(&RpcRequest{cr: clientrequest, rpchan: ch})

	select {
	case rp := <-ch:
		if rp.cr == nil || rp.cr.GetStatus() != proto.ClientResponse_SUCCESS {
			t.Error("Client response is wrong:", rp)
		}
	default:
		t.Error("Didn't receive response")
	}

	getrq := &proto.ClientRequest{Strategy: proto.ClientRequest_TRANSACTION.Enum(),
		Actions: []*proto.Action{&proto.Action{Type: proto.Action_GET.Enum(),
			Key: []byte("abc")}},
		ClusterId: pb.String("cluster1"),
		ClientId:  pb.String("client1")}

	proc.runLeaderClientRequest(&RpcRequest{cr: getrq, rpchan: ch})

	select {
	case rp := <-ch:
		if rp.cr == nil || rp.cr.GetResults()[0].GetStatus() != proto.ActionResult_SUCCESS || rp.cr.GetResults()[0].GetCounterResult() != 1 ||
			len(rp.cr.GetResults()[0].GetResult()) > 0 {
			t.Error("Response has unexpected properties:", rp, rp.cr)
		}
	default:
		t.Error("Didn't receive response")
	}
}

func TestClientRequestMultiAction(t *testing.T) {
	setPTUp()
	defer tearPTDown()

	proc, err := newProcessor(vs, "cluster1", "__test.member1")

	proc.processor_state = state_LEADER

	if err != nil {
		t.Error(err)
	}

	setUpFakeMembers(proc)

	actions := []*proto.Action{&proto.Action{Type: proto.Action_GET.Enum(),
		Key: []byte("abc")},

		&proto.Action{Type: proto.Action_SET.Enum(),
			Key:   []byte("abc"),
			Value: []byte("def")},

		&proto.Action{Type: proto.Action_LOCK.Enum(),
			Key:   []byte("lock"),
			Value: []byte("val")},
	}

	clientrequest := &proto.ClientRequest{Strategy: proto.ClientRequest_BATCH.Enum(),
		Actions:   actions,
		ClusterId: pb.String("cluster1"),
		ClientId:  pb.String("client1")}

	newix, ae, crp := proc.createAERequest(clientrequest)

	if newix != 2 {
		t.Error("Wrong newix:", newix)
	}

	if ae == nil || len(ae.GetEntries()) != 2 {
		t.Error("Wrong AE:", ae)
	}

	if len(crp.GetResults()) != 3 || crp.GetStatus() != proto.ClientResponse_SUCCESS {
		t.Error("Wrong client response", crp)
	}

	t.Log(crp)
}

func TestClientRequestTransactionFail(t *testing.T) {
	setPTUp()
	defer tearPTDown()

	proc, err := newProcessor(vs, "cluster1", "__test.member1")

	proc.processor_state = state_LEADER

	if err != nil {
		t.Error(err)
	}

	setUpFakeMembers(proc)

	actions := []*proto.Action{&proto.Action{Type: proto.Action_GET.Enum(),
		Key: []byte("abc")},

		&proto.Action{Type: proto.Action_LOCK.Enum(),
			Key:   []byte("lock"),
			Value: []byte("val")},

		&proto.Action{Type: proto.Action_SET.Enum(),
			Key:   []byte("abc"),
			Value: []byte("def")},
	}

	clientrequest := &proto.ClientRequest{Strategy: proto.ClientRequest_TRANSACTION.Enum(),
		Actions:   actions,
		ClusterId: pb.String("cluster1"),
		ClientId:  pb.String("client1")}

	newix, ae, crp := proc.createAERequest(clientrequest)

	if newix != 2 {
		t.Error("Wrong newix:", newix)
	}

	if ae == nil || len(ae.GetEntries()) != 2 {
		t.Error("Wrong AE:", ae)
	}

	if len(crp.GetResults()) != 3 || crp.GetStatus() != proto.ClientResponse_SUCCESS {
		t.Error("Wrong client response", crp)
	}

	proc.st.Commit(newix)
	t.Log(crp)

	clientrequest2 := &proto.ClientRequest{Strategy: proto.ClientRequest_TRANSACTION.Enum(),
		Actions:   actions,
		ClusterId: pb.String("cluster1"),
		ClientId:  pb.String("client2")}

	newix, ae, crp = proc.createAERequest(clientrequest2)

	if newix != 0 {
		t.Error("Wrong newix:", newix)
	}

	if ae != nil {
		t.Error("Wrong AE:", ae)
	}

	if len(crp.GetResults()) != 3 || crp.GetStatus() != proto.ClientResponse_FAILED {
		t.Error("Wrong client response", crp)
	}

	if crp.GetResults()[1].GetStatus() != proto.ActionResult_ERROR_LOCKED {
		t.Error("Wrong result status:", crp.GetResults()[1].GetStatus().String())
	}
	t.Log(crp)
}
