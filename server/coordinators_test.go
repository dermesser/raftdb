package server

import (
	"os"
	"raftdb/proto"
	"raftdb/server/locking"
	"raftdb/server/log"
	"raftdb/server/storage"
	"raftdb/server/versionedstorage"
	"testing"

	pb "github.com/gogo/protobuf/proto"
)

var lg, lg2 log.Log
var st, st2 storage.Storage
var vs, vs2 versionedstorage.VersionedStorage
var proc member

var entry *proto.Entry

func init() {
	parseFlags()
}

func setCTUp() {
	st = storage.NewTempStorage()
	st2 = storage.NewTempStorage()

	lg = log.NewStorageLog(st)
	lg2 = log.NewStorageLog(st2)
	vs = versionedstorage.NewVersionedStorage(locking.NewMNManager(), st, lg)
	vs2 = versionedstorage.NewVersionedStorage(locking.NewMNManager(), st2, lg2)

	vs.SetNextTerm("")
	vs2.SetNextTerm("")

	_proc, err := newProcessor(vs2, "_testcluster1", "1")

	if err != nil {
		panic(err.Error())
	}

	proc = _proc

	entry = &proto.Entry{Type: proto.Entry_PUT.Enum(),
		Key: []byte("abc"),
		Entry: &proto.DBEntry{Type: proto.DBEntry_DATA.Enum(),
			Value:   []byte("def"),
			Version: &proto.VersionInfo{Term: pb.Uint64(1), LogIx: (pb.Uint64(1))},
		},
	}
}

func tearCTDown() {
	os.RemoveAll("test1.db")
	os.RemoveAll("test2.db")
}

func TestReplicate1(t *testing.T) {
	setCTUp()
	defer tearCTDown()

	// Three entries.
	lg.Append(1, []*proto.Entry{entry}) // 1
	lg.Append(2, []*proto.Entry{entry}) // 2
	lg.Append(3, []*proto.Entry{entry}) // 3

	coord := &coordinator{st: vs, next_index: 4, match_index: 0,
		peer: proc}

	rq := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId: pb.String("1"),
		PreviousLogVersion: &proto.VersionInfo{Term: pb.Uint64(1),
			LogIx: pb.Uint64(3)},
		Entries: []*proto.Entry{entry}}

	_, err1 := coord.replicate(rq)

	if err1 == nil || err1.Error() != "wrn - non-matching index on member. retry" {
		t.Error("Wrong error returned:", err1)
	}

	_, err2 := coord.replicate(rq)

	if err2 != nil {
		t.Error("Second replicate returned error", err2)
	}
}

func TestReplicate2(t *testing.T) {

	setCTUp()
	defer tearCTDown()

	// Three entries.
	lg.Append(1, []*proto.Entry{entry}) // 1
	lg.Append(2, []*proto.Entry{entry}) // 2
	lg.Append(3, []*proto.Entry{entry}) // 3
	lg.Append(4, []*proto.Entry{entry}) // 4

	coord := &coordinator{st: vs, next_index: 4, match_index: 0,
		peer: proc}

	// Trigger sending a log of entries at first because we know of the index discrepancy
	lg.Append(5, []*proto.Entry{entry}) // 5

	rq := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId: pb.String("1"),
		PreviousLogVersion: &proto.VersionInfo{Term: pb.Uint64(1),
			LogIx: pb.Uint64(4)},
		Entries: []*proto.Entry{entry}}

	_, err1 := coord.replicate(rq)

	if err1 == nil || err1.Error() != "wrn - success = false when trying to send catch-up log entries" {
		t.Error("Wrong error returned:", err1)
	}

	_, err2 := coord.replicate(rq)

	if err2 != nil {
		t.Error("Second replicate returned error")
	}
}

// Tests that version information is ignored when only sending heartbeats
func TestReplicate3(t *testing.T) {
	setCTUp()
	defer tearCTDown()

	// Three entries.
	lg.Append(1, []*proto.Entry{entry}) // 1
	lg.Append(2, []*proto.Entry{entry}) // 2
	lg.Append(3, []*proto.Entry{entry}) // 3

	coord := &coordinator{st: vs, next_index: 4, match_index: 3,
		peer: proc}

	rq := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId: pb.String("1"),
		Entries:  []*proto.Entry{}}

	_, err1 := coord.replicate(rq)

	if err1 != nil {
		t.Error("Wrong error returned:", err1)
	}

}

func TestInstallSnapshot(t *testing.T) {
	setCTUp()
	defer tearCTDown()

	for i := 0; i < 1000; i++ {
		lg.Append(uint64(i+1), []*proto.Entry{entry})
		*entry.GetEntry().GetVersion().LogIx++
	}

	lg.SetCommitted(1000)
	st.Set(entry.GetKey(), entry.GetEntry().GetValue())

	coord := &coordinator{st: vs, next_index: 1, match_index: 0,
		peer: proc}

	rq := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId: pb.String("1"),
		PreviousLogVersion: &proto.VersionInfo{Term: pb.Uint64(1),
			LogIx: pb.Uint64(1000)},
		Entries: []*proto.Entry{entry}}

	_, err1 := coord.replicate(rq)

	if err1 == nil || err1.Error() != "Snapshot sent; now send entries" {
		t.Error("Unexpected error returned when sending snapshot:", err1.Error())
	}

	_, err2 := coord.replicate(rq)

	if err2 != nil {
		t.Error("Second call to replicate() did return an error:", err2.Error())
	}
}

func TestCoordRequestVote(t *testing.T) {
	setCTUp()
	defer tearCTDown()

	rq := &proto.RequestVote{ConfigVersion: pb.Uint64(1), Term: pb.Uint64(2)}

	coord := &coordinator{st: vs, next_index: 1, match_index: 0,
		peer: proc}

	rp, err := coord.RequestVote(rq)

	if err != nil {
		t.Error(err)
	}

	if !rp.GetVoteGranted() {
		t.Error("Vote was not granted")
	}
}
