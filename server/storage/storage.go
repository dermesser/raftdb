package storage

import "errors"

const STORAGE_KEY_PREFIX = "__RFTDBST."

type storageConstructor func(string) (Storage, error)

var storageBackends = map[string]storageConstructor{
	"leveldb": newLevelDBStorage,
}

type Iterator interface {
	Next() (key, value []byte)
	Close()
}

type Storage interface {
	Set(key, value []byte) error
	Get(key []byte) ([]byte, error)
	Delete(key []byte) error
	GetIterator(from, to []byte) Iterator
	// Delete everything.
	Reset() error
	Close()
}

type ReadOnlyStorage interface {
	Get(key []byte) ([]byte, error)
	GetIterator(from, to []byte) Iterator
}

// Type is one of (currently) [leveldb]
// name is a string that the storage backend can figure out which database to use.
func NewStorage(storage_type, name string) (Storage, error) {
	ctor, ok := storageBackends[storage_type]

	if !ok {
		return nil, errors.New("No such backend")
	}

	return ctor(name)
}
