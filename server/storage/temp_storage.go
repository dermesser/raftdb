package storage

import "errors"

// Implements a mock storage which is memory-resident

type tempStorage struct {
	st map[string][]byte
}

func NewTempStorage() Storage {
	return &tempStorage{st: make(map[string][]byte)}
}

func (ts *tempStorage) Set(k []byte, v []byte) error {
	ts.st[string(k)] = v
	return nil
}

func (ts *tempStorage) Close() {
	return
}

func (ts *tempStorage) Delete(k []byte) error {
	delete(ts.st, string(k))
	return nil
}

func (ts *tempStorage) Get(k []byte) ([]byte, error) {
	if r, ok := ts.st[string(k)]; ok {
		return r, nil
	} else {
		return nil, errors.New("not found")
	}
}

func (ts *tempStorage) Reset() error {
	ts.st = make(map[string][]byte)
	return nil
}

type tempIterator struct {
	st   map[string][]byte
	keys []string
	i    int
}

func (ti *tempIterator) Next() ([]byte, []byte) {
	if ti.i > len(ti.keys)-1 {
		return nil, nil
	}
	k, v := []byte(ti.keys[ti.i]), ti.st[ti.keys[ti.i]]
	ti.i++
	return k, v
}

func (ti *tempIterator) Close() {
	return
}

// The iterator doesn't respect bounds nor does it return ordered entries
func (ts *tempStorage) GetIterator(from, to []byte) Iterator {
	ks := make([]string, len(ts.st))
	var i int
	for k, _ := range ts.st {
		ks[i] = k
	}

	return &tempIterator{st: ts.st, keys: ks, i: 0}
}
