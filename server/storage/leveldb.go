package storage

import (
	"bytes"
	"os"

	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/iterator"
	"github.com/syndtr/goleveldb/leveldb/opt"
	"github.com/syndtr/goleveldb/leveldb/util"
)

var storage_key_prefix []byte = []byte(STORAGE_KEY_PREFIX)

// Implements storage.Storage
type levelDBStorageBackend struct {
	ldb      *leveldb.DB
	filename string

	wropt *opt.WriteOptions
	ropt  *opt.ReadOptions
}

func newLevelDBStorage(name string) (Storage, error) {
	db, err := leveldb.OpenFile(name, nil)

	if err != nil {
		return nil, err
	}

	return &levelDBStorageBackend{ldb: db, filename: name}, nil
}

func (db *levelDBStorageBackend) Set(key, value []byte) error {
	if !bytes.HasPrefix(key, storage_key_prefix) {
		return db.ldb.Put(append(storage_key_prefix, key...), value, db.wropt)
	} else {
		return db.ldb.Put(key, value, db.wropt)
	}
}

func (db *levelDBStorageBackend) Delete(key []byte) error {
	if !bytes.HasPrefix(key, storage_key_prefix) {
		return db.ldb.Delete(append(storage_key_prefix, key...), db.wropt)
	} else {
		return db.ldb.Delete(key, db.wropt)
	}
}

func (db *levelDBStorageBackend) Get(key []byte) ([]byte, error) {
	if !bytes.HasPrefix(key, storage_key_prefix) {
		return db.ldb.Get(append(storage_key_prefix, key...), db.ropt)
	} else {
		return db.ldb.Get(key, db.ropt)
	}
}

func (db *levelDBStorageBackend) Reset() error {
	db.ldb.Close()
	err := os.RemoveAll(db.filename)

	if err != nil {
		return err
	}

	db.ldb, err = leveldb.OpenFile(db.filename, nil)

	return err
}

func (db *levelDBStorageBackend) Close() {
	db.ldb.Close()
}

func (db *levelDBStorageBackend) GetIterator(from, to []byte) Iterator {
	if to == nil {
		to = append(storage_key_prefix, []byte("~~~~~~~~~~~~~")...)
	} else {
		to = append(storage_key_prefix, to...)
	}

	it := db.ldb.NewIterator(&util.Range{Start: append(storage_key_prefix, from...),
		Limit: to}, /* that's always the last string */ db.ropt)

	return &leveldbIterator{it: it}
}

// Implements storage.Iterator
type leveldbIterator struct {
	it iterator.Iterator
}

func (it *leveldbIterator) Next() (key, value []byte) {
	if it.it.Next() {
		splitup := bytes.Split(it.it.Key(), storage_key_prefix)
		var actual_key []byte

		if len(splitup) >= 2 {
			actual_key = splitup[1]
		} else {
			actual_key = splitup[0]
		}

		return actual_key, it.it.Value()
	} else {
		return nil, nil
	}
}

func (it *leveldbIterator) Close() {
	it.it.Release()
}
