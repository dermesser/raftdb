package server

import (
	monp "clustermon/proto"
	rpcs "clusterrpc/server"
	"errors"
	"raftdb/proto"
	"raftdb/server/constants"
	"raftdb/server/locking"
	"raftdb/server/log"
	"raftdb/server/storage"
	"raftdb/server/versionedstorage"
	"sync"
	"time"

	pb "github.com/gogo/protobuf/proto"
)

type processorEntry struct {
	ch   chan *RpcRequest
	nmgr *locking.MultiplexedNotificationManager
}

// Holds context and access to the processors of individual clusters
type RpcServer struct {
	mx  sync.Mutex
	srv *rpcs.Server
	// clusterid -> processor chan
	processors map[string]*processorEntry
}

func newRaftdbServer(rpcserver *rpcs.Server) *RpcServer {
	srv := &RpcServer{srv: rpcserver, processors: make(map[string]*processorEntry)}

	srv.registerHandlers()

	MONVARS.SetInt(monp.Namespace_RAFTDB, "processors", 0)
	MONVARS.SetMapInt(monp.Namespace_RAFTDB, "requests", "AppendEntries", 0)
	MONVARS.SetInt(monp.Namespace_RAFTDB, "hanging_requests", 0)
	MONVARS.SetMapInt(monp.Namespace_RAFTDB, "requests_per_processor", "", 0)
	MONVARS.SetMapInt(monp.Namespace_RAFTDB, "requests_sent_by_method", "", 0)
	MONVARS.SetMapInt(monp.Namespace_RAFTDB, "requests_sent_by_peer", "", 0)

	return srv
}

func (s *RpcServer) registerHandlers() {
	s.srv.RegisterHandler(constants.RPC_SERVICE, "ConfigChange", s.ConfigRequestHandler)
	s.srv.RegisterHandler(constants.RPC_SERVICE, "AppendEntries", s.AppendEntriesHandler)
	s.srv.RegisterHandler(constants.RPC_SERVICE, "RequestVote", s.RequestVoteHandler)
	s.srv.RegisterHandler(constants.RPC_SERVICE, "InstallSnapshot", s.InstallSnapshotHandler)
	s.srv.RegisterHandler(constants.RPC_SERVICE, "ClientRequest", s.ClientRequestHandler)
	s.srv.RegisterHandler(constants.RPC_SERVICE, "ClientHangingRequest", s.ClientHangingRequestHandler)
	s.srv.RegisterHandler("_CLUSTERMON", "Poll", MONVARS.RpcHandler)

}

// Sends a request to the given processor. Returns nil if the processor doesn't exist.
func (s *RpcServer) sendRequest(cluster_id string, rq *RpcRequest) *RpcResponse {
	s.mx.Lock()
	defer s.mx.Unlock()

	proc, ok := s.processors[cluster_id]

	if !ok {
		return nil
	}

	// Use a buffered channel so the processor doesn't block
	rpchan := make(chan *RpcResponse, 1)
	rq.rpchan = rpchan
	proc.ch <- rq

	t := time.NewTimer(FLAG_rpc_timeout)

	select {
	case rp := <-rpchan:
		t.Stop()
		return rp
	case <-t.C:
		return &RpcResponse{err: errors.New("Timed out waiting for response")}
	}

}

func (s *RpcServer) createProcessor(cluster_id string, cfgrq *proto.ConfigRequest) error {

	st, err := storage.NewStorage(FLAG_storage, cluster_id)

	if err != nil {
		raftdb_log(LOGLEVEL_ERRORS, "Could not set up storage for", cluster_id, ":", err.Error())
		return err
	}

	l := log.NewStorageLog(st)
	nm := locking.NewMNManager()
	vs := versionedstorage.NewVersionedStorage(nm, st, l)

	entry := &processorEntry{nmgr: nm}
	proc, err := newProcessor(vs, cluster_id, FLAG_member_id)

	if err != nil {
		return err
	}

	entry.ch = proc.requests

	if cfgrq != nil {
		proc.cluster.config.apply(cfgrq)
		proc.convertCandidate()
	} else {
		proc.convertFollower()
	}

	s.processors[cluster_id] = entry
	go proc.run()

	MONVARS.Add("processors", 1)

	return nil
}

func (srv *RpcServer) ConfigRequestHandler(ctx *rpcs.Context) {
	rpchan := make(chan *RpcResponse)
	rq := new(proto.ConfigRequest)

	if nil != ctx.GetArgument(rq) {
		ctx.Fail("Could not unmarshal argument")
		return
	}

	MONVARS.AddMap("requests", "ConfigRequest", 1)

	entry, ok := srv.processors[rq.GetClusterId()]

	if !ok {
		// ConfigRequest is also used to bootstrap a cluster (in which case a ConfigRequest is sent manually).
		// The first member to come up sends config requests to the other members, which will then start
		// their processors too.
		raftdb_log(LOGLEVEL_INFO, "Received CC for unknown cluster: Starting first processor", rq.GetClusterId())
		err := srv.createProcessor(rq.GetClusterId(), rq)

		if err != nil {
			raftdb_log(LOGLEVEL_ERRORS, "Could not start processor for new cluster", rq.GetClusterId())
			ctx.Fail("Could not start processor - " + err.Error())
			return
		}
		entry = srv.processors[rq.GetClusterId()]
	}

	entry.ch <- &RpcRequest{cc: rq, rpchan: rpchan}
	t := time.NewTimer(FLAG_rpc_timeout)

	select {
	case rp := <-rpchan:
		if rq.GetType() == proto.ConfigRequest_SHUTDOWN {
			srv.processors[rq.GetClusterId()].nmgr.Broadcast(locking.ShutDown)
			delete(srv.processors, rq.GetClusterId())
			MONVARS.Add("processors", -1)
		}

		if rp.cc != nil {
			ctx.Return(rp.cc)
		} else {
			ctx.Fail(rp.err.Error())
		}
		t.Stop()
	case <-t.C:
		ctx.Fail("Timed out")
	}
}

func (srv *RpcServer) AppendEntriesHandler(ctx *rpcs.Context) {
	rpchan := make(chan *RpcResponse)
	rq := new(proto.AppendEntriesRequest)

	if nil != ctx.GetArgument(rq) {
		raftdb_log(LOGLEVEL_WARNINGS, "Could not unmarshal AE request")
		ctx.Fail("Could not unmarshal argument")
	}

	MONVARS.AddMap("requests", "AppendEntries", 1)

	entry, ok := srv.processors[rq.GetClusterId()]

	if !ok {
		err := srv.createProcessor(rq.GetClusterId(), nil)

		if err != nil {
			ctx.Fail("Could not start processor - " + err.Error())
			return
		}
		entry = srv.processors[rq.GetClusterId()]
	}

	entry.ch <- &RpcRequest{ae: rq, rpchan: rpchan}

	t := time.NewTimer(FLAG_rpc_timeout)

	select {
	case rp := <-rpchan:
		if rp.ae != nil {
			ctx.Return(rp.ae)
		} else {
			ctx.Fail(rp.err.Error())
		}
		t.Stop()
	case <-t.C:
		ctx.Fail("Timed out")
	}
}

func (srv *RpcServer) RequestVoteHandler(ctx *rpcs.Context) {
	rq := new(proto.RequestVote)

	if nil != ctx.GetArgument(rq) {
		raftdb_log(LOGLEVEL_WARNINGS, "Could not unmarshal RV request")
		ctx.Fail("Could not unmarshal argument")
		return
	}

	MONVARS.AddMap("requests", "RequestVote", 1)

	_, ok := srv.processors[rq.GetClusterId()]

	if !ok {
		err := srv.createProcessor(rq.GetClusterId(), nil)

		if err != nil {
			ctx.Fail("Could not start processor - " + err.Error())
			return
		}
	}

	rp := srv.sendRequest(rq.GetClusterId(), &RpcRequest{rqv: rq})

	if rp.err != nil {
		raftdb_log(LOGLEVEL_ERRORS, "Returning error to RPC caller:", rp.err.Error())
		ctx.Fail(rp.err.Error())
	}

	if rp.v != nil {
		ctx.Return(rp.v)
	}
}

func (srv *RpcServer) InstallSnapshotHandler(ctx *rpcs.Context) {
	rq := new(proto.InstallSnapshot)

	if nil != ctx.GetArgument(rq) {
		raftdb_log(LOGLEVEL_WARNINGS, "Could not unmarshal IS request")
		ctx.Fail("Could not unmarshal argument")
		return
	}

	MONVARS.AddMap("requests", "InstallSnapshot", 1)

	rp := srv.sendRequest(rq.GetClusterId(), &RpcRequest{is: rq})

	if rp.err != nil {
		raftdb_log(LOGLEVEL_ERRORS, "Returning error to RPC caller:", rp.err.Error())
	}

	if rp.is == nil {
		rp.is = &proto.InstallSnapshotResponse{Status: proto.InstallSnapshotResponse_NO_CLUSTER.Enum(), Term: pb.Uint64(0)}
	}

	ctx.Return(rp.is)
}
