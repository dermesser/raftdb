package log

import (
	"fmt"
	"raftdb/proto"
	"raftdb/server/storage"
	"sync"

	pb "github.com/gogo/protobuf/proto"
	"github.com/syndtr/goleveldb/leveldb/util"
)

const INDEX_INFO_KEY = "___RAFTDB_INTERNAL_IXINFO"
const PERSISTENT_KEY = "___RAFTDB_INTERNAL_PERSIST_STATE"

const LOG_ENTRY_PREFIX = "_RLG"

func getKey(index uint64) []byte {
	return []byte(fmt.Sprintf("%s::%0.9d", LOG_ENTRY_PREFIX, index))
}

// Implements Log interfaces on top of the Storage interface.

type storageLog struct {
	storage                                      storage.Storage
	first_index, next_log_index, committed_index uint64
	mx                                           sync.Mutex
}

func NewStorageLog(st storage.Storage) Log {
	log := &storageLog{storage: st}

	ixinfo_raw, err := st.Get([]byte(INDEX_INFO_KEY))

	if err != nil {
		log.first_index = 0
		log.next_log_index = 1
		log.committed_index = 0
	} else {
		ixinfo := new(proto.IndexState)
		ixinfo.Unmarshal(ixinfo_raw)

		log.first_index = ixinfo.GetFirstIndex()
		log.next_log_index = ixinfo.GetLogIndex()
		log.committed_index = ixinfo.GetCommittedIndex()
	}

	term, _ := log.GetPersistentState()

	if term == 0 {
		// Log is new, set 0 as first term. When the processor receives
		// the initial config change, it will request a vote for term+1,
		// i.e. the initial term will be 1.
		log.SetPersistentState(0, "")
		log.saveIndex()
	}

	zero_entry := &proto.Entry{Type: proto.Entry_UNDEFINED.Enum(), Key: []byte("__"),
		Entry: &proto.DBEntry{Type: proto.DBEntry_UNDEFINED.Enum(), Value: []byte("__"), Version: &proto.VersionInfo{Term: pb.Uint64(1), LogIx: (pb.Uint64(0))}}}

	e, err := zero_entry.Marshal()

	if err == nil {
		st.Set(getKey(0), e)
	} else {
		return nil
	}
	return log
}

func (log *storageLog) saveIndex() {
	log.mx.Lock()
	defer log.mx.Unlock()

	ixinfo := new(proto.IndexState)
	ixinfo.CommittedIndex = pb.Uint64(log.committed_index)
	ixinfo.LogIndex = pb.Uint64(log.next_log_index)
	ixinfo.FirstIndex = pb.Uint64(log.first_index)

	b, err := ixinfo.Marshal()

	if err == nil {
		log.storage.Set([]byte(INDEX_INFO_KEY), b)
	}
}

func (log *storageLog) GetIndex() (logix, commitix, logstartix uint64) {
	log.mx.Lock()
	defer log.mx.Unlock()

	logix = log.next_log_index
	commitix = log.committed_index
	logstartix = log.first_index

	return
}

func (log *storageLog) SetPersistentState(term uint64, voted_for string) {
	log.mx.Lock()
	defer log.mx.Unlock()

	state := &proto.PersistentState{Term: pb.Uint64(term), VotedFor: (pb.String(voted_for))}

	value, err := state.Marshal()

	if err == nil {
		log.storage.Set([]byte(PERSISTENT_KEY), value)
	}
}

// Returns current term and votedFor
func (log *storageLog) GetPersistentState() (uint64, string) {
	log.mx.Lock()
	defer log.mx.Unlock()

	state := &proto.PersistentState{}

	value, err := log.storage.Get([]byte(PERSISTENT_KEY))

	if err == nil {
		state.Unmarshal(value)
		return state.GetTerm(), state.GetVotedFor()
	}
	return 0, ""
}

func (log *storageLog) SetCommitted(ix uint64) {
	next, _, _ := log.GetIndex()

	log.mx.Lock()
	defer log.mx.Unlock()

	if ix > log.committed_index && ix < next {
		log.committed_index = ix

		log.mx.Unlock()
		log.saveIndex()
		log.mx.Lock()
	}
}

func (log *storageLog) ResetNext(ix uint64) {
	log.mx.Lock()
	defer log.mx.Unlock()

	log.next_log_index = ix

	if ix < log.committed_index {
		log.committed_index = ix
	}

	log.mx.Unlock()
	log.saveIndex()
	log.mx.Lock()
}

// The LogIx field in Entry.DBEntry.VersionInfo is populated here.
// Modifies the given argument protobufs with the new versions!
// May only be called if the first entry's index is equal to next_log_index. Otherwise
// this misbehaves and corrupts state.
// Returns the new nextix
func (log *storageLog) Append(next uint64, entries []*proto.Entry) uint64 {
	values := make([][]byte, len(entries))
	var err error

	log.ResetNext(next)

	term, _ := log.GetPersistentState()

	// Set version info in entries and marshal them so we can catch errors before modifying anything
	for i, v := range entries {
		if v.GetEntry().GetVersion() == nil {
			v.GetEntry().Version = new(proto.VersionInfo)
		}

		v.GetEntry().GetVersion().LogIx = pb.Uint64(log.next_log_index + uint64(i))

		// Only set the term if it's undefined. Otherwise this would set the wrong term
		// on incoming entries where the AE request has a newer term than the entry that is
		// appended here (because the new term is set first in the AppendEntries handler)
		if v.GetEntry().GetVersion().GetTerm() == 0 {
			v.GetEntry().GetVersion().Term = pb.Uint64(term)
		}

		values[i], err = v.Marshal()

		// Marshal error! Can't handle this properly.
		if err != nil {
			return 0
		}
	}

	log.mx.Lock()
	defer log.mx.Unlock()

	// Marshaled without errors, now write and modify state.
	for _, v := range values {
		key := getKey(log.next_log_index)

		log.storage.Set(key, v)
		log.next_log_index++
	}

	if log.first_index == 0 {
		log.first_index = 1
	}

	log.mx.Unlock()
	log.saveIndex()
	log.mx.Lock()

	return log.next_log_index
}

func (log *storageLog) GetLogEntry(ix uint64) *proto.Entry {
	log.mx.Lock()
	defer log.mx.Unlock()

	val, err := log.storage.Get(getKey(ix))

	if err != nil {
		return nil
	}

	entry := new(proto.Entry)

	err = entry.Unmarshal(val)

	if err != nil {
		fmt.Println(err.Error())
		return nil
	}

	return entry
}

func (log *storageLog) Delete(to uint64) uint64 {
	log.mx.Lock()
	defer log.mx.Unlock()

	if to > log.next_log_index {
		return log.first_index
	}

	for i := log.first_index; i <= to; i++ {
		log.storage.Delete(getKey(i))
	}
	log.first_index = to + 1

	log.mx.Unlock()
	log.saveIndex()
	log.mx.Lock()

	return log.first_index
}

func (log *storageLog) Length() uint64 {
	log.mx.Lock()
	defer log.mx.Unlock()

	return log.next_log_index - log.first_index
}

func (log *storageLog) Close() {

}

func (log *storageLog) Traverse(from, to uint64) LogIterator {
	rng := &util.Range{Start: getKey(from),
		Limit: getKey(to + 1)}

	return &storageLogIterator{it: log.storage.GetIterator(rng.Start, rng.Limit)}
}

type storageLogIterator struct {
	it storage.Iterator
}

func (sli *storageLogIterator) Next() *proto.Entry {
	_, v := sli.it.Next()

	if v == nil {
		return nil
	}

	entry := new(proto.Entry)
	err := entry.Unmarshal(v)

	if err != nil {
		return nil
	}

	return entry
}
