package log

import (
	"raftdb/proto"
)

type LogIterator interface {
	Next() *proto.Entry // or nil when finished
}

// Implementers need to handle concurrency
type Log interface {
	GetIndex() (next_logix, commitix, logstartix uint64)
	SetCommitted(uint64)
	ResetNext(uint64)
	SetPersistentState(term uint64, votedFor string)
	GetPersistentState() (uint64, string)
	// Auto-fills in current index/term information, returns next_index
	Append(next uint64, entries []*proto.Entry) (new_logix uint64)
	GetLogEntry(uint64) *proto.Entry
	// Implements a safety mechanism to prevent deleting the whole log: Will not delete the last
	// 20 entries. Returns the (now) oldest index in the log.
	Delete(to uint64) uint64
	// Returns the physical length of the log, i.e. next_ix - 1 - first_ix
	Length() uint64
	Traverse(from, to uint64) LogIterator
	Close()
}

type ReadOnlyLog interface {
	GetIndex() (next_logix, commitix, logstartix uint64)
	GetPersistentState() (uint64, string)
	GetLogEntry(uint64) *proto.Entry
	// Returns the physical length of the log.
	Length() uint64
	Traverse(from, to uint64) LogIterator
}
