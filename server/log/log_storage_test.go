package log

import (
	"bytes"
	"os"
	"raftdb/proto"
	"raftdb/server/storage"
	"testing"
)

var log Log

var entry1 *proto.Entry = &proto.Entry{Type: proto.Entry_PUT.Enum(),
	Key: []byte("abc"),
	Entry: &proto.DBEntry{Type: proto.DBEntry_DATA.Enum(),
		Value: []byte("def")},
}

var entry2 *proto.Entry = &proto.Entry{Type: proto.Entry_PUT.Enum(),
	Key: []byte("123"),
	Entry: &proto.DBEntry{Type: proto.DBEntry_DATA.Enum(),
		Value: []byte("456")},
}

func setUp() {
	os.RemoveAll("test_leveldb.db")
	// Can't use TempStorage here because we need a well-working iterator
	st, err := storage.NewStorage("leveldb", "test_leveldb.db")

	if err != nil {
		panic(err.Error())
	}

	log = NewStorageLog(st)
}

func TestAppendGet(t *testing.T) {
	setUp()
	log.SetPersistentState(1, "")
	log.Append(1, []*proto.Entry{entry1})

	if entry1.GetEntry().GetVersion().GetTerm() != 1 ||
		entry1.GetEntry().GetVersion().GetLogIx() != 1 {
		t.Error("Append() did not amend version information", entry1.GetEntry().GetVersion())
	}

	entry := log.GetLogEntry(1)

	if !bytes.Equal(entry.GetKey(), entry1.GetKey()) || entry.GetType() != entry1.GetType() ||
		!bytes.Equal(entry.GetEntry().GetValue(), entry1.GetEntry().GetValue()) {
		t.Error("Wrong data returned ")
	}
}

func TestIndexCorrectness(t *testing.T) {
	setUp()

	log.Append(1, []*proto.Entry{entry1})

	entry := log.GetLogEntry(1)

	if !bytes.Equal(entry.GetKey(), entry1.GetKey()) || entry.GetType() != entry1.GetType() ||
		!bytes.Equal(entry.GetEntry().GetValue(), entry1.GetEntry().GetValue()) {
		t.Error("Wrong data returned ")
	}

	nextix, commitix, logstartix := log.GetIndex()

	if nextix != 2 || commitix != 0 || logstartix != 1 {
		t.Error("Wrong indices present")
	}
}

func TestPersistentData(t *testing.T) {
	setUp()

	log.SetPersistentState(34, "abc")

	term, votedFor := log.GetPersistentState()

	if term != 34 || votedFor != "abc" {
		t.Error("Wrong persistent data was returned")
	}
}

func TestLength(t *testing.T) {
	setUp()

	log.Append(1, []*proto.Entry{entry1})

	entry := log.GetLogEntry(1)

	if !bytes.Equal(entry.GetKey(), entry1.GetKey()) || entry.GetType() != entry1.GetType() ||
		!bytes.Equal(entry.GetEntry().GetValue(), entry1.GetEntry().GetValue()) {
		t.Error("Wrong data returned ")
	}

	if log.Length() != 1 {
		t.Error("Wrong length returned")
	}
}

func TestDelete(t *testing.T) {
	setUp()

	var j uint64 = 1

	for i := 0; i < 100; i++ {
		log.Append(j, []*proto.Entry{entry1})
		log.Append(j+1, []*proto.Entry{entry2})
		log.Append(j+2, []*proto.Entry{entry1, entry2})
		j += 4
	}

	if log.Length() != 400 {
		t.Error("Precondition: Wrong length", log.Length())
	}

	new_first_ix := log.Delete(200)

	result := log.GetLogEntry(1)

	if result != nil {
		t.Error("Delete did not work")
	}

	if new_first_ix != 201 {
		t.Error("Returned wrong new first index")
	}

	_, _, startix := log.GetIndex()

	if startix != 201 {
		t.Error("Wrong start_ix")
	}
}

func TestSetCommitted(t *testing.T) {
	setUp()

	log.SetCommitted(234)

	_, commit, _ := log.GetIndex()

	// Test protection - commitix !> lastix
	if commit == 234 {
		t.Error("Wrong commit index returned")
	}

	// Append entry and then commit it
	log.Append(1, []*proto.Entry{entry1})
	log.SetCommitted(1)
	_, commit, _ = log.GetIndex()

	// Test protection - commitix !> lastix
	if commit != 1 {
		t.Error("Wrong commit index returned")
	}
}

func TestTraverse(t *testing.T) {
	setUp()

	var j uint64 = 1

	for i := 0; i < 100; i++ {
		log.Append(j, []*proto.Entry{entry1})
		log.Append(j+1, []*proto.Entry{entry2})
		j += 2
	}

	if log.Length() != 200 {
		t.Error("Precondition: Wrong length")
	}

	it := log.Traverse(1, 200)
	i := 0

	for entry := it.Next(); entry != nil; entry = it.Next() {
		i++

		if i%2 == 1 {
			if !bytes.Equal(entry.GetKey(), entry1.GetKey()) {
				t.Error("Wrong key in odd entry", i, string(entry.GetKey()), "vs", string(entry1.GetKey()))
			}
		}
		if i%2 == 0 {
			if !bytes.Equal(entry.GetKey(), entry2.GetKey()) {
				t.Error("Wrong key in even entry", i, string(entry.GetKey()), "vs", string(entry2.GetKey()))
			}
		}
	}

	if i != 200 {
		t.Error("Not correctly traversed:", i)
	}
}
