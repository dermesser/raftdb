package server

import (
	rpcc "clusterrpc/client"
	smgr "clusterrpc/securitymanager"
	"raftdb/proto"
	"raftdb/server/constants"
)

// Represents the general interface provided by servers
// Either internally (as interface to coordinators) as well as externally (as RPC stub)
type member interface {
	// Errors are returned for network errors etc, not for app errors.

	// The first AppendEntries has to have term = 1, previousLogTerm = 1, previousLogIx = 0
	AppendEntries(*proto.AppendEntriesRequest) (*proto.AppendEntriesResponse, error)
	RequestVote(*proto.RequestVote) (*proto.Vote, error)
	InstallSnapshot(*proto.InstallSnapshot) (*proto.InstallSnapshotResponse, error)
}

// Provides the RPC interface via internal channels for concurrent processing
// e.g. handler->processor, processor->coordinator
type internalMember struct {
	ch        chan *RpcRequest
	member_id string
}

func newInternalMember(ch chan *RpcRequest, member_id string) *internalMember {
	return &internalMember{ch: ch, member_id: member_id}
}

func (m *internalMember) AppendEntries(rq *proto.AppendEntriesRequest) (*proto.AppendEntriesResponse, error) {
	rpchan := make(chan *RpcResponse)

	select {
	case m.ch <- &RpcRequest{ae: rq, rpchan: rpchan}:
		//
	default:
		raftdb_log(LOGLEVEL_ERRORS, "Dropped AE request to", m.member_id, "because of full channel")
	}

	rp := <-rpchan
	close(rpchan)
	return rp.ae, rp.err
}

func (m *internalMember) RequestVote(rq *proto.RequestVote) (*proto.Vote, error) {
	rpchan := make(chan *RpcResponse)

	select {
	case m.ch <- &RpcRequest{rqv: rq, rpchan: rpchan}:
	//
	default:
		raftdb_log(LOGLEVEL_ERRORS, "Dropped RV request to", m.member_id, "because of full channel")
	}

	rp := <-rpchan
	close(rpchan)
	return rp.v, rp.err
}

func (m *internalMember) InstallSnapshot(rq *proto.InstallSnapshot) (*proto.InstallSnapshotResponse, error) {
	rpchan := make(chan *RpcResponse)

	select {
	case m.ch <- &RpcRequest{is: rq, rpchan: rpchan}:
	//
	default:
		raftdb_log(LOGLEVEL_ERRORS, "Dropped IS request to", m.member_id, "because of full channel")
	}

	rp := <-rpchan
	close(rpchan)
	return rp.is, rp.err
}

// Interface to remote (networked) cluster members
type remoteMember struct {
	client       *rpcc.Client
	name, selfid string
	secmgr       *smgr.ClientSecurityManager
}

func newRemoteMember(self_member_id, name string) (member, error) {
	task, _, err := LOOKUP_CLIENT.LookupTask(name+constants.CLKUP_MEMBER_SUFFIX, 0)

	if err != nil {
		raftdb_log(LOGLEVEL_ERRORS, "Could not lookup task for", name, ":", err.Error())
		return nil, err
	}

	secmgr := smgr.NewClientSecurityManager()
	secmgr.SetServerPubkey(task.GetPublicKey())

	m := &remoteMember{selfid: self_member_id, name: name, secmgr: secmgr}

	channel, err := rpcc.NewRpcChannel(secmgr)

	if err != nil {
		raftdb_log(LOGLEVEL_ERRORS, "Could not create channel:", err.Error())
		return nil, err
	}

	err = channel.Connect(rpcc.Peer(task.GetTaskName(), uint(task.GetPort())))

	if err != nil {
		raftdb_log(LOGLEVEL_ERRORS, "Could not connect channel:", err.Error())
		return nil, err
	}

	client := rpcc.NewClient(self_member_id+"->"+name, channel)
	m.client = &client
	m.client.SetTimeout(FLAG_rpc_timeout, false)

	if err != nil {
		raftdb_log(LOGLEVEL_ERRORS, "Could not connect to task", name, ":", err.Error())
		return nil, err
	}

	return m, nil
}

func (m *remoteMember) AppendEntries(rq *proto.AppendEntriesRequest) (*proto.AppendEntriesResponse, error) {
	rp := new(proto.AppendEntriesResponse)

	raftdb_log(LOGLEVEL_DEBUG, "Sending AE request to", m.name)
	err := m.client.RequestProtobuf(rq, rp, constants.RPC_SERVICE, "AppendEntries", nil)

	MONVARS.AddMap("requests_sent_by_method", "AppendEntries", 1)
	MONVARS.AddMap("requests_sent_by_peer", m.name, 1)

	if err != nil {
		raftdb_log(LOGLEVEL_WARNINGS, "Could not send AppendEntries request to", m.name, ":", err.Error())

		m.client.Destroy()
		rm, err := newRemoteMember(m.selfid, m.name)

		if err != nil {
			raftdb_log(LOGLEVEL_ERRORS, "Could not reconnect to member", m.name)
			return nil, err
		} else {
			*m = *rm.(*remoteMember)
			err := m.client.RequestProtobuf(rq, rp, constants.RPC_SERVICE, "AppendEntries", nil)

			if err != nil {
				raftdb_log(LOGLEVEL_ERRORS, "Second try did not work (to", m.name, "):", err.Error())
				return nil, err
			}
		}
	}
	return rp, nil
}

func (m *remoteMember) RequestVote(rq *proto.RequestVote) (*proto.Vote, error) {
	rp := new(proto.Vote)

	raftdb_log(LOGLEVEL_DEBUG, "Sending RV request to", m.name)
	err := m.client.RequestProtobuf(rq, rp, constants.RPC_SERVICE, "RequestVote", nil)

	MONVARS.AddMap("requests_sent_by_method", "RequestVote", 1)
	MONVARS.AddMap("requests_sent_by_peer", m.name, 1)

	if err != nil {
		raftdb_log(LOGLEVEL_WARNINGS, "Could not send RequestVote request to", m.name, ":", err.Error())

		rm, err := newRemoteMember(m.selfid, m.name)
		*m = *rm.(*remoteMember)

		if err != nil {
			raftdb_log(LOGLEVEL_ERRORS, "Could not reconnect to member", m.name, ":", err.Error())
			return nil, err
		} else {
			err := m.client.RequestProtobuf(rq, rp, constants.RPC_SERVICE, "RequestVote", nil)

			if err != nil {
				raftdb_log(LOGLEVEL_ERRORS, "Second try did not work (to", m.name, "):", err.Error())
				return nil, err
			}
		}
	}
	return rp, nil
}

func (m *remoteMember) InstallSnapshot(rq *proto.InstallSnapshot) (*proto.InstallSnapshotResponse, error) {
	rp := new(proto.InstallSnapshotResponse)

	raftdb_log(LOGLEVEL_DEBUG, "Sending IS request to", m.name)
	err := m.client.RequestProtobuf(rq, rp, constants.RPC_SERVICE, "InstallSnapshot", nil)

	MONVARS.AddMap("requests_sent_by_method", "InstallSnapshot", 1)
	MONVARS.AddMap("requests_sent_by_peer", m.name, 1)

	if err != nil {
		raftdb_log(LOGLEVEL_WARNINGS, "Could not send InstallSnapshot request to", m.name, ":", err.Error())

		rm, err := newRemoteMember(m.selfid, m.name)
		*m = *rm.(*remoteMember)

		if err != nil {
			raftdb_log(LOGLEVEL_ERRORS, "Could not reconnect to member", m.name, ":", err.Error())
			return nil, err
		} else {
			err := m.client.RequestProtobuf(rq, rp, constants.RPC_SERVICE, "InstallSnapshot", nil)

			if err != nil {
				raftdb_log(LOGLEVEL_ERRORS, "Second try did not work (to", m.name, "):", err.Error())
				return nil, err
			}
		}
	}
	return rp, nil
}
