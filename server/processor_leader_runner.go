package server

import (
	"errors"
	"raftdb/proto"
	"raftdb/server/constants"
	"raftdb/server/locking"
	"raftdb/server/versionedstorage"
	"time"

	pb "github.com/gogo/protobuf/proto"
)

func (p *processor) getExternalMajority() int {
	external_majority := ((len(p.cluster.config.getMembers()) - 1) / 2)
	raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "external majority", external_majority, p.cluster.config)

	// If we have 3 other members, the majority has to be 3 out of 4, i.e. us and 3//2 + 1
	if (len(p.cluster.config.getMembers())-1)%2 == 1 {
		external_majority++
	}

	return external_majority
}

func (p *processor) runLeader() {
	raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "Running leader")

	rq := <-p.requests

	if rq.ignore {
		return
	}

	if rq.ae != nil {
		p.runLeaderAppendEntries(rq)
	} else if rq.rqv != nil {
		p.runLeaderRequestVote(rq)
	} else if rq.cc != nil {
		p.runLeaderConfigRequest(rq)
	} else if rq.cr != nil {
		p.runLeaderClientRequest(rq)
	} else {
		rq.rpchan <- &RpcResponse{err: errors.New("Unknown method called")}
	}
}

func (p *processor) runLeaderAppendEntries(rq *RpcRequest) {
	raftdb_log(LOGLEVEL_WARNINGS, p.cluster.getClusterId(), "Leader received AE although we're leader (ERR_NOT_FOLLOWER)")
	raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "Erroneous AE message:", rq.ae)

	term := p.st.Term()
	nextix, _, _ := p.st.Index()
	if rq.ae.GetTerm() > term {
		p.requests <- rq
		p.convertFollower()
		rq.rpchan <- &RpcResponse{ae: &proto.AppendEntriesResponse{Success: pb.Bool(true), Term: rq.ae.Term, LastIndex: pb.Uint64(nextix - 1)}}
		return
	}

	rq.rpchan <- &RpcResponse{ae: &proto.AppendEntriesResponse{Success: pb.Bool(false), Term: pb.Uint64(term)}, err: errors.New(constants.ERR_NOT_FOLLOWER)}
}

func (p *processor) runLeaderRequestVote(rq *RpcRequest) {
	raftdb_log(LOGLEVEL_INFO, p.cluster.getClusterId(), "Leader received RV")
	raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "RV message:", rq.rqv)

	rp, err := p.RequestVote(rq.rqv)

	rq.rpchan <- &RpcResponse{v: rp, err: err}
}

func (p *processor) runLeaderConfigRequest(rq *RpcRequest) {
	raftdb_log(LOGLEVEL_INFO, p.cluster.getClusterId(), "Leader received CC message")
	raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "CC message:", rq.cc)

	if rq.cc.GetType() == proto.ConfigRequest_GET_CONFIG {
		cfg := p.cluster.config.toConfig()
		rq.rpchan <- &RpcResponse{cc: &proto.ConfigResponse{Status: proto.ConfigResponse_OK.Enum(), Config: cfg}}

		return
	}

	cfg_entry, oldcfg, err := p.prepareConfigRequest(rq.cc)

	if err != nil {
		rq.rpchan <- &RpcResponse{cc: &proto.ConfigResponse{Status: proto.ConfigResponse_ERROR.Enum()},
			err: err}
		return
	}

	external_majority := (len(p.cluster.config.getMembers()) / 2)
	raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "external majority for config change:", external_majority)

	// Fill AppendEntries request
	nextix, commitix, _ := p.st.Index()

	new_lease := time.Now().Add(FLAG_lease)

	aerq := &proto.AppendEntriesRequest{Entries: []*proto.Entry{cfg_entry}, LeaderCommitIx: pb.Uint64(commitix),
		LeaderLease: pb.Uint64(uint64(new_lease.UnixNano()))}

	if previous_entry := p.st.GetLogEntry(nextix - 1); previous_entry != nil && previous_entry.GetEntry() != nil {
		aerq.PreviousLogVersion = previous_entry.GetEntry().Version
	}

	raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "Sending AE message to members for config change:", aerq)

	rps := p.cluster.sendToAll(&RpcRequest{ae: aerq}, external_majority)

	var valid_rps int
	for _, rp := range rps {
		if rp.err == nil && rp.ae.GetSuccess() {
			valid_rps++
		} else {
			raftdb_log(LOGLEVEL_WARNINGS, "Negative config AE response:", rp.ae, rp.err)
		}
	}

	raftdb_log(LOGLEVEL_DEBUG, "Received", valid_rps, "valid (positive) responses for config AE")

	if valid_rps >= external_majority {
		newix, _ := p.st.Prepare(versionedstorage.Entry2Op(cfg_entry))

		raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "Committing", newix)
		p.st.Commit(newix)
		p.leader_lease = new_lease

		p.commitConfigRequest(rq.cc)

		if rq.cc.GetType() == proto.ConfigRequest_SHUTDOWN {

			p.processor_state = state_FINALIZE
			p.shutDownLeader()
		}

		raftdb_log(LOGLEVEL_INFO, p.cluster.getClusterId(), "Committed config change entry")

		rq.rpchan <- &RpcResponse{cc: &proto.ConfigResponse{Status: proto.ConfigResponse_OK.Enum()}}
	} else {
		p.revertConfigRequest(oldcfg)
		rq.rpchan <- &RpcResponse{err: errors.New(constants.ERR_NO_MAJORITY)}
	}
}

func (p *processor) runLeaderClientRequest(rq *RpcRequest) {
	raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "Leader received CR:", rq.cr)

	newix, aerq, crp := p.createAERequest(rq.cr)

	if aerq == nil && crp.GetStatus() != proto.ClientResponse_SUCCESS {
		rq.rpchan <- &RpcResponse{cr: crp}
		return
	}

	// i.e., if this is a GET request and we have enough of the lease left (more than 25%) => return fast
	if len(aerq.GetEntries()) == 0 &&
		crp.GetStatus() == proto.ClientResponse_SUCCESS &&
		p.leader_lease.Sub(time.Now()) > time.Duration(0.25*float64(FLAG_lease)) {

		rq.rpchan <- &RpcResponse{cr: crp}
		raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "Returning fast with", crp, "(", p.leader_lease.Sub(time.Now()), "left of the lease)")
		return
	}
	// else: Use empty AErq, which is a GET, to get consensus and then return

	new_lease := time.Now().Add(FLAG_lease)
	aerq.LeaderLease = pb.Uint64(uint64(new_lease.UnixNano()))

	external_majority := p.getExternalMajority()
	raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "Sending AE request to at least", external_majority, "followers:", aerq)
	rps := p.cluster.sendToAll(&RpcRequest{ae: aerq}, external_majority)

	var valid_rps int
	for _, rp := range rps {
		if rp.err == nil && rp.ae.GetSuccess() {
			valid_rps++
		}
	}

	if valid_rps >= external_majority {
		raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "Received responses from majority for AE")

		p.st.Commit(newix)
		p.leader_lease = new_lease

		raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "Responding with", crp)
		rq.rpchan <- &RpcResponse{cr: crp}
		return
	} else {
		p.st.Revert()
		raftdb_log(LOGLEVEL_WARNINGS, p.cluster.getClusterId(), "No majority achieved for AE:", valid_rps, "/", external_majority)
		rq.rpchan <- &RpcResponse{err: errors.New(constants.ERR_NO_MAJORITY)}
		return
	}
}

func (p *processor) createAERequest(cr *proto.ClientRequest) (uint64, *proto.AppendEntriesRequest, *proto.ClientResponse) {
	results := make([]*proto.ActionResult, len(cr.GetActions()))
	actions_to_do := []*proto.Action{}

	var succeeded bool = true

	// If one wouldn't succeed, cut it short and fail the complete request.
	if cr.GetStrategy() == proto.ClientRequest_TRANSACTION {
		for i := range cr.GetActions() {
			// Already check if (un)lock would be successful. If not, reply immediately.
			// This saves us handling errors etc. from the ClientRequest Op.

			if cr.GetActions()[i].GetType() == proto.Action_GET {
				results[i] = p.st.ActionGet(cr.GetActions()[i])
			} else {
				will_succeed, res := p.st.WillSucceed(cr.GetActions()[i], cr.GetClientId())

				if res != nil {
					results[i] = res
				} else {
					results[i] = new(proto.ActionResult)
					results[i].Status = proto.ActionResult_UNDEFINED.Enum()
				}

				if !will_succeed {
					succeeded = false
					raftdb_log(LOGLEVEL_DEBUG, "Client action (TA) failed:", res.String())
				} else {
					actions_to_do = append(actions_to_do, cr.GetActions()[i])
				}
			}

		}
	} else if cr.GetStrategy() == proto.ClientRequest_BATCH { // If one doesn't succeed still try the others.
		for i := range cr.GetActions() {
			if cr.GetActions()[i].GetType() == proto.Action_GET {
				results[i] = p.st.ActionGet(cr.GetActions()[i])
			} else {
				will_succeed, res := p.st.WillSucceed(cr.GetActions()[i], cr.GetClientId())

				if res != nil {
					results[i] = res
				} else {
					results[i] = new(proto.ActionResult)
					results[i].Status = proto.ActionResult_UNDEFINED.Enum()
				}

				if !will_succeed {
					raftdb_log(LOGLEVEL_DEBUG, "Client action (B) failed:", res.String())
				} else {
					actions_to_do = append(actions_to_do, cr.GetActions()[i])
				}
			}
		}
	}

	if !succeeded {
		return 0, nil, &proto.ClientResponse{Status: proto.ClientResponse_FAILED.Enum(), Results: results}
	}

	aerq := new(proto.AppendEntriesRequest)
	term := p.st.Term()
	nextix, commitix, _ := p.st.Index()
	previous_entry := p.st.GetLogEntry(nextix - 1)

	aerq.Term = pb.Uint64(term)
	aerq.ClusterId = pb.String(p.cluster.getClusterId())
	aerq.LeaderId = pb.String(p.member_id)
	aerq.LeaderCommitIx = pb.Uint64(commitix)
	aerq.PreviousLogVersion = previous_entry.GetEntry().GetVersion()

	// Return an empty AErq that can be used for getting consensus for a GET
	if len(actions_to_do) == 0 {
		return 0, aerq, &proto.ClientResponse{Status: proto.ClientResponse_SUCCESS.Enum(), Results: results}
	}

	// when prepared, ae_op returns the entries for this AppendEntries request
	ae_op := versionedstorage.Actions2Op(actions_to_do, cr.GetClientId())
	newix, entries := p.st.Prepare(ae_op)
	aerq.Entries = entries

	actions_ix := 0

	// A nil result corresponds to a mutating action that has a corresponding Entry.
	// Fill in the action result from the entry that we got.

	for i := range results {
		if results[i].GetStatus() == proto.ActionResult_UNDEFINED {
			results[i] = new(proto.ActionResult)
			results[i].Status = proto.ActionResult_SUCCESS.Enum()
			results[i].CommitIx = entries[actions_ix].GetEntry().GetVersion().LogIx
			results[i].CounterResult = entries[actions_ix].GetEntry().CounterValue
			results[i].Holder = entries[actions_ix].GetEntry().HolderClientId
			actions_ix++
		}
	}

	rp := &proto.ClientResponse{Status: proto.ClientResponse_SUCCESS.Enum(), Results: results}

	return newix, aerq, rp
}

// Basically shuts down all coordinators.
func (p *processor) shutDownLeader() {
	// Reset
	for _, m := range p.cluster.config.getMembers() {
		p.cluster.removeMember(m)
	}
	p.st.Broadcast(locking.ShutDown)
	p.st.Close()
}
