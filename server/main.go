package server

import (
	"bytes"
	"clusterlookup/client"
	clkup "clusterlookup/proto"
	mon "clustermon/serverlib"
	rpclog "clusterrpc/log"
	smgr "clusterrpc/securitymanager"
	rpcs "clusterrpc/server"
	"flag"
	"fmt"
	"os"
	"raftdb/server/constants"
	"runtime/pprof"
	"time"
)

var FLAG_rpc_threads uint
var FLAG_transaction_buffer uint
var FLAG_heartbeat_interval time.Duration
var FLAG_rpc_timeout time.Duration
var FLAG_timer_variance time.Duration
var FLAG_coord_buffer uint
var FLAG_storage string
var FLAG_max_entries_per_rpc uint
var FLAG_max_dbentries_per_rpc uint
var FLAG_internal_request_queue uint
var FLAG_port uint
var FLAG_listen string
var FLAG_member_id string
var FLAG_pubkey, FLAG_privkey string
var FLAG_lkupsrv string
var FLAG_profile time.Duration
var FLAG_lease time.Duration

var LOOKUP_CLIENT *client.LookupClient

var SECURITY_MANAGER *smgr.ServerSecurityManager

var MONVARS *mon.VarSet

type dispatcher struct {
	server     *rpcs.Server
	processors map[string]chan RpcRequest
}

func parseFlags() {
	flag.UintVar(&FLAG_rpc_threads, "rpc_threads", 16, "Number of RPC handler goroutines")
	flag.UintVar(&FLAG_coord_buffer, "coord_buffer", 25,
		"How many requests can be queued in front of coordinators")
	flag.DurationVar(&FLAG_heartbeat_interval, "heartbeat_interval", 1*time.Second, "Interval used for pinging peers. Followers will have a 1.5 times as high interval before converting to candidates.")
	flag.DurationVar(&FLAG_timer_variance, "timer_variance", 250*time.Millisecond, "Range in which to put the random client timeouts: [(1.5 * heartbeat_interval) - timer_variance, (1.5 * heartbeat_interval) + timer_variance]")
	flag.DurationVar(&FLAG_rpc_timeout, "rpc_timeout", 10*time.Second, "RPC timeout in seconds -- wait no longer than this for peers to respond")
	flag.StringVar(&FLAG_storage, "storage", "leveldb", "Which storage backend to use. Supported: leveldb")
	flag.UintVar(&FLAG_max_entries_per_rpc, "max_transmitted_entries", 500, "Maximum number of log entries to transmit at once")
	flag.UintVar(&FLAG_max_dbentries_per_rpc, "max_snapshot_entries", 2500, "Maximum number of database entries to transmit per InstallSnapshot RPC")
	flag.UintVar(&FLAG_internal_request_queue, "internal_rqq_length", 25, "Length of request queue per processor")
	flag.UintVar(&FLAG_port, "port", 2007, "TCP port")
	flag.StringVar(&FLAG_listen, "listen", "*", "Address to listen on")
	flag.UintVar(&FLAG_loglevel, "log", 2, "Loglevel (0 = None, 1 = Errors, 2 = Warnings, 3 = Info, 4 = Debug)")
	flag.StringVar(&FLAG_member_id, "memberid", getNiceId(), "ID to register this server under")
	flag.StringVar(&FLAG_pubkey, "pubkey", "server_pubkey.txt", "Location of server pubkey")
	flag.StringVar(&FLAG_privkey, "privkey", "server_privkey.txt", "Location of server private key")
	flag.StringVar(&FLAG_lkupsrv, "lkupsrv", "", "clusterlookup server to use")
	flag.DurationVar(&FLAG_profile, "profile", 0, "Write CPU profile for <profile> after start")
	flag.DurationVar(&FLAG_lease, "lease", 5*time.Second, "The period for which no new leader is elected, after the last action of the current leader")
	flag.Parse()

	raftdb_log(LOGLEVEL_INFO, "Parsed flags")
}

func setUpLkupClient() {
	for i := 0; i < 5; i++ {
		var err error
		LOOKUP_CLIENT, err = client.NewLookupClient(FLAG_lkupsrv)

		if err != nil {
			raftdb_log(LOGLEVEL_ERRORS, "Could not create lookup client:", err.Error(), "Trying again")
		} else {
			break
		}
	}
}

func setUpSecurity() {
	SECURITY_MANAGER = smgr.NewServerSecurityManager()

	err := SECURITY_MANAGER.LoadKeys(FLAG_pubkey, FLAG_privkey)

	if err != nil {
		raftdb_log(LOGLEVEL_INFO, "Couldn't load key pair - generating new one")
		SECURITY_MANAGER = smgr.NewServerSecurityManager()
		SECURITY_MANAGER.WriteKeys(FLAG_pubkey, FLAG_privkey)
	} else {
		raftdb_log(LOGLEVEL_INFO, "Loaded key pair from disk")
	}

	raftdb_log(LOGLEVEL_INFO, "Server public key is", SECURITY_MANAGER.GetPublicKey())
}

// Needs LKUP client and parsed flags!
func registerMember() {
	raftdb_log(LOGLEVEL_INFO, "Using Member ID", FLAG_member_id+constants.CLKUP_MEMBER_SUFFIX)

	hn, err := os.Hostname()

	if err != nil {
		panic(err.Error())
	}

	task, _, err := LOOKUP_CLIENT.LookupService(FLAG_member_id+constants.CLKUP_MEMBER_SUFFIX, 0)
	raftdb_log(LOGLEVEL_DEBUG, "Existing tasks", task)

	if err == nil || err.Error() == "SERVICE_NOT_FOUND" {
		if len(task) > 0 {
			for i := 0; i < len(task); i++ {
				raftdb_log(LOGLEVEL_INFO, "Unregistering member", FLAG_member_id+constants.CLKUP_MEMBER_SUFFIX, i)
				LOOKUP_CLIENT.Unregister(FLAG_member_id+constants.CLKUP_MEMBER_SUFFIX, uint32(i))
			}
		}

		raftdb_log(LOGLEVEL_INFO, "Registering member as", FLAG_member_id+constants.CLKUP_MEMBER_SUFFIX)
		_, err = LOOKUP_CLIENT.Register(FLAG_member_id+constants.CLKUP_MEMBER_SUFFIX, hn, uint32(FLAG_port),
			clkup.ServiceType_CRPC_GENERIC, SECURITY_MANAGER.GetPublicKey())

		if err != nil {
			raftdb_log(LOGLEVEL_ERRORS, "Registering failed with", err.Error())
			panic(err.Error())
		}
	} else {
		panic(err.Error())
	}
}

func startRPCServer() {
	rpclog.SetLoglevel(int(FLAG_loglevel))
	server, err := rpcs.NewServer(FLAG_listen, FLAG_port, FLAG_rpc_threads, SECURITY_MANAGER)

	if err != nil {
		raftdb_log(LOGLEVEL_ERRORS, "Could not create RPC server:", err.Error())
		panic("fatal error; see logs")
	}

	newRaftdbServer(server)

	err = server.Start()

	if err != nil {
		raftdb_log(LOGLEVEL_ERRORS, "Could not start RPC server:", err.Error())
		panic("fatal error; see logs")
	}
}

func getCommandLine() string {
	var buf bytes.Buffer

	flag.Visit(func(f *flag.Flag) {
		buf.WriteString("-")
		buf.WriteString(f.Name)
		buf.WriteString("=")
		buf.WriteString(fmt.Sprint(f.Value))
		buf.WriteString(" ")
	})

	return buf.String()

}

func Server_main() {
	raftdb_log(LOGLEVEL_INFO, "Starting raftdb server version", constants.RAFTDB_VERSION)

	parseFlags()

	raftdb_log(LOGLEVEL_INFO, "Running with command line", getCommandLine())

	if FLAG_profile > 0 {
		raftdb_log(LOGLEVEL_INFO, "Writing profile to raft.pprof")

		w, err := os.OpenFile("raft.pprof", os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)

		if err != nil {
			raftdb_log(LOGLEVEL_WARNINGS, "Couldn't open pprof destination file:", err.Error())
		} else {
			pprof.StartCPUProfile(w)
			time.AfterFunc(FLAG_profile, func() { pprof.StopCPUProfile(); w.Close() })
		}
	}

	MONVARS = mon.NewVarSet()

	setUpLkupClient()
	setUpSecurity()
	registerMember()

	startRPCServer()
}
