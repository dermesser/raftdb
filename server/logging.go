package server

import (
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"runtime"
)

var FLAG_loglevel uint = LOGLEVEL_INFO

var logger *log.Logger

const (
	LOGLEVEL_NONE uint = iota
	LOGLEVEL_ERRORS
	LOGLEVEL_WARNINGS
	LOGLEVEL_INFO
	LOGLEVEL_DEBUG
)

const logger_flags = log.LstdFlags | log.Lmicroseconds
const stacktrace_buffer_size = 1 << 12

var loglevel_strings []string = []string{"[NON]", "[ERR]", "[WRN]", "[INF]", "[DBG]"}

func init() {
	logger = log.New(os.Stderr, "raftdb ", logger_flags)
}

func set_logging_device(out io.Writer) {
	logger = log.New(out, "raftdb ", logger_flags)
}

func loglevel_to_string(loglevel uint) string {
	return loglevel_strings[loglevel]
}

func raftdb_log(loglevel uint, what ...interface{}) {
	if loglevel <= FLAG_loglevel {
		_, path, line, ok := runtime.Caller(1)
		file := filepath.Base(path)

		if ok {
			logger.Printf("%s at %15s:%4d: %s", loglevel_to_string(loglevel), file, line, fmt.Sprintln(what...))
		} else {
			logger.Printf("%s: %s", loglevel_to_string(loglevel), fmt.Sprintln(what...))
		}
	}
}

func raftdb_log_stacktrace(all bool) {
	buffer := make([]byte, stacktrace_buffer_size)

	n := runtime.Stack(buffer, all)

	logger.Print(string(buffer[0:n]))
}
