package server

import (
	"bytes"
	"errors"
	"raftdb/proto"
	"raftdb/server/storage"
	"raftdb/server/versionedstorage"

	pb "github.com/gogo/protobuf/proto"
)

type coordinator struct {
	st                      versionedstorage.ROVersionedStorage
	next_index, match_index uint64
	member_id, cluster_id   string
	rqchan                  chan *RpcRequest
	peer                    member
	main_config             *clusterconfig
}

// Returns a new coordinator object.
// ch: Channel for incoming local requests
// remote: interface to the cluster member this coordinator is talking to
// log: The leader's log
// state: The leader's state
// cluster_id: Name of cluster
// Don't forget to run it!
func newCoordinator(ch chan *RpcRequest, remote member, member_id string, st versionedstorage.ROVersionedStorage,
	cluster_id string) *coordinator {

	next_index, _, _ := st.Index()

	return &coordinator{st: st, next_index: next_index,
		rqchan: ch, match_index: 0, peer: remote, cluster_id: cluster_id}
}

// The coordinator fills in at least PreviousLogVersion and amends other fields of
// requests it receives from the processor.

func (c *coordinator) runCoordinator() {
	raftdb_log(LOGLEVEL_DEBUG, c.cluster_id, "Coordinator running")
	for rq := range c.rqchan {

		if rq.shut_down {
			return
		} else if rq.ae != nil {
			ae_response, err := c.AppendEntries(rq.ae)
			rq.rpchan <- &RpcResponse{ae: ae_response, err: err}
			continue
		} else if rq.rqv != nil {
			rqv_response, err := c.RequestVote(rq.rqv)

			rq.rpchan <- &RpcResponse{v: rqv_response, err: err}
		}
		// No InstallSnapshot, only used by replicate()
	}
}

// Fills in necessary fields if not present.
func (c *coordinator) RequestVote(rq *proto.RequestVote) (*proto.Vote, error) {

	term := c.st.Term()
	logix, _, _ := c.st.Index()

	if rq.GetTerm() == 0 {
		rq.Term = pb.Uint64(term)
	}

	if rq.GetCandidateId() == "" {
		rq.CandidateId = pb.String(c.member_id)
	}

	if rq.GetLogIx() == nil {
		rq.LogIx = &proto.VersionInfo{Term: pb.Uint64(term), LogIx: pb.Uint64(logix - 1)}
	}

	if rq.GetClusterId() == "" {
		rq.ClusterId = pb.String(c.cluster_id)
	}

	if rq.GetConfigVersion() == 0 {
		rq.ConfigVersion = &c.main_config.version
	}

	rp, err := c.peer.RequestVote(rq)

	return rp, err
}

// Only the Entries field must already be filled in, the rest can be auto-filled.
func (c *coordinator) AppendEntries(rq *proto.AppendEntriesRequest) (*proto.AppendEntriesResponse, error) {
	nextix, commitix, _ := c.st.Index()

	// TODO Think about removing this as this produces race conditions.
	if rq.GetTerm() == 0 {
		rq.Term = pb.Uint64(c.st.Term())
	}
	if rq.GetClusterId() == "" {
		rq.ClusterId = pb.String(c.cluster_id)
	}
	if rq.GetLeaderId() == "" {
		rq.LeaderId = pb.String(c.member_id)
	}
	if rq.GetLeaderCommitIx() == 0 {
		rq.LeaderCommitIx = pb.Uint64(commitix)
	}
	if rq.GetPreviousLogVersion() == nil {
		previous_entry := c.st.GetLogEntry(nextix - 1)

		if previous_entry != nil && previous_entry.GetEntry() != nil {
			rq.PreviousLogVersion = previous_entry.GetEntry().Version
		}
	}
	// Retry until member has the newest state:
	// If replicate() returns an error, that usually means that it has sent something and changed the
	// follower's state and now wants to be called again to act on that new state (for example, it sent
	// an AppendEntries that was rejected, so it reset the follower's indices and will act accordingly
	// when called again). We do this for a maximum of three times; under certain circumstances, requests
	// are dropped as well.

	for i := 0; i < 3; i++ {
		// Sends AppendEntries if difference between member and us is smaller than 500 entries or so
		// Otherwise, snapshot

		rp, err := c.replicate(rq)

		if err == nil {
			return rp, nil
		} else {
			// This is a heartbeat, just ignore it
			if len(rq.GetEntries()) == 0 {
				break
			}
			// If the queue is close to fullness, drop this request: State will be caught up later
			if len(c.rqchan) > int(0.9*float64(FLAG_coord_buffer)) {
				break
			}

			raftdb_log(LOGLEVEL_INFO, c.member_id, c.cluster_id, "Replication error to", c.peer, ":", err.Error())
		}
	}

	return nil, nil
}

// Actually sends AppendEntries and InstallSnapshot.
// Knows three cases:
// a) Difference between member's next_index and first index in log entries is 0: Send new entries.
// b) Difference "--" is > 0 but < FLAG_max_entries_per_rpc: Send old and new log entries.
// c) Difference is larger: Send snapshot until current committed index, then recurse to send log entries since that
//
// Returns non-nil until the member is up-to-date, i.e. keep calling it until it returns nil
func (c *coordinator) replicate(rq *proto.AppendEntriesRequest) (*proto.AppendEntriesResponse, error) {
	term := c.st.Term()
	// Heartbeat
	if len(rq.GetEntries()) == 0 {

		rp, err := c.peer.AppendEntries(rq)

		return rp, err
	} else if rq.GetEntries()[0].GetEntry().GetVersion().GetLogIx() <= c.match_index { // Skip over requests that may have been sent via snapshot
		return nil, nil
	} else {
		// Next index is the index of the first log entry in the AppendEntries RPC
		next_index := rq.GetEntries()[0].GetEntry().GetVersion().GetLogIx()
		difference := next_index - c.next_index

		// Normal AppendEntries. Avoid checking an overflown number by comparing indices first
		if next_index <= c.next_index {

			first_entry := rq.GetEntries()[0]

			// Assume that the appended entries will be accepted (if not, replicate then)
			// Because: On startup, the log entries seem to be correct from our perspective.
			// Afterwards, the AppendEntries are processed in sequence, so they will always seem to
			// fit in the member's log.
			previous_entry := c.st.GetLogEntry(first_entry.GetEntry().GetVersion().GetLogIx() - 1)
			rq.PreviousLogVersion = previous_entry.GetEntry().GetVersion()

			rp, err := c.peer.AppendEntries(rq)

			if err != nil {
				return rp, err
			}

			if rp.GetSuccess() {
				c.next_index = rq.GetEntries()[len(rq.GetEntries())-1].GetEntry().GetVersion().GetLogIx() + 1
				c.match_index = rq.GetEntries()[len(rq.GetEntries())-1].GetEntry().GetVersion().GetLogIx()
				return rp, nil
			} else { // Somewhat likely, stale member or lost leadership.
				if rp.GetTerm() > term {
					return rp, errors.New("Not leader anymore")
				} else {
					// No success because log matching is not fulfilled, retry with smaller next_index
					c.next_index = rp.GetLastIndex() + 1
					return rp, errors.New("wrn - non-matching index on member. retry")
				}
			}
		} else if difference < uint64(FLAG_max_entries_per_rpc) { // Arbitrary threshold: If the difference is less than 400, send the log. (otherwise, a snapshot)
			new_entries := rq.GetEntries()
			old_entries := make([]*proto.Entry, difference)

			for i := c.next_index; i < next_index; i++ {
				old_entries[i-c.next_index] = c.st.GetLogEntry(i)
			}

			all_entries := append(old_entries, new_entries...)

			previous_entry := c.st.GetLogEntry(all_entries[0].GetEntry().GetVersion().GetLogIx() - 1)

			rq.Entries = all_entries
			rq.PreviousLogVersion = &proto.VersionInfo{LogIx: pb.Uint64(previous_entry.GetEntry().GetVersion().GetLogIx()),
				Term: pb.Uint64(previous_entry.GetEntry().GetVersion().GetTerm())}

			rp, err := c.peer.AppendEntries(rq)

			if err != nil { // :-(
				return rp, err
			} else if rp.GetSuccess() {
				c.next_index = rq.GetEntries()[len(rq.GetEntries())-1].GetEntry().GetVersion().GetLogIx() + 1
				c.match_index = rq.GetEntries()[len(rq.GetEntries())-1].GetEntry().GetVersion().GetLogIx()

				return rp, nil
			} else { // success == false :-(
				if rp.GetTerm() > c.st.Term() {
					return rp, errors.New("Not leader anymore")
				}

				// This happens when we know the member is stale, but it actually is even more stale. Therefore repeat.
				c.next_index = rp.GetLastIndex() + 1
				return rp, errors.New("wrn - success = false when trying to send catch-up log entries")

			}
		} else { // Send snapshots.
			_, committed, _ := c.st.Index()
			// Transmit all entries up until the committed one.
			latest_version := committed

			raftdb_log(LOGLEVEL_INFO, "Sending snapshot until", latest_version)

			iterator := c.st.GetIterator(nil, nil)
			defer iterator.Close()

			rq := new(proto.InstallSnapshot)
			rq.Term = pb.Uint64(c.st.Term())
			rq.LeaderId = pb.String(c.member_id)
			rq.LastIncluded = c.st.GetLogEntry(latest_version)
			rq.ClusterId = &c.cluster_id
			rq.MemberId = pb.String(c.member_id)

			// Until we have all snapshots
			for {
				rp := new(proto.InstallSnapshotResponse)

				rq.Snapshot = c.createSnapshot(iterator, latest_version)

				if uint(len(rq.Snapshot)) == 0 {
					rq.Done = pb.Bool(true)
				}

				rp, err := c.peer.InstallSnapshot(rq)

				if err != nil {
					raftdb_log(LOGLEVEL_ERRORS, c.member_id, c.cluster_id, "Could not send snapshot to client:", err.Error())
					return nil, err
				}

				if rp.GetStatus() == proto.InstallSnapshotResponse_NO_CLUSTER {
					return nil, errors.New("retry AE request")
				}

				// both should be usually true
				if rp.GetTerm() > term || rp.GetStatus() == proto.InstallSnapshotResponse_WRONG_TERM {
					return nil, errors.New("Not leader anymore")
				}

				// End of snapshots
				if rq.GetDone() {
					c.next_index = latest_version + 1
					c.match_index = latest_version
					return nil, errors.New("Snapshot sent; now send entries")
				}
			}
			// After having sent snapshots, send actual entries.
			return nil, errors.New("wrn - snapshot sent, now send entries")
		}
	}

	return nil, nil
}

func (c *coordinator) createSnapshot(iterator storage.Iterator, latest_version uint64) []*proto.Entry {
	snapshot_entries := make([]*proto.Entry, 0, FLAG_max_dbentries_per_rpc)

	for i := uint(0); i < FLAG_max_dbentries_per_rpc; i++ {
		key_b, entry_b := iterator.Next()

		if key_b == nil {
			break
		}

		if bytes.HasPrefix(key_b, []byte("__RFTDBST.__RFTDBLG")) ||
			bytes.HasPrefix(key_b, []byte("__RFTDBST.___RAFTDB_INTERNAL_PERSIST_STATE")) {
			i--
			continue
		}

		dbentry := new(proto.DBEntry)
		err := dbentry.Unmarshal(entry_b)

		if err == nil && dbentry.GetVersion().GetLogIx() <= latest_version {
			entry := new(proto.Entry)
			entry.Type = proto.Entry_PUT.Enum()

			newkey := make([]byte, len(key_b))
			copy(newkey, key_b)

			entry.Key = newkey
			entry.Entry = dbentry

			snapshot_entries = append(snapshot_entries, entry)
		} else {
			i--
			continue
		}
	}

	return snapshot_entries
}
