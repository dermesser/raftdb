package server

import (
	"errors"
	"raftdb/proto"
	"raftdb/server/constants"
)

func (p *processor) runFollower() {
	raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "Running follower")

	rq := <-p.requests

	if rq.ignore {
		return
	}

	if rq.ae != nil {
		p.runFollowerAppendEntries(rq)
	} else if rq.rqv != nil {
		p.runFollowerRequestVote(rq)
	} else if rq.is != nil {
		p.runFollowerInstallSnapshot(rq)
	} else if rq.cc != nil {
		p.runFollowerConfigChange(rq)
	} else if rq.cr != nil {
		raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "Follower received CR:", rq.cr)
		rq.rpchan <- &RpcResponse{cr: &proto.ClientResponse{Status: proto.ClientResponse_ERROR_NOT_MASTER.Enum()}}
	} else {
		rq.rpchan <- &RpcResponse{err: errors.New("Unknown method called")}
	}
}

func (p *processor) runFollowerAppendEntries(rq *RpcRequest) {
	raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "Follower received AE:", rq.ae)

	rp, err := p.AppendEntries(rq.ae)

	rq.rpchan <- &RpcResponse{ae: rp, err: err}
}

func (p *processor) runFollowerRequestVote(rq *RpcRequest) {
	raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "Follower received RV:", rq.rqv)

	rp, err := p.RequestVote(rq.rqv)

	rq.rpchan <- &RpcResponse{v: rp, err: err}
}

func (p *processor) runFollowerInstallSnapshot(rq *RpcRequest) {
	raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "Follower received IS:", rq.is)

	rp, err := p.InstallSnapshot(rq.is)

	rq.rpchan <- &RpcResponse{is: rp, err: err}
}

func (p *processor) runFollowerConfigChange(rq *RpcRequest) {
	raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "Follower received CC:", rq.cc)

	if rq.cc.GetType() == proto.ConfigRequest_SHUTDOWN {
		p.shutDownFollower()
		rq.rpchan <- &RpcResponse{cc: &proto.ConfigResponse{Status: proto.ConfigResponse_OK.Enum()}}
	} else {
		rq.rpchan <- &RpcResponse{cc: &proto.ConfigResponse{Status: proto.ConfigResponse_ERROR_NOT_MASTER.Enum()}, err: errors.New(constants.ERR_NOT_LEADER)}
	}
}

func (p *processor) shutDownFollower() {
	p.processor_state = state_FINALIZE
	p.st.Close()
}
