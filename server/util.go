package server

import (
	"crypto/sha1"
	"fmt"
	"math/rand"
	"os"
	"time"
)

var randsrc *os.File

func init() {
	var err error
	randsrc, err = os.Open("/dev/urandom")

	if err != nil {
		panic("Could not open /dev/urandom")
	}

	rand.Seed(time.Now().UnixNano())
}

func getNiceId() string {
	if randsrc == nil {
		return ""
	}

	data := make([]byte, 128)
	n, err := randsrc.Read(data)

	if n < 10 || err != nil {
		return ""
	}

	return fmt.Sprintf("%x", sha1.Sum(data))
}

func randBetween(min, max int) int {
	var absmin int

	if min > 0 {
		absmin = min
	} else {
		absmin = -min
	}

	return rand.Intn((max-min)+1) - absmin
}
