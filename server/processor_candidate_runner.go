package server

import (
	"raftdb/proto"
	"time"

	pb "github.com/gogo/protobuf/proto"
)

// Run election
func (p *processor) runCandidate() {
	raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "Running candidate")

	rq := new(proto.RequestVote)

	nextix, _, _ := p.st.Index()
	oldterm := p.st.Term()

	rq.CandidateId = pb.String(p.member_id)
	rq.ClusterId = pb.String(p.cluster.getClusterId())
	rq.ConfigVersion = pb.Uint64(p.cluster.config.version)
	rq.LogIx = &proto.VersionInfo{LogIx: pb.Uint64(nextix - 1),
		Term: pb.Uint64(p.st.GetLogEntry(nextix - 1).GetEntry().GetVersion().GetTerm())}
	rq.Term = pb.Uint64(oldterm + 1)

	external_majority := ((len(p.cluster.config.getMembers()) - 1) / 2)

	// If we have 3 other members, the majority has to be 3 out of 4, i.e. us and 3//2 + 1
	if (len(p.cluster.config.getMembers())-1)%2 == 1 {
		external_majority++
	}

	raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "External election majority is", external_majority, p.cluster.config)

	// safeguard
	if external_majority == 0 {
		raftdb_log(LOGLEVEL_ERRORS, "Configuration doesn't exist!")
		p.convertFollower()
		return
	}

	rps := p.cluster.sendToAll(&RpcRequest{rqv: rq}, external_majority)

	if len(rps) < external_majority {
		raftdb_log(LOGLEVEL_WARNINGS, p.cluster.getClusterId(), "Did not receive enough votes:", len(rps))
		time.Sleep(time.Millisecond * time.Duration(randBetween(-int(FLAG_timer_variance/time.Millisecond), int(FLAG_timer_variance/time.Millisecond))))
		p.convertFollower()
		return
	} else if p.processor_state == state_FOLLOWER { // another requestvote to us was successful
		return
	} else {
		raftdb_log(LOGLEVEL_INFO, p.cluster.getClusterId(), "Received", len(rps), "votes")

		var votes int

		for _, v := range rps {
			if !v.v.GetVoteGranted() {

				raftdb_log(LOGLEVEL_WARNINGS, p.cluster.getClusterId(), "Received negative vote:", v.v.GetFailType().String())
				raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "Negative vote (full):", v.v)

				switch v.v.GetFailType() {
				case proto.Vote_WRONG_TERM:
					for p.st.Term() < v.v.GetTerm() {
						p.st.SetNextTerm("")
					}
				case proto.Vote_WRONG_ENTRY_TERM:
				case proto.Vote_WRONG_LOGIX:
				case proto.Vote_WRONG_CONFIG_VERSION:
					// Wait for explicit shutdown
				}
				break
			} else {
				votes++
			}
		}

		if votes < external_majority {
			p.convertFollower()
		} else {
			p.convertLeader()
		}
	}
}
