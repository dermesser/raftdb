package server

import "testing"

func TestNiceId(t *testing.T) {
	id1, id2 := getNiceId(), getNiceId()

	if id1 == id2 {
		t.Error("Identical id returned twice")
	}
}
