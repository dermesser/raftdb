package versionedstorage

import (
	"os"
	"raftdb/proto"
	"raftdb/server/locking"
	"raftdb/server/storage"
	"testing"

	pb "github.com/gogo/protobuf/proto"
)

// This tests append/commit performance with single entries on an in-memory storage
func BenchmarkAppendCommitMemory(b *testing.B) {
	vs := getVS()
	entry := getTestEntries()[3]

	for i := 0; i < b.N; i++ {
		newix, _ := vs.Prepare(Entry2Op(entry))
		err := vs.Commit(newix)

		if err != nil {
			b.Error(err.Error())
		}
	}

}

func getDiskVS() (VersionedStorage, storage.Storage) {
	cst, err := storage.NewStorage("leveldb", "test.db")
	if err != nil {
		panic(err.Error())
	}
	return NewIntegratedVersionedStorage(locking.NewMNManager(), cst), cst
}

func tearDownStorage(st storage.Storage) {
	st.Close()
	os.RemoveAll("test.db")
}

func BenchmarkAppendCommitDisk(b *testing.B) {
	vs, st := getDiskVS()
	defer tearDownStorage(st)

	entry := getTestEntries()[3]

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		newix, _ := vs.Prepare(Entry2Op(entry))
		err := vs.Commit(newix)

		if err != nil {
			b.Error(err.Error())
		}
	}
	b.StopTimer()
}

func BenchmarkIndex(b *testing.B) {
	vs, st := getDiskVS()
	defer tearDownStorage(st)

	entry := getTestEntries()[3]

	for i := 0; i < 10; i++ {
		newix, _ := vs.Prepare(Entry2Op(entry))
		err := vs.Commit(newix)

		if err != nil {
			b.Error(err.Error())
		}
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		vs.Index()
	}
}

func BenchmarkTerm(b *testing.B) {
	vs := getVS()

	vs.SetNextTerm("")
	vs.SetNextTerm("")

	entry := getTestEntries()[3]

	for i := 0; i < 10; i++ {
		newix, _ := vs.Prepare(Entry2Op(entry))
		err := vs.Commit(newix)

		if err != nil {
			b.Error(err.Error())
		}
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		vs.Term()
	}
}

func BenchmarkAppendEntriesMem(b *testing.B) {
	vs := getVS()
	localvs := getVS()
	defer vs.Close()
	defer localvs.Close()

	op := Entries2Op(getTestEntries())

	aerq := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId:           pb.String("x"),
		PreviousLogVersion: &proto.VersionInfo{LogIx: pb.Uint64(0), Term: pb.Uint64(1)},
		LeaderCommitIx:     pb.Uint64(0),
		ClusterId:          pb.String("test"),
	}

	aerq_commit := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId:       pb.String("x"),
		Entries:        []*proto.Entry{},
		LeaderCommitIx: pb.Uint64(0),
		ClusterId:      pb.String("test"),
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		newix, entries := localvs.Prepare(op)

		aerq.Entries = entries

		rp, err := vs.AppendEntries(aerq)

		if err != nil {
			b.Error(err)
		}
		if !rp.GetSuccess() {
			b.Error("No success")
		}

		localvs.Commit(newix)

		*aerq_commit.LeaderCommitIx += 4

		rp, err = vs.AppendEntries(aerq_commit)

		if err != nil {
			b.Error(err)
		}
		if !rp.GetSuccess() {
			b.Error("No success")
		}

		*aerq.PreviousLogVersion.LogIx += 4
		*aerq.LeaderCommitIx += 4
	}

	b.StopTimer()
}
