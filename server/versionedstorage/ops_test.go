package versionedstorage

import (
	"bytes"
	"raftdb/proto"
	"raftdb/server/storage"
	"testing"
	"time"

	pb "github.com/gogo/protobuf/proto"
)

// Ops tests

func TestEntryOp(t *testing.T) {
	entry := getTestEntries()[0]

	op := Entry2Op(entry)
	entries := op.GetEntries(storage.NewTempStorage())

	if len(entries) != 1 {
		t.Error("Wrong len(entries):", len(entries))
	}
	if !pb.Equal(entry, entries[0]) {
		t.Error("Non-equal messages:", entry, entries[0])
	}
}

func TestEntriesOp(t *testing.T) {
	entries := getTestEntries()

	op := Entries2Op(entries)
	opdentries := op.GetEntries(storage.NewTempStorage())

	if len(opdentries) != len(entries) {
		t.Error("Wrong len(opdentries):", len(opdentries))
	}

	for i := range opdentries {
		if !pb.Equal(entries[i], opdentries[i]) {
			t.Error("Non-equal message at", i, ":", entries[i], opdentries[i])
		}
	}
}

func TestAEOp(t *testing.T) {
	entries := getTestEntries()

	aerq := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId:           pb.String("x"),
		PreviousLogVersion: &proto.VersionInfo{LogIx: pb.Uint64(2), Term: pb.Uint64(1)},
		Entries:            entries,
		LeaderCommitIx:     pb.Uint64(0),
		ClusterId:          pb.String("test"),
	}

	op := AE2Op(aerq)
	opdentries := op.GetEntries(storage.NewTempStorage())

	if len(opdentries) != len(entries) {
		t.Error("Wrong len(opdentries):", len(opdentries))
	}

	for i := range opdentries {
		if !pb.Equal(entries[i], opdentries[i]) {
			t.Error("Non-equal message at", i, ":", entries[i], opdentries[i])
		}
	}
}

func TestClientRequest(t *testing.T) {
	Actions := []*proto.Action{&proto.Action{Type: proto.Action_SET.Enum(),
		Key:   []byte("abc"),
		Value: []byte("def"),
	},

		&proto.Action{Type: proto.Action_LOCK.Enum(),
			Key:        []byte("abc"),
			Value:      []byte("def"),
			Expiration: pb.Uint64(uint64(time.Now().Add(10 * time.Second).UnixNano()))},

		&proto.Action{Type: proto.Action_DELETE.Enum(),
			Key:   []byte("abc"),
			Value: []byte("def"),
		},

		&proto.Action{Type: proto.Action_UNLOCK.Enum(),
			Key:   []byte("abc"),
			Value: []byte("def"),
		},

		&proto.Action{Type: proto.Action_COUNT.Enum(),
			Key:       []byte("abc"),
			Value:     []byte("def"),
			CounterOp: &proto.CounterOp{Type: proto.CounterOp_COUNT.Enum(), Val: pb.Int64(1)},
		},

		&proto.Action{Type: proto.Action_COUNT.Enum(),
			Key:       []byte("abc"),
			Value:     []byte("def"),
			CounterOp: &proto.CounterOp{Type: proto.CounterOp_SET.Enum(), Val: pb.Int64(4)},
		},
		&proto.Action{Type: proto.Action_CHECK_EQ.Enum(),
			Key:   []byte("abc"),
			Value: []byte("def"),
		},
	}

	expected_entries := []*proto.Entry{&proto.Entry{Type: proto.Entry_PUT.Enum(),
		Key: []byte("abc"),
		Entry: &proto.DBEntry{Type: proto.DBEntry_DATA.Enum(), Value: []byte("def"),
			HolderClientId: pb.String("client1")},
	},

		&proto.Entry{Type: proto.Entry_LOCK.Enum(),
			Key: []byte("abc"),
			Entry: &proto.DBEntry{Type: proto.DBEntry_LOCK.Enum(), Value: []byte("def"),
				HolderClientId: pb.String("client1")},
		},

		&proto.Entry{Type: proto.Entry_DELETE.Enum(),
			Key:   []byte("abc"),
			Entry: &proto.DBEntry{Type: proto.DBEntry_DATA.Enum(), IsDeleted: pb.Bool(true)},
		},

		&proto.Entry{Type: proto.Entry_UNLOCK.Enum(),
			Key:   []byte("abc"),
			Entry: &proto.DBEntry{Type: proto.DBEntry_DATA.Enum(), IsDeleted: pb.Bool(true)},
		},

		&proto.Entry{Type: proto.Entry_PUT.Enum(),
			Key: []byte("abc"),
			Entry: &proto.DBEntry{Type: proto.DBEntry_COUNTER.Enum(), CounterValue: pb.Int64(1),
				HolderClientId: pb.String("client1")},
		},
		&proto.Entry{Type: proto.Entry_PUT.Enum(),
			Key: []byte("abc"),
			Entry: &proto.DBEntry{Type: proto.DBEntry_COUNTER.Enum(), CounterValue: pb.Int64(4),
				HolderClientId: pb.String("client1")},
		},
	}

	typemap := map[proto.Action_Type]proto.Entry_Type{proto.Action_SET: proto.Entry_PUT,
		proto.Action_COUNT:  proto.Entry_PUT,
		proto.Action_LOCK:   proto.Entry_LOCK,
		proto.Action_UNLOCK: proto.Entry_UNLOCK,
		proto.Action_DELETE: proto.Entry_DELETE}

	// Account for CHECK_EQ action which yields no entry
	if len(expected_entries) != len(Actions)-1 {
		t.Error("Check the test: Actions and expected_entries are not of the same length")
	}

	// Check that we haven't done any mistake when writing the test data
	for i := range expected_entries {
		// Don't check on DELETE, UNLOCK entries (because of poor test setup here)
		if !bytes.Equal(Actions[i].GetKey(), expected_entries[i].GetKey()) {
			t.Error("Key is not equal:", string(Actions[i].GetKey()), string(expected_entries[i].GetKey()))
		}
		// Don't test on DELETE, UNLOCK, COUNT
		if !bytes.Equal(Actions[i].GetValue(), expected_entries[i].GetEntry().GetValue()) && i < 2 {
			t.Error("Key is not equal:", string(Actions[i].GetValue()), string(expected_entries[i].GetEntry().GetValue()))
		}
		if i == 4 && expected_entries[i].GetEntry().GetCounterValue() != 1 {
			t.Error("Wrong counter value:", expected_entries[i].GetEntry().GetCounterValue())
		}
		if i == 5 && expected_entries[i].GetEntry().GetCounterValue() != 4 {
			t.Error("Wrong counter value:", expected_entries[i].GetEntry().GetCounterValue())
		}
		if typemap[Actions[i].GetType()] != expected_entries[i].GetType() {
			t.Error("Type mismatch:", Actions[i].GetType().String(), expected_entries[i].GetType().String())
		}
	}

	st := storage.NewTempStorage()
	op := Actions2Op(Actions, "client1")
	entries := op.GetEntries(st)

	if len(entries) != len(expected_entries) {
		t.Error("Unexpected entries length", len(entries), len(expected_entries))
	}

	for i := range expected_entries {
		entry := entries[i]

		// Don't check on DELETE, UNLOCK entries (because of poor test setup here)
		if !bytes.Equal(entry.GetKey(), expected_entries[i].GetKey()) {
			t.Error("Key is not equal:", string(Actions[i].GetKey()), string(expected_entries[i].GetKey()))
		}
		// Don't test on DELETE, UNLOCK, COUNT
		if !bytes.Equal(entry.GetEntry().GetValue(), expected_entries[i].GetEntry().GetValue()) && i < 2 {
			t.Error("Key is not equal:", string(Actions[i].GetValue()), string(expected_entries[i].GetEntry().GetValue()))
		}
		if i == 4 && entry.GetEntry().GetCounterValue() != 1 {
			t.Error("Wrong counter value:", expected_entries[i].GetEntry().GetCounterValue())
		}
		if i == 5 && entry.GetEntry().GetCounterValue() != 4 {
			t.Error("Wrong counter value:", expected_entries[i].GetEntry().GetCounterValue())
		}
		if entry.GetType() != expected_entries[i].GetType() {
			t.Error("Type mismatch:", entry.GetType().String(), expected_entries[i].GetType().String())
		}
	}

}
