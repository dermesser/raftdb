package versionedstorage

import (
	"bytes"
	"raftdb/proto"
	"raftdb/server/locking"
	"raftdb/server/log"
	"raftdb/server/storage"

	"errors"
	"fmt"
	"time"

	pb "github.com/gogo/protobuf/proto"
)

var _ = pb.Uint64

type versionedStorageImpl struct {
	storage.ReadOnlyStorage
	log.ReadOnlyLog

	st storage.Storage
	l  log.Log
	n  locking.INotificationManager
	tm *locking.TimeoutManager

	previous_index uint64
}

func NewVersionedStorage(
	nmgr locking.INotificationManager,
	st storage.Storage,
	log log.Log) VersionedStorage {

	vs := &versionedStorageImpl{n: nmgr, ReadOnlyStorage: st,
		st: st, ReadOnlyLog: log, l: log,
		tm: locking.NewTimeoutManager(nmgr)}
	go vs.tm.RunNotifier()
	return vs
}

func NewIntegratedVersionedStorage(
	nmgr locking.INotificationManager,
	st storage.Storage) VersionedStorage {

	l := log.NewStorageLog(st)

	vs := &versionedStorageImpl{n: nmgr, tm: locking.NewTimeoutManager(nmgr),
		ReadOnlyStorage: st, st: st,
		l: l, ReadOnlyLog: l}
	go vs.tm.RunNotifier()
	return vs
}

func (vs *versionedStorageImpl) Term() uint64 {
	t, _ := vs.l.GetPersistentState()
	return t
}

func (vs *versionedStorageImpl) SetNextTerm(votedfor string) {
	vs.l.SetPersistentState(vs.Term()+1, votedfor)
}

func (vs *versionedStorageImpl) Index() (uint64, uint64, uint64) {
	return vs.l.GetIndex()
}

func (vs *versionedStorageImpl) Prepare(op Op) (uint64, []*proto.Entry) {
	entries := op.GetEntries(vs.st)

	next, _, _ := vs.l.GetIndex()

	vs.previous_index = next

	newix := vs.l.Append(next, entries)
	return newix - 1, entries
}

func (vs *versionedStorageImpl) Revert() {
	if vs.previous_index > 0 {
		vs.l.ResetNext(vs.previous_index)
		vs.previous_index = 0
	}
}

func (vs *versionedStorageImpl) Broadcast(n locking.NotificationType) {
	vs.n.Broadcast(n)
}

func (vs *versionedStorageImpl) StopTimeoutManager() {
	vs.tm.SetPollInterval(0)
	vs.tm = nil
}

func (vs *versionedStorageImpl) StartTimeoutManager(ival time.Duration) {
	if vs.tm != nil {
		vs.StopTimeoutManager()
	}
	vs.tm = locking.NewTimeoutManager(vs.n)
	vs.tm.SetPollInterval(ival)
	go vs.tm.RunNotifier()
}

func (vs *versionedStorageImpl) Close() {
	vs.l.Close()
	vs.st.Close()
	vs.tm.SetPollInterval(0)
}

func (vs *versionedStorageImpl) Commit(to uint64) error {
	_, commitix, _ := vs.l.GetIndex()

	for i := commitix + 1; i <= to; i++ {
		entry := vs.l.GetLogEntry(i)

		if entry == nil {
			vs.l.SetCommitted(i - 1)
			return errors.New(fmt.Sprintf("Could not find entry at index %d", i))
		}

		err := vs.setEntry(entry)

		if err != nil {
			vs.l.SetCommitted(i - 1)
			return err
		}
	}
	vs.l.SetCommitted(to)
	vs.previous_index = 0
	return nil
}

func (vs *versionedStorageImpl) setEntry(e *proto.Entry) error {
	dbentry, err := e.GetEntry().Marshal()

	if err != nil {
		return errors.New(fmt.Sprintf("Could not commit entry: %s", err.Error()))
	} else {
		switch e.GetType() {
		case proto.Entry_PUT:
			vs.st.Set(e.GetKey(), dbentry)
			vs.n.Send(string(e.GetKey()), locking.Set)
		case proto.Entry_DELETE:
			vs.st.Delete(e.GetKey())
			vs.n.Send(string(e.GetKey()), locking.Deleted)
		case proto.Entry_LOCK:
			// The leader code has already checked if the locking situation is ok
			// and aborted if it was not
			vs.st.Set(e.GetKey(), dbentry)
			vs.n.Send(string(e.GetKey()), locking.Locked)

			if e.GetEntry().GetExpiration() > 0 {
				vs.tm.SetTimeout(string(e.GetKey()), time.Unix(0, int64(e.GetEntry().GetExpiration())))
			}
		case proto.Entry_UNLOCK:
			vs.st.Delete(e.GetKey())
			vs.n.Send(string(e.GetKey()), locking.Released)
			vs.tm.StopTimeout(string(e.GetKey()))
		}
	}
	return nil
}

func (vs *versionedStorageImpl) Reset() {
	vs.st.Reset()
	vs.l = log.NewStorageLog(vs.st)
}

func (vs *versionedStorageImpl) InstallSnapshot(ss *proto.InstallSnapshot) *proto.InstallSnapshotResponse {
	rp := &proto.InstallSnapshotResponse{}

	term, _ := vs.l.GetPersistentState()
	rp.Term = pb.Uint64(term)

	if ss.GetTerm() < term {
		rp.Status = proto.InstallSnapshotResponse_WRONG_TERM.Enum()
		return rp
	} else {
		vs.l.SetPersistentState(ss.GetTerm(), ss.GetLeaderId())
	}

	now := time.Now()
	splitter := []byte(storage.STORAGE_KEY_PREFIX)

	for _, e := range ss.GetSnapshot() {
		v, err := e.Entry.Marshal()

		if err == nil && (e.GetType() == proto.Entry_PUT || e.GetType() == proto.Entry_LOCK) {
			vs.st.Set(e.GetKey(), v)

			if e.GetType() == proto.Entry_LOCK &&
				e.GetEntry().GetExpiration() > 0 && now.Before(time.Unix(0, int64(e.GetEntry().GetExpiration()))) {

				splitup := bytes.Split(e.GetKey(), splitter)
				var actual_key []byte

				if len(splitup) >= 2 {
					actual_key = splitup[1]
				} else {
					actual_key = splitup[0]
				}

				vs.tm.SetTimeout(string(actual_key), time.Unix(0, int64(e.GetEntry().GetExpiration())))
			}
		}
	}

	if ss.GetDone() {
		// Create fake log entry
		vs.l.Append(ss.GetLastIncluded().GetEntry().GetVersion().GetLogIx(), []*proto.Entry{ss.GetLastIncluded()})
		vs.l.Delete(ss.GetLastIncluded().GetEntry().GetVersion().GetLogIx() - 1)

		vs.l.SetCommitted(ss.GetLastIncluded().GetEntry().GetVersion().GetLogIx())
	}

	rp.Status = proto.InstallSnapshotResponse_OK.Enum()

	return rp

}

func (vs *versionedStorageImpl) AppendEntries(rq *proto.AppendEntriesRequest) (*proto.AppendEntriesResponse, error) {
	rp := &proto.AppendEntriesResponse{}

	term := vs.Term()

	next, commit, _ := vs.Index()

	rp.Term = pb.Uint64(term)
	rp.Success = pb.Bool(true)

	if rq.GetTerm() != term {
		if rq.GetTerm() < term {
			rp.Success = pb.Bool(false)

			return rp, nil
		} else if rq.GetTerm() > term {
			rp.Success = pb.Bool(true)
			rp.Term = pb.Uint64(rq.GetTerm())

			for vs.Term() < rq.GetTerm() {
				vs.SetNextTerm(rq.GetLeaderId())
			}
		}
	}

	if next > rq.GetLeaderCommitIx() && rq.GetLeaderCommitIx() > commit {
		vs.Commit(rq.GetLeaderCommitIx())
	}

	// Invariant
	if len(rq.GetEntries()) > 0 && rq.GetPreviousLogVersion().GetLogIx()+1 != rq.GetEntries()[0].GetEntry().GetVersion().GetLogIx() {
		rp.Success = pb.Bool(false)
		rp.LastIndex = pb.Uint64(next - 1)

		return rp, fmt.Errorf("Invariant failed: %d vs %d", rq.GetPreviousLogVersion().GetLogIx()+1, rq.GetEntries()[0].GetEntry().GetVersion().GetLogIx())
	}

	if len(rq.GetEntries()) > 0 && rq.GetPreviousLogVersion().GetLogIx() < next {
		previous_entry := vs.l.GetLogEntry(rq.GetPreviousLogVersion().GetLogIx())

		if previous_entry == nil {
			rp.LastIndex = pb.Uint64(next - 1)
			rp.Success = pb.Bool(false)

			return rp, fmt.Errorf("Previous entry was nil, ix %d, requesting %d", rq.GetPreviousLogVersion().GetLogIx(), next-1)
		} else if previous_entry.GetEntry().GetVersion().GetTerm() != rq.GetPreviousLogVersion().GetTerm() {

			rp.Success = pb.Bool(false)

			conflicting_term := rq.GetPreviousLogVersion().GetTerm()

			// Find first
			for i := previous_entry.GetEntry().GetVersion().GetLogIx(); i > 0; i-- {
				if vs.l.GetLogEntry(i-1).GetEntry().GetVersion().GetTerm() != conflicting_term {
					rp.LastIndex = pb.Uint64(vs.l.GetLogEntry(i).GetEntry().GetVersion().GetLogIx())
					break
				}
			}

			return rp, fmt.Errorf("Term mismatch, %d vs %d", term, conflicting_term)
		} else {
			vs.l.Append(rq.GetEntries()[0].GetEntry().GetVersion().GetLogIx(), rq.GetEntries())
			rp.Success = pb.Bool(true)
			return rp, nil

		}
	} else if len(rq.GetEntries()) == 0 { // Heartbeat
		rp.Success = pb.Bool(true)
		return rp, nil
	} else {
		rp.LastIndex = pb.Uint64(next - 1)
		rp.Success = pb.Bool(false)

		return rp, fmt.Errorf("Previous log version is higher than next, %d vs %d", rq.GetPreviousLogVersion().GetLogIx(), next)
	}
}

func (vs *versionedStorageImpl) ActionGet(crq *proto.Action) *proto.ActionResult {
	if crq.GetType() != proto.Action_GET {
		return nil
	}

	var err error

	result := new(proto.ActionResult)
	dbentry, err := vs.Get(crq.GetKey())

	if err != nil {
		result.Status = proto.ActionResult_NOT_FOUND.Enum()
	} else {
		dbentry_p := new(proto.DBEntry)
		err = dbentry_p.Unmarshal(dbentry)

		if err != nil {
			result.Status = proto.ActionResult_ERROR.Enum()
		} else if dbentry_p.GetExpiration() > 0 && time.Now().After(time.Unix(0, int64(dbentry_p.GetExpiration()))) {
			result.Status = proto.ActionResult_NOT_FOUND.Enum()
		} else if dbentry_p.GetIsDeleted() {
			result.Status = proto.ActionResult_NOT_FOUND.Enum()
		} else {
			result.Status = proto.ActionResult_SUCCESS.Enum()
			result.Result = dbentry_p.GetValue()
			result.CommitIx = dbentry_p.GetVersion().LogIx
			result.Holder = dbentry_p.HolderClientId
			result.CounterResult = dbentry_p.CounterValue
		}
	}

	return result
}

// Handles CHECK_* actions. They need special treatment: Normal actions usually succeed when a key doesn't
// exist, checks usually don't.
func (vs *versionedStorageImpl) doCheck(check *proto.Action, client_id string) (bool, *proto.ActionResult) {
	raw, err := vs.Get(check.GetKey())
	dbentry := new(proto.DBEntry)

	if err != nil || raw == nil {
		dbentry = nil
	} else {
		err = dbentry.Unmarshal(raw)

		if err != nil {
			dbentry = nil
		}
	}

	var r bool

	switch check.GetType() {
	case proto.Action_CHECK_EQ:
		if dbentry != nil {
			r = bytes.Equal(check.GetValue(), dbentry.GetValue())
		} else if check.GetValue() == nil || bytes.Equal(check.GetValue(), []byte{}) {
			r = true
		} else {
			r = false
		}
		return r, &proto.ActionResult{Status: proto.ActionResult_SUCCESS.Enum(), CheckOk: pb.Bool(r)}

	case proto.Action_CHECK_COUNTER_EQ:
		if dbentry != nil {
			r = check.GetCounterOp().GetVal() == dbentry.GetCounterValue()
		} else if check.GetCounterOp().GetVal() == 0 {
			r = true
		} else {
			r = false
		}
		return r, &proto.ActionResult{Status: proto.ActionResult_SUCCESS.Enum(), CheckOk: pb.Bool(r)}

	case proto.Action_CHECK_VERSION_EQ:
		if dbentry != nil {
			r = check.GetOnVersion() == dbentry.GetVersion().GetLogIx()
		} else if check.GetOnVersion() == 0 {
			r = true
		} else {
			r = false
		}
		return r, &proto.ActionResult{Status: proto.ActionResult_SUCCESS.Enum(), CheckOk: pb.Bool(r)}

	case proto.Action_CHECK_VERSION_LE:
		if dbentry != nil {
			r = check.GetOnVersion() >= dbentry.GetVersion().GetLogIx()
		} else {
			r = false
		}
		return r, &proto.ActionResult{Status: proto.ActionResult_SUCCESS.Enum(), CheckOk: pb.Bool(r)}
	}
	return true, nil
}

func (vs *versionedStorageImpl) checkWriteAction(action *proto.Action, client_id string) (bool, *proto.ActionResult) {
	raw, err := vs.Get(action.GetKey())

	if err != nil || raw == nil {
		return true, nil
	}

	dbentry := new(proto.DBEntry)

	err = dbentry.Unmarshal(raw)

	if err != nil {
		return true, nil
	}

	switch action.GetType() {
	case proto.Action_SET, proto.Action_DELETE:
		if action.GetOnVersion() > 0 && dbentry.GetVersion().GetLogIx() > action.GetOnVersion() {
			return false, &proto.ActionResult{Status: proto.ActionResult_ERROR_WRONG_VERSION.Enum(),
				CommitIx: dbentry.Version.LogIx,
				Holder:   dbentry.HolderClientId}
		}
		return true, nil
	case proto.Action_LOCK, proto.Action_UNLOCK:
		if dbentry.GetIsDeleted() {
			return true, nil
		}
		if dbentry.GetHolderClientId() == client_id || dbentry.GetHolderClientId() == "" {
			return true, nil
		}
		if time.Unix(0, int64(dbentry.GetExpiration())).Sub(time.Now()) < 0 {
			return true, nil
		}
		return false, &proto.ActionResult{Status: proto.ActionResult_ERROR_LOCKED.Enum(),
			Holder: dbentry.HolderClientId}
	}
	return true, nil
}

// Checks if the operation a client wants to do is allowed, i.e.
// - if the lock is lockable for that client
// - if the on-version condition of the request is fulfilled.
// - if the check succeeds
// If the operation is allowed, returns true and a result (which may or may not be nil)
// If not, returns false and an error result.
// This function may NEVER return false,nil
func (vs *versionedStorageImpl) WillSucceed(action *proto.Action, client_id string) (bool, *proto.ActionResult) {
	if action.GetType() == proto.Action_GET {
		return true, nil
	}

	switch action.GetType() {
	case proto.Action_SET, proto.Action_DELETE:
		return vs.checkWriteAction(action, client_id)
	case proto.Action_LOCK, proto.Action_UNLOCK:
		return vs.checkWriteAction(action, client_id)
	case proto.Action_CHECK_EQ, proto.Action_CHECK_COUNTER_EQ, proto.Action_CHECK_VERSION_EQ, proto.Action_CHECK_VERSION_LE:
		return vs.doCheck(action, client_id)
	}
	return true, nil
}
