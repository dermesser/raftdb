package versionedstorage

import (
	"raftdb/proto"
	"raftdb/server/storage"
	"time"

	pb "github.com/gogo/protobuf/proto"
)

type OpFunc (func(storage.ReadOnlyStorage) []*proto.Entry)

func (f OpFunc) GetEntries(s storage.ReadOnlyStorage) []*proto.Entry {
	return f(s)
}

type entryOp struct {
	e []*proto.Entry
}

func Entry2Op(e *proto.Entry) Op {
	return entryOp{e: []*proto.Entry{e}}
}

func Entries2Op(t []*proto.Entry) Op {
	return entryOp{e: t}
}

func (eo entryOp) GetEntries(st storage.ReadOnlyStorage) []*proto.Entry {
	return eo.e
}

type appendEntriesOp proto.AppendEntriesRequest

func (aeo *appendEntriesOp) GetEntries(st storage.ReadOnlyStorage) []*proto.Entry {
	return aeo.Entries
}

func AE2Op(rq *proto.AppendEntriesRequest) Op {
	return (*appendEntriesOp)(rq)
}

// IMPORTANT: This function doesn't check whether the client is allowed to lock
// or unlock; call WillSucceed first to determine whether the operation
// is allowed.
func Actions2Op(actions []*proto.Action, client_id string) Op {
	return OpFunc(func(st storage.ReadOnlyStorage) []*proto.Entry {
		entries := make([]*proto.Entry, len(actions))
		var entries_ix int

		for i := range actions {
			action := actions[i]

			entry := new(proto.Entry)
			entries[entries_ix] = entry

			entry.Key = action.GetKey()
			entry.Entry = new(proto.DBEntry)

			switch action.GetType() {
			case proto.Action_SET:
				entry.Type = proto.Entry_PUT.Enum()
				entry.Entry.Type = proto.DBEntry_DATA.Enum()
				entry.Entry.HolderClientId = pb.String(client_id)
			case proto.Action_LOCK:
				entry.Type = proto.Entry_LOCK.Enum()
				entry.Entry.Type = proto.DBEntry_LOCK.Enum()
				entry.Entry.HolderClientId = pb.String(client_id)
			case proto.Action_COUNT:
				entry.Type = proto.Entry_PUT.Enum()
				entry.Entry.Type = proto.DBEntry_COUNTER.Enum()
				entry.Entry.HolderClientId = pb.String(client_id)
				new_counter_value := getNewCounterValue(st, action.GetKey(), action.GetCounterOp())
				entry.Entry.CounterValue = pb.Int64(new_counter_value)
				action.Value = []byte{}
			case proto.Action_UNLOCK:
				entry.Type = proto.Entry_UNLOCK.Enum()
				entry.Entry.Type = proto.DBEntry_LOCK.Enum()
				action.Value = []byte{}
				entry.Entry.IsDeleted = pb.Bool(true)
			case proto.Action_DELETE:
				entry.Type = proto.Entry_DELETE.Enum()
				entry.Entry.Type = proto.DBEntry_DATA.Enum()
				action.Value = []byte{}
				entry.Entry.IsDeleted = pb.Bool(true)
				entry.Entry.HolderClientId = pb.String(client_id)
			default: // Skipping CHECK_ actions
				continue
			}

			entries_ix++
			entry.Entry.Value = action.GetValue()

			if action.GetType() == proto.Action_LOCK {

				if action.GetExpiration() != 0 {
					entry.Entry.Expiration = pb.Uint64(uint64(time.Now().Add(time.Duration(action.GetExpiration()) * time.Nanosecond).UnixNano()))
				} else {
					entry.Entry.Expiration = pb.Uint64(uint64((time.Now().Add(60 * time.Second)).UnixNano()))
				}
			}
		}
		return entries[0:entries_ix]
	})
}

// Returns the new counter value to set the counter to, i.e. does the math (current value + delta)
func getNewCounterValue(st storage.ReadOnlyStorage, key []byte, op *proto.CounterOp) int64 {
	if op.GetType() == proto.CounterOp_SET {
		return op.GetVal()
	} else {
		current_entry, err := st.Get(key)

		// Say 0 if the counter doesn't exist yet
		if current_entry == nil || err != nil {
			return op.GetVal()
		}

		counter := new(proto.DBEntry)
		err = counter.Unmarshal(current_entry)

		if err != nil {
			return op.GetVal()
		}

		return counter.GetCounterValue() + op.GetVal()
	}
}
