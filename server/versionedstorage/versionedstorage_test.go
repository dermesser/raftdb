package versionedstorage

import (
	"bytes"
	"raftdb/proto"
	"raftdb/server/locking"
	"raftdb/server/log"
	"raftdb/server/storage"
	"testing"
	"time"

	pb "github.com/gogo/protobuf/proto"
)

// Returns a versionedStorage with mocked storage and log
func getVS() VersionedStorage {
	vs := NewVersionedStorage(locking.NewMNManager(), storage.NewTempStorage(), log.NewStorageLog(storage.NewTempStorage()))
	vs.SetNextTerm("")
	return vs
}

func getTestEntries() []*proto.Entry {
	return []*proto.Entry{&proto.Entry{Type: proto.Entry_DELETE.Enum(),
		Key: []byte("abc"),
		Entry: &proto.DBEntry{
			Type:    proto.DBEntry_DATA.Enum(),
			Value:   []byte(""),
			Version: &proto.VersionInfo{Term: pb.Uint64(0), LogIx: pb.Uint64(0)}, // those should be set in prepare/log.Append
		},
	},
		&proto.Entry{Type: proto.Entry_LOCK.Enum(),
			Key: []byte("abc"),
			Entry: &proto.DBEntry{
				Type:       proto.DBEntry_DATA.Enum(),
				Value:      []byte("def"),
				Version:    &proto.VersionInfo{Term: pb.Uint64(0), LogIx: pb.Uint64(0)}, // those should be set in prepare/log.Append
				Expiration: pb.Uint64(uint64(time.Now().Add(6 * time.Second).UnixNano())),
			},
		},
		&proto.Entry{Type: proto.Entry_UNLOCK.Enum(),
			Key: []byte("abc"),
			Entry: &proto.DBEntry{
				Type:    proto.DBEntry_DATA.Enum(),
				Value:   []byte(""),
				Version: &proto.VersionInfo{Term: pb.Uint64(0), LogIx: pb.Uint64(0)}, // those should be set in prepare/log.Append
			},
		},
		&proto.Entry{Type: proto.Entry_PUT.Enum(),
			Key: []byte("abc"),
			Entry: &proto.DBEntry{
				Type:    proto.DBEntry_DATA.Enum(),
				Value:   []byte("def"),
				Version: &proto.VersionInfo{Term: pb.Uint64(0), LogIx: pb.Uint64(0)}, // those should be set in prepare/log.Append
			},
		},
	}
}

func TestInterfaceFulfilled(t *testing.T) {
	var vs VersionedStorage = &versionedStorageImpl{}
	var ros storage.ReadOnlyStorage = vs
	var rol log.ReadOnlyLog = vs
	_ = ros
	_ = rol
}

func TestConstructor(t *testing.T) {
	vs := NewVersionedStorage(locking.NewMNManager(), storage.NewTempStorage(), log.NewStorageLog(storage.NewTempStorage()))
	vsi := vs.(*versionedStorageImpl)

	if vsi.l == nil {
		t.Error("Log not set")
	}
	if vsi.tm == nil {
		t.Error("TM not set")
	}
	if vsi.st == nil {
		t.Error("Storage not set")
	}
	if vsi.n == nil {
		t.Error("NM not set")
	}

	vs = NewIntegratedVersionedStorage(locking.NewMNManager(), storage.NewTempStorage())
	vsi = vs.(*versionedStorageImpl)

	if vsi.l == nil {
		t.Error("IVS: Log not set")
	}
	if vsi.tm == nil {
		t.Error("IVS: TM not set")
	}
	if vsi.st == nil {
		t.Error("IVS: Storage not set")
	}
	if vsi.n == nil {
		t.Error("IVS: NM not set")
	}
}

func TestSetNextTerm(t *testing.T) {
	vs := getVS()
	defer vs.Close()

	if vs.Term() != 1 {
		t.Error("Initial term not 1")
	}

	vs.SetNextTerm("abc")

	if vs.Term() != 2 {
		t.Error("Next term not 2")
	}

	te, vf := vs.GetPersistentState()

	if te != 2 || vf != "abc" {
		t.Error("Wrong persistent state:", te, vf)
	}
}

func TestIndices(t *testing.T) {
	vs := getVS()
	defer vs.Close()

	next, commit, first := vs.Index()

	if next != 1 || commit != 0 || first != 0 {
		t.Error("Wrong initial indices:", next, commit, first)
	}

	e := getTestEntries()

	newix, _ := vs.Prepare(Entries2Op(e))
	next, commit, first = vs.Index()

	if newix != 4 {
		t.Error("Newix != 1:", newix)
	}
	if next != 5 || commit != 0 || first != 1 {
		t.Error("Wrong indices:", next, commit, first)
	}
	if e[0].GetEntry().GetVersion().GetLogIx() != 1 || e[0].GetEntry().GetVersion().GetTerm() != 1 {
		t.Error("Term or logix not set:", e[0].String())
	}

	vs.Commit(newix)
	next, commit, first = vs.Index()

	if next != 5 || commit != 4 || first != 1 {
		t.Error("Wrong indices:", next, commit, first)
	}

	if _, err := vs.Get([]byte("abc")); err != nil {
		t.Error("Entry not stored:", err)
	}
}

func TestTooHighCommit(t *testing.T) {
	vs := getVS()
	defer vs.Close()

	err := vs.Commit(1)

	if err == nil {
		t.Error("Commit() did not fail")
	}
}

func TestPrepareRevert(t *testing.T) {
	vs := getVS()
	defer vs.Close()

	next, commit, first := vs.Index()

	if next != 1 || commit != 0 || first != 0 {
		t.Error("Wrong initial indices:", next, commit, first)
	}

	// Get PUT entry out of all
	e := getTestEntries()[3]

	newix, _ := vs.Prepare(Entry2Op(e))
	next, commit, first = vs.Index()

	if newix != 1 {
		t.Error("Newix != 1:", newix)
	}
	if next != 2 || commit != 0 || first != 1 {
		t.Error("Wrong indices:", next, commit, first)
	}
	if e.GetEntry().GetVersion().GetLogIx() != 1 || e.GetEntry().GetVersion().GetTerm() != 1 {
		t.Error("Term or logix not set:", e.String())
	}

	vs.Revert()
	next, commit, first = vs.Index()

	if next != 1 || commit != 0 || first != 1 {
		t.Error("Wrong indices:", next, commit, first)
	}
}

func TestNotifications(t *testing.T) {
	vs := getVS()
	defer vs.Close()
	vsi := vs.(*versionedStorageImpl)

	w := vsi.n.Waiter("abc")
	vs.Broadcast(locking.MasterStepDown)

	ev := w.Wait(100 * time.Millisecond)

	if ev != locking.MasterStepDown {
		t.Error("Wrong event:", ev.String())
	}
}

func TestTimeoutManager(t *testing.T) {
	vs := getVS()
	defer vs.Close()
	vsi := vs.(*versionedStorageImpl)

	vs.StartTimeoutManager(50 * time.Millisecond)

	w := vsi.n.Waiter("abc")
	vsi.tm.SetTimeout("abc", time.Now().Add(50*time.Millisecond))

	ev := w.Wait(300 * time.Millisecond)

	if ev != locking.ReleasedTimedOut {
		t.Error("Wrong event:", ev.String())
	}

	vs.StopTimeoutManager()

	vsi.tm.SetTimeout("abc", time.Now().Add(50*time.Millisecond))
	ev = w.Wait(300 * time.Millisecond)

	if ev != locking.Timeout {
		t.Error("Wrong event:", ev.String())
	}
}

func TestReset(t *testing.T) {
	vs := getVS()
	defer vs.Close()

	next, commit, first := vs.Index()

	if next != 1 || commit != 0 || first != 0 {
		t.Error("Wrong initial indices:", next, commit, first)
	}

	e := &proto.Entry{Type: proto.Entry_PUT.Enum(),
		Key: []byte("abc"),
		Entry: &proto.DBEntry{
			Type:    proto.DBEntry_DATA.Enum(),
			Value:   []byte("def"),
			Version: &proto.VersionInfo{Term: pb.Uint64(0), LogIx: pb.Uint64(0)}, // those should be set in prepare/log.Append
		},
	}

	newix, _ := vs.Prepare(Entry2Op(e))
	next, commit, first = vs.Index()

	if newix != 1 {
		t.Error("Newix != 1:", newix)
	}
	if next != 2 || commit != 0 || first != 1 {
		t.Error("Wrong indices:", next, commit, first)
	}
	if e.GetEntry().GetVersion().GetLogIx() != 1 || e.GetEntry().GetVersion().GetTerm() != 1 {
		t.Error("Term or logix not set:", e.String())
	}

	vs.Commit(newix)
	next, commit, first = vs.Index()

	if next != 2 || commit != 1 || first != 1 {
		t.Error("Wrong indices:", next, commit, first)
	}
	if _, err := vs.Get([]byte("abc")); err != nil {
		t.Error("Entry not stored:", err)
	}

	vs.Reset()
	next, commit, first = vs.Index()

	if _, err := vs.Get([]byte("abc")); err == nil {
		t.Error("Entry still stored:", err)
	}
	if next != 1 || commit != 0 || first != 0 {
		t.Error("Wrong indices:", next, commit, first)
	}
}

func TestAppendEntries(t *testing.T) {
	vs := getVS()
	localvs := getVS()
	defer vs.Close()
	defer localvs.Close()

	newix, entries := localvs.Prepare(Entries2Op(getTestEntries()))

	aerq := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId:           pb.String("x"),
		PreviousLogVersion: &proto.VersionInfo{LogIx: pb.Uint64(0), Term: pb.Uint64(1)},
		Entries:            entries,
		LeaderCommitIx:     pb.Uint64(0),
		ClusterId:          pb.String("test"),
	}

	rp, err := vs.AppendEntries(aerq)

	if err != nil {
		t.Error(err)
	}
	if !rp.GetSuccess() {
		t.Error("No success")
	}

	localvs.Commit(newix)

	aerq_commit := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId:       pb.String("x"),
		Entries:        []*proto.Entry{},
		LeaderCommitIx: pb.Uint64(newix),
		ClusterId:      pb.String("test"),
	}

	rp, err = vs.AppendEntries(aerq_commit)

	if err != nil {
		t.Error(err)
	}
	if !rp.GetSuccess() {
		t.Error("No success")
	}
}

func TestAppendEntriesInvariantFail(t *testing.T) {
	vs := getVS()
	localvs := getVS()
	defer vs.Close()
	defer localvs.Close()

	_, entries := localvs.Prepare(Entries2Op(getTestEntries()))

	aerq := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId:           pb.String("x"),
		PreviousLogVersion: &proto.VersionInfo{LogIx: pb.Uint64(2), Term: pb.Uint64(1)},
		Entries:            entries,
		LeaderCommitIx:     pb.Uint64(0),
		ClusterId:          pb.String("test"),
	}
	rp, err := vs.AppendEntries(aerq)

	if err == nil {
		t.Error("Expected FailedInvariant error, got nil")
	}
	if rp.GetSuccess() {
		t.Error("Expected no success")
	}
}

func TestAppendEntriesNotCaughtUp(t *testing.T) {
	vs := getVS()
	localvs := getVS()
	defer vs.Close()
	defer localvs.Close()

	newix, entries := localvs.Prepare(Entries2Op(getTestEntries()))
	localvs.Commit(newix)

	newix, entries = localvs.Prepare(Entries2Op(getTestEntries()))

	aerq := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId:           pb.String("x"),
		PreviousLogVersion: &proto.VersionInfo{LogIx: pb.Uint64(4), Term: pb.Uint64(1)},
		Entries:            entries,
		LeaderCommitIx:     pb.Uint64(4),
		ClusterId:          pb.String("test"),
	}
	rp, err := vs.AppendEntries(aerq)

	if err == nil {
		t.Error("Expected FailedInvariant error, got nil")
	}
	if rp.GetSuccess() {
		t.Error("Expected no success")
	}
	if rp.GetLastIndex() != 0 {
		t.Error("Wrong LastIndex:", rp.GetLastIndex())
	}
}

func TestAppendEntriesPreviousEntryMissing(t *testing.T) {
	vs := getVS()
	localvs := getVS()
	defer vs.Close()
	defer localvs.Close()

	newix, entries := localvs.Prepare(Entries2Op(getTestEntries()))
	localvs.Commit(newix)

	newix, entries = localvs.Prepare(Entries2Op(getTestEntries()))

	aerq := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId:           pb.String("x"),
		PreviousLogVersion: &proto.VersionInfo{LogIx: pb.Uint64(4), Term: pb.Uint64(1)},
		Entries:            entries,
		LeaderCommitIx:     pb.Uint64(4),
		ClusterId:          pb.String("test"),
	}

	vs.Reset()
	vs.(*versionedStorageImpl).l.ResetNext(5)
	rp, err := vs.AppendEntries(aerq)

	if err == nil {
		t.Error("Expected PreviousEntryMissing error, got nil")
	}
	if rp.GetSuccess() {
		t.Error("Expected no success")
	}
	if rp.GetLastIndex() != 4 {
		t.Error("Wrong LastIndex:", rp.GetLastIndex())
	}
}

func TestAppendLogTermMismatch(t *testing.T) {
	vs := getVS()
	localvs := getVS()
	defer vs.Close()
	defer localvs.Close()

	newix, entries := localvs.Prepare(Entries2Op(getTestEntries()))

	aerq := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId:           pb.String("x"),
		PreviousLogVersion: &proto.VersionInfo{LogIx: pb.Uint64(0), Term: pb.Uint64(1)},
		Entries:            entries,
		LeaderCommitIx:     pb.Uint64(0),
		ClusterId:          pb.String("test"),
	}

	rp, err := vs.AppendEntries(aerq)

	if err != nil {
		t.Error(err)
	}
	if !rp.GetSuccess() {
		t.Error("No success")
	}

	localvs.Commit(newix)

	newix, entries = localvs.Prepare(Entries2Op(getTestEntries()))

	aerq_commit := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId:           pb.String("x"),
		Entries:            entries,
		PreviousLogVersion: &proto.VersionInfo{LogIx: pb.Uint64(4), Term: pb.Uint64(2)},
		LeaderCommitIx:     pb.Uint64(newix),
		ClusterId:          pb.String("test"),
	}

	rp, err = vs.AppendEntries(aerq_commit)

	if err == nil {
		t.Error("Expected error (term mismatch), got nil")
	}
	if rp.GetSuccess() {
		t.Error("Expected no success")
	}
	if rp.GetLastIndex() != 4 {
		t.Error("Wrong LastIndex", rp.GetLastIndex())
	}
}

func TestInstallSnapshot(t *testing.T) {
	vs := getVS()
	localvs := getVS()
	defer vs.Close()
	defer localvs.Close()

	newix, entries := localvs.Prepare(Entries2Op(getTestEntries()))
	localvs.Commit(newix)

	isrq := &proto.InstallSnapshot{Term: pb.Uint64(1),
		LeaderId:     pb.String("x"),
		Snapshot:     entries,
		LastIncluded: entries[len(entries)-1],
		Done:         pb.Bool(false),
		ClusterId:    pb.String("y"),
	}
	done_rq := &proto.InstallSnapshot{Term: pb.Uint64(1),
		LeaderId:     pb.String("x"),
		Snapshot:     []*proto.Entry{},
		LastIncluded: entries[len(entries)-1],
		Done:         pb.Bool(true),
		ClusterId:    pb.String("y"),
	}

	rp := vs.InstallSnapshot(isrq)

	if rp.GetStatus() != proto.InstallSnapshotResponse_OK {
		t.Error("Wrong status:", rp.GetStatus().String())
	}
	if rp.GetTerm() != 1 {
		t.Error("Wrong term", rp.GetTerm())
	}

	if r, err := vs.Get([]byte("abc")); r == nil || err != nil {
		t.Error("Couldn't find entry in storage:", r, err)
	}

	rp = vs.InstallSnapshot(done_rq)

	if rp.GetStatus() != proto.InstallSnapshotResponse_OK {
		t.Error("Wrong status:", rp.GetStatus().String())
	}
	if rp.GetTerm() != 1 {
		t.Error("Wrong term", rp.GetTerm())
	}

	if r, err := vs.Get([]byte("abc")); r == nil || err != nil {
		t.Error("Couldn't find entry in storage:", r, err)
	}

	if _, err := vs.(*versionedStorageImpl).tm.HasTimeout("abc"); err != nil {
		t.Error("Should have timeout set")
	}

	isrq = &proto.InstallSnapshot{Term: pb.Uint64(0),
		LeaderId:     pb.String("x"),
		Snapshot:     entries,
		LastIncluded: entries[len(entries)-1],
		Done:         pb.Bool(false),
		ClusterId:    pb.String("y"),
	}

	rp = vs.InstallSnapshot(isrq)

	if rp.GetStatus() != proto.InstallSnapshotResponse_WRONG_TERM {
		t.Error("Wrong status:", rp.GetStatus().String())
	}
}

func TestClientRequestGet(t *testing.T) {
	vs := getVS()
	defer vs.Close()

	dbe := &proto.DBEntry{
		Type:    proto.DBEntry_DATA.Enum(),
		Value:   []byte("def"),
		Version: &proto.VersionInfo{Term: pb.Uint64(0), LogIx: pb.Uint64(0)}, // those should be set in prepare/log.Append
	}
	dbe2 := &proto.DBEntry{
		Type:       proto.DBEntry_DATA.Enum(),
		Value:      []byte("def"),
		Version:    &proto.VersionInfo{Term: pb.Uint64(0), LogIx: pb.Uint64(0)}, // those should be set in prepare/log.Append
		Expiration: pb.Uint64(uint64(time.Now().Add(-1 * time.Minute).UnixNano())),
	}

	dbeb, err := dbe.Marshal()

	if err != nil {
		t.Error(err)
	}

	vs.(*versionedStorageImpl).st.Set([]byte("abc"), dbeb)

	dbeb2, err := dbe2.Marshal()

	if err != nil {
		t.Error(err)
	}

	vs.(*versionedStorageImpl).st.Set([]byte("xyz"), dbeb2)

	action := &proto.Action{Type: proto.Action_GET.Enum(),
		Key: []byte("abc"),
	}

	actionresult := vs.ActionGet(action)

	if actionresult.GetStatus() != proto.ActionResult_SUCCESS {
		t.Error("Wrong status:", actionresult.GetStatus().String())
	}
	if !bytes.Equal(actionresult.GetResult(), dbe.GetValue()) {
		t.Error("Wrong result:", string(actionresult.GetResult()), "vs. def")
	}

	// Test non-existing entry

	action.Key = []byte("123")

	actionresult = vs.ActionGet(action)

	if actionresult.GetStatus() != proto.ActionResult_NOT_FOUND {
		t.Error("Wrong status:", actionresult.GetStatus().String())
	}

	// Test expired entry

	action2 := &proto.Action{Type: proto.Action_GET.Enum(),
		Key: []byte("xyz"),
	}

	actionresult2 := vs.ActionGet(action2)

	if actionresult2.GetStatus() != proto.ActionResult_NOT_FOUND {
		t.Error("Wrong status:", actionresult2.GetStatus().String())
	}

}

func TestWillSucceedCHECKs(t *testing.T) {
	vs := getVS()
	defer vs.Close()

	dbe := &proto.DBEntry{
		Type:           proto.DBEntry_LOCK.Enum(),
		Value:          []byte("def"),
		Version:        &proto.VersionInfo{Term: pb.Uint64(1), LogIx: pb.Uint64(2)}, // those should be set in prepare/log.Append
		Expiration:     pb.Uint64(uint64(time.Now().Add(1 * time.Minute).UnixNano())),
		HolderClientId: pb.String("someone"),
	}

	dbeb, err := dbe.Marshal()
	if err != nil {
		t.Error(err)
	}
	vs.(*versionedStorageImpl).st.Set([]byte("abc"), dbeb)

	dbe2 := &proto.DBEntry{
		Type:           proto.DBEntry_COUNTER.Enum(),
		CounterValue:   pb.Int64(32),
		Value:          []byte{},
		Version:        &proto.VersionInfo{Term: pb.Uint64(1), LogIx: pb.Uint64(2)}, // those should be set in prepare/log.Append
		Expiration:     pb.Uint64(uint64(time.Now().Add(1 * time.Minute).UnixNano())),
		HolderClientId: pb.String("someone"),
	}

	dbe2b, err := dbe2.Marshal()
	if err != nil {
		t.Error(err)
	}
	vs.(*versionedStorageImpl).st.Set([]byte("cnt1"), dbe2b)

	cr := &proto.Action{Type: proto.Action_CHECK_EQ.Enum(), Key: []byte("abc"), Value: []byte("def")}

	if succ, err_rp := vs.WillSucceed(cr, "someone"); !succ {
		t.Error("Wrong result from WillSucceed for CHECK_EQ", succ, err_rp)
	}

	cr = &proto.Action{Type: proto.Action_CHECK_VERSION_EQ.Enum(), Key: []byte("abc"), OnVersion: pb.Uint64(2)}

	if succ, err_rp := vs.WillSucceed(cr, "someone"); !succ {
		t.Error("Wrong result from WillSucceed for CHECK_VERSION_EQ", succ, err_rp)
	}

	cr = &proto.Action{Type: proto.Action_CHECK_VERSION_EQ.Enum(), Key: []byte("abc"), OnVersion: pb.Uint64(1)}

	if succ, err_rp := vs.WillSucceed(cr, "someone"); succ {
		t.Error("Wrong result from WillSucceed for CHECK_VERSION_EQ", succ, err_rp)
	}

	cr = &proto.Action{Type: proto.Action_CHECK_VERSION_LE.Enum(), Key: []byte("abc"), OnVersion: pb.Uint64(4)}

	if succ, err_rp := vs.WillSucceed(cr, "someone"); !succ {
		t.Error("Wrong result from WillSucceed for CHECK_VERSION_LE", succ, err_rp)
	}

	cr = &proto.Action{Type: proto.Action_CHECK_COUNTER_EQ.Enum(), Key: []byte("cnt1"),
		CounterOp: &proto.CounterOp{Type: proto.CounterOp_CHECK.Enum(), Val: pb.Int64(32)}}

	if succ, err_rp := vs.WillSucceed(cr, "someone"); !succ {
		t.Error("Wrong result from WillSucceed for CHECK_COUNTER_EQ", succ, err_rp)
	}

	cr = &proto.Action{Type: proto.Action_CHECK_COUNTER_EQ.Enum(), Key: []byte("cnt1"),
		CounterOp: &proto.CounterOp{Type: proto.CounterOp_CHECK.Enum(), Val: pb.Int64(33)}}

	if succ, err_rp := vs.WillSucceed(cr, "someone"); succ {
		t.Error("Wrong result from WillSucceed for CHECK_COUNTER_EQ", succ, err_rp)
	}
}

func TestWillSucceed(t *testing.T) {
	vs := getVS()
	defer vs.Close()

	dbe := &proto.DBEntry{
		Type:           proto.DBEntry_LOCK.Enum(),
		Value:          []byte("def"),
		Version:        &proto.VersionInfo{Term: pb.Uint64(1), LogIx: pb.Uint64(2)}, // those should be set in prepare/log.Append
		Expiration:     pb.Uint64(uint64(time.Now().Add(1 * time.Minute).UnixNano())),
		HolderClientId: pb.String("someone"),
	}

	dbeb, err := dbe.Marshal()
	if err != nil {
		t.Error(err)
	}
	vs.(*versionedStorageImpl).st.Set([]byte("else"), dbeb)

	dbe.HolderClientId = pb.String("someone_else")
	dbeb, err = dbe.Marshal()
	if err != nil {
		t.Error(err)
	}
	vs.(*versionedStorageImpl).st.Set([]byte("we"), dbeb)

	dbe.HolderClientId = pb.String("someone")
	dbe.IsDeleted = pb.Bool(true)
	dbeb, err = dbe.Marshal()
	if err != nil {
		t.Error(err)
	}
	vs.(*versionedStorageImpl).st.Set([]byte("deleted"), dbeb)

	dbe.HolderClientId = pb.String("someone")
	dbe.IsDeleted = pb.Bool(false)
	dbe.Expiration = pb.Uint64(uint64(time.Now().Add(-1 * time.Minute).UnixNano()))
	dbeb, err = dbe.Marshal()
	if err != nil {
		t.Error(err)
	}
	vs.(*versionedStorageImpl).st.Set([]byte("expired"), dbeb)

	cr := &proto.Action{Type: proto.Action_LOCK.Enum(),
		Key: []byte("else"),
	}

	if succ, err_rp := vs.WillSucceed(cr, "someone_else"); succ || err_rp.GetStatus() != proto.ActionResult_ERROR_LOCKED {
		t.Error("Wrong result from WillSuceed", succ, err_rp)
	}

	cr.Key = []byte("xyz")

	if succ, err_rp := vs.WillSucceed(cr, "someone_else"); !succ {
		t.Error("Wrong result from WillSuceed", succ, err_rp)
	}

	cr.Key = []byte("we")

	if succ, err_rp := vs.WillSucceed(cr, "someone_else"); !succ {
		t.Error("Wrong result from WillSuceed", succ, err_rp)
	}

	cr.Key = []byte("deleted")

	if succ, err_rp := vs.WillSucceed(cr, "someone_else"); !succ {
		t.Error("Wrong result from WillSuceed", succ, err_rp)
	}

	cr.Key = []byte("expired")

	if succ, err_rp := vs.WillSucceed(cr, "someone_else"); !succ {
		t.Error("Wrong result from WillSuceed", succ, err_rp)
	}

	cr.Type = proto.Action_SET.Enum()
	cr.Key = []byte("expired")
	cr.OnVersion = pb.Uint64(1)

	if succ, err_rp := vs.WillSucceed(cr, "someone_else"); succ || err_rp.GetStatus() != proto.ActionResult_ERROR_WRONG_VERSION {
		t.Error("Wrong result from WillSuceed", succ, err_rp)
	}

	cr.OnVersion = nil

	if succ, err_rp := vs.WillSucceed(cr, "someone_else"); !succ {
		t.Error("Wrong result from WillSuceed", succ, err_rp)
	}

	cr.Type = proto.Action_SET.Enum()
	cr.Key = []byte("expired")
	cr.OnVersion = pb.Uint64(2)

	if succ, err_rp := vs.WillSucceed(cr, "someone_else"); !succ {
		t.Error("Wrong result from WillSuceed", succ, err_rp)
	}

	cr.Type = proto.Action_GET.Enum()
	cr.Key = []byte("expired")
	cr.OnVersion = pb.Uint64(0)

	if succ, err_rp := vs.WillSucceed(cr, "someone_else"); !succ {
		t.Error("Wrong result from WillSuceed", succ, err_rp)
	}
}
