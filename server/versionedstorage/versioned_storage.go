package versionedstorage

import (
	"raftdb/proto"
	"raftdb/server/locking"
	"raftdb/server/log"
	"raftdb/server/storage"
	"time"
)

// Represents a modification of the database
type Op interface {
	// Returns -- based on the current overall state -- a list of entries to append.
	GetEntries(st storage.ReadOnlyStorage) []*proto.Entry
}

// Represents a versioned persistent storage backend.
type VersionedStorage interface {
	storage.ReadOnlyStorage
	log.ReadOnlyLog

	// Checks if the client request will succeed
	WillSucceed(*proto.Action, string) (bool, *proto.ActionResult)
	// Writes the entry in the log in order for it to be committed later.
	Prepare(Op) (uint64, []*proto.Entry)
	// Reverts the previous Prepare
	Revert()
	// Commites until and including the given index
	Commit(uint64) error
	// Resets log and storage
	Reset()
	// Closes log and storage
	Close()

	// next index, committed index, first logged index
	Index() (uint64, uint64, uint64)
	Term() uint64
	SetNextTerm(votedfor string)

	Broadcast(locking.NotificationType)

	StartTimeoutManager(time.Duration)
	StopTimeoutManager()

	// // RAFT INTERFACE // //

	InstallSnapshot(ss *proto.InstallSnapshot) *proto.InstallSnapshotResponse
	AppendEntries(rq *proto.AppendEntriesRequest) (*proto.AppendEntriesResponse, error)
	ActionGet(*proto.Action) *proto.ActionResult
}

type ROVersionedStorage interface {
	storage.ReadOnlyStorage
	log.ReadOnlyLog

	// next index, committed index, first logged index
	Index() (uint64, uint64, uint64)
	Term() uint64
}
