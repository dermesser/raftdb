package server

// A processor is a member of a RaftDB cluster.

import (
	clkup "clusterlookup/proto"
	"errors"
	"os"
	"raftdb/proto"
	"raftdb/server/constants"
	"raftdb/server/locking"
	"raftdb/server/versionedstorage"
	"time"

	pb "github.com/gogo/protobuf/proto"
)

type state int

const (
	state_UNDEF state = iota
	state_LEADER
	state_FOLLOWER
	state_FOLLOWER_SNAPSHOTTING
	state_CANDIDATE
	state_FINALIZE
)

// Sent to coordinators and received from dispatcher
type RpcRequest struct {
	// RaftDB internal
	rqv *proto.RequestVote
	ae  *proto.AppendEntriesRequest
	is  *proto.InstallSnapshot

	// Clients
	cc *proto.ConfigRequest
	cr *proto.ClientRequest

	rpchan chan<- *RpcResponse
	// shut_down terminates the receiving thread (if applicable), ignore means to ignore
	// the message (useful for changes in state machines)
	shut_down, ignore bool
}

type RpcResponse struct {
	member_id string
	// When exited
	err error

	// RaftDB internal
	v  *proto.Vote
	ae *proto.AppendEntriesResponse
	is *proto.InstallSnapshotResponse

	// Clients
	cc *proto.ConfigResponse
	cr *proto.ClientResponse
}

// implements member
type processor struct {
	cluster *cluster

	// State machine
	st versionedstorage.VersionedStorage

	requests chan *RpcRequest

	member_id string
	leader_id string

	// FLAG_heartbeat_interval when leader, randomized value elsewhere
	timeout time.Duration
	// The main processor thread sends signals to this channel on received heartbeats.
	heartbeat_channel chan bool
	// Watched by the main thread, handled accordingly when changed
	processor_state state

	// If we are follower: Do not accept any votes until this point in time.
	// If we are leader: Until this point in time, we can be sure to be leader.
	// The implementation usually uses 75% of this "lease period" to make decisions,
	// in order to account for clock inaccuracies.
	leader_lease time.Time
}

// Also call one of convert*()!
func newProcessor(vs versionedstorage.VersionedStorage, cluster_id, name string) (*processor, error) {
	p := new(processor)
	p.cluster = newCluster(cluster_id)

	p.st = vs

	p.requests = make(chan *RpcRequest, FLAG_internal_request_queue)
	p.member_id = name
	p.leader_id = ""
	p.heartbeat_channel = make(chan bool, 5)

	// Calculate follower timeout. The heartbeat sender uses FLAG_heartbeat_interval as-is
	p.timeout = (time.Duration(float64(FLAG_heartbeat_interval)*1.5) + (time.Millisecond * time.Duration(randBetween(-int(FLAG_timer_variance/time.Millisecond), int(FLAG_timer_variance/time.Millisecond)))))

	raftdb_log(LOGLEVEL_INFO, p.cluster.getClusterId(), "Processor", name, "cluster", cluster_id, "was set up; timeout is", p.timeout)

	p.setConfigFromPersistent()

	return p, nil
}

func (p *processor) setConfigFromPersistent() {
	b, err := p.st.Get([]byte(constants.CONFIG_ENTRY_KEY))

	if err != nil {
		// no config yet, e.g. when starting cluster
		raftdb_log(LOGLEVEL_WARNINGS, "No config found in database")
		return
	}

	entry := new(proto.DBEntry)

	err = entry.Unmarshal(b)

	if err != nil {
		raftdb_log(LOGLEVEL_ERRORS, "Couldn't unmarshal entry!", err.Error())
		return
	}

	err = p.cluster.config.fromConfig(entry.GetValue())

	if err != nil {
		raftdb_log(LOGLEVEL_ERRORS, "Could not unmarshal configuration from database!:", err.Error())
	} else {
		raftdb_log(LOGLEVEL_INFO, "Loaded configuration from database:", p.cluster.config.version)
		raftdb_log(LOGLEVEL_DEBUG, "Configuration:", p.cluster.config.toConfig())
	}

}

func (p *processor) run() {
LOOP:
	for {
		MONVARS.AddMap("requests_per_processor", p.cluster.getClusterId(), 1)
		switch p.processor_state {
		case state_FOLLOWER:
			fallthrough
		case state_FOLLOWER_SNAPSHOTTING:
			p.runFollower()
		case state_CANDIDATE:
			p.runCandidate()
		case state_LEADER:
			p.runLeader()
		case state_FINALIZE:
			break LOOP
		}
	}
}

// Registers this processor as leader in clusterlookup.
// Call when transitioning to leader
func (p *processor) registerMasterWithClkup() error {

	hn, err := os.Hostname()

	if err != nil {
		return err
	}

	task, _, err := LOOKUP_CLIENT.LookupService(p.cluster.getClusterId()+constants.CLKUP_SUFFIX, 0)
	raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "Existing tasks", task)

	if err == nil || err.Error() == "SERVICE_NOT_FOUND" {
		if len(task) > 0 {
			for i := 0; i < len(task); i++ {
				raftdb_log(LOGLEVEL_INFO, p.cluster.getClusterId(), "Unregistering", p.cluster.getClusterId()+constants.CLKUP_SUFFIX, i)
				LOOKUP_CLIENT.Unregister(p.cluster.getClusterId()+constants.CLKUP_SUFFIX, uint32(i))
			}
		}

		raftdb_log(LOGLEVEL_INFO, p.cluster.getClusterId(), "Registering master as", p.cluster.getClusterId()+constants.CLKUP_SUFFIX)
		_, err = LOOKUP_CLIENT.Register(p.cluster.getClusterId()+constants.CLKUP_SUFFIX, hn, uint32(FLAG_port),
			clkup.ServiceType_CRPC_GENERIC, SECURITY_MANAGER.GetPublicKey())

		if err != nil {
			raftdb_log(LOGLEVEL_ERRORS, p.cluster.getClusterId(), "Registering failed with", err.Error())
			return err
		}
	} else {
		return err
	}

	return nil
}

// Prepare a configuration change. Returns an entry to be distributed, a snapshot of the old config, and a potential error.
// If the change fails to be committed, roll back the change using revertConfigRequest().
func (p *processor) prepareConfigRequest(rq *proto.ConfigRequest) (*proto.Entry, []byte, error) {
	oldconfig, _ := p.cluster.config.toConfig().Marshal()
	raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "Trying to apply config request", rq)

	p.cluster.config.apply(rq)

	raftdb_log(LOGLEVEL_DEBUG, "Applying config", p.cluster.config.toConfig())
	val, err := p.cluster.config.toConfig().Marshal()

	if err != nil {
		p.cluster.config.fromConfig(oldconfig)
		return nil, nil, err
	}

	term := p.st.Term()
	nextix, _, _ := p.st.Index()

	entry := &proto.Entry{Type: proto.Entry_PUT.Enum(),
		Key: []byte(constants.CONFIG_ENTRY_KEY),
		Entry: &proto.DBEntry{Type: proto.DBEntry_CONFIG.Enum(),
			Value: val,
			Version: &proto.VersionInfo{Term: pb.Uint64(term),
				LogIx: pb.Uint64(nextix)}},
	}

	return entry, oldconfig, nil
}

// Restore the previous cluster configuration (from the second value returned by prepareConfigRequest)
func (p *processor) revertConfigRequest(old []byte) {
	p.cluster.config.fromConfig(old)
}

// Once a configuration change has been committed, the coordinators can be started/stopped.
func (p *processor) commitConfigRequest(rq *proto.ConfigRequest) error {
	raftdb_log(LOGLEVEL_INFO, "Changing memberships")
	switch rq.GetType() {
	case proto.ConfigRequest_UNDEFINED:
		return errors.New("Invalid request type")
	case proto.ConfigRequest_ADD_MEMBER:
		if p.processor_state == state_LEADER {
			for _, name := range rq.GetMembers() {
				raftdb_log(LOGLEVEL_INFO, "Starting coordinator", name)
				// Run coordinator
				p.runMemberCoordinator(name)
			}
		}
	case proto.ConfigRequest_REMOVE_MEMBER:
		if p.processor_state == state_LEADER {
			for _, name := range rq.GetMembers() {
				raftdb_log(LOGLEVEL_INFO, "Stopping coordinator", name)
				p.cluster.removeMember(name)
			}
		}
	case proto.ConfigRequest_GET_CONFIG:
		return nil
	}

	return nil
}

// Looks up the name in clusterlookup and connects.
// Also runs the local coordinator.
// Usually called on an admin's ADD_MEMBER ConfigRequest
// Does however not add the member to p.cluster.config.
func (p *processor) runMemberCoordinator(name string) error {
	raftdb_log(LOGLEVEL_DEBUG, "Trying to add member", name)

	if nil != p.cluster.getMember(name) {
		raftdb_log(LOGLEVEL_WARNINGS, "Coordinator already exists")
		return errors.New("Coordinator already exists")
	}

	// Don't create a remotemember for us
	if name == p.member_id {
		return errors.New("[info] Not adding leader")
	}

	rm, err := newRemoteMember(p.member_id, name)

	if err != nil {
		raftdb_log(LOGLEVEL_ERRORS, p.cluster.getClusterId(), "Encountered", err.Error(), "when trying to create remote member")
		return err
	}

	ch := make(chan *RpcRequest, FLAG_coord_buffer)

	im := newInternalMember(ch, name)

	coord := newCoordinator(ch, rm, p.member_id, p.st, p.cluster.getClusterId())

	err = p.cluster.addMember(name, im)

	if err != nil {
		return err
	} else {
		raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "Running coordinator", coord)

		go coord.runCoordinator()
	}

	return nil
}

func (p *processor) convertFollower() {
	if p.processor_state == state_FOLLOWER {
		return
	}

	// Reset
	for _, m := range p.cluster.config.getMembers() {
		p.cluster.removeMember(m)
	}

	p.processor_state = state_FOLLOWER

	p.st.Broadcast(locking.MasterStepDown)

	go p.timeOutWaiter()
}

func (p *processor) convertLeader() {
	if p.processor_state == state_LEADER {
		return
	}

	if p.processor_state != state_LEADER && p.processor_state != state_CANDIDATE {
		// Connect to other members of the cluster
		for _, m := range p.cluster.config.getMembers() {
			if m != p.member_id {
				p.runMemberCoordinator(m)
			}
		}
	}

	p.st.SetNextTerm("<self>")

	p.registerMasterWithClkup()

	p.processor_state = state_LEADER
	go p.heartBeatSender()

	fake_clientrq := &proto.ClientRequest{Strategy: proto.ClientRequest_BATCH.Enum(),
		ClientId:  pb.String("<self>"),
		ClusterId: pb.String(p.cluster.getClusterId()),
		Actions:   []*proto.Action{&proto.Action{Type: proto.Action_SET.Enum(), Key: []byte("__RAFTDB_OLDTERM_COMMIT"), Value: []byte("___")}},
	}

	p.requests <- &RpcRequest{cr: fake_clientrq, rpchan: make(chan *RpcResponse, 1)}
}

// Convert a processor to a Candidate.
func (p *processor) convertCandidate() {
	if p.processor_state == state_CANDIDATE {
		return
	}

	if p.processor_state != state_LEADER {
		// Connect to other members of the cluster
		for _, m := range p.cluster.config.getMembers() {
			if m != p.member_id {
				p.runMemberCoordinator(m)
			}
		}
	}

	p.processor_state = state_CANDIDATE
}

func (p *processor) AppendEntries(rq *proto.AppendEntriesRequest) (*proto.AppendEntriesResponse, error) {
	// Shouldn't happen usually, only after re-election
	if p.processor_state == state_FOLLOWER_SNAPSHOTTING {
		p.st.Reset()
		p.convertFollower()
	}
	if p.processor_state != state_LEADER {
		// Don't block on this
		select {
		case p.heartbeat_channel <- true:
		default:
			raftdb_log(LOGLEVEL_WARNINGS, p.cluster.getClusterId(), "Heartbeat channel is filled up. Heartbeat receiver crashed?")
		}
	}

	p.leader_id = rq.GetLeaderId()

	if rq.GetLeaderLease() > 0 {
		p.leader_lease = time.Unix(0, int64(rq.GetLeaderLease()))
		raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "Extending leader lease for", p.leader_id, "by", p.leader_lease.Sub(time.Now()), "until", p.leader_lease)
	}

	// Look for config updates first
	for _, e := range rq.GetEntries() {
		if e.GetEntry().GetType() == proto.DBEntry_CONFIG {
			cfg := new(proto.Configuration)
			err := cfg.Unmarshal(e.GetEntry().GetValue())

			if err != nil {
				raftdb_log(LOGLEVEL_ERRORS, p.cluster.getClusterId(), "Could not apply config entry:", err.Error())
				continue
			}

			raftdb_log(LOGLEVEL_INFO, p.cluster.getClusterId(), "Applied config entry w/ version", cfg.GetConfigVersion())

			p.cluster.config.fromConfig(e.GetEntry().GetValue())
		}
	}

	term := p.st.Term()

	if rq.GetTerm() < term {
		raftdb_log(LOGLEVEL_INFO, p.cluster.getClusterId(), "Failing AE due to old term", rq.GetTerm(), "vs.", term)
	}

	rp, err := p.st.AppendEntries(rq)

	if err != nil {
		raftdb_log(LOGLEVEL_WARNINGS, p.cluster.getClusterId(), err)
	}

	next, commit, first := p.st.Index()
	raftdb_log(LOGLEVEL_DEBUG, "Current state (t,n,c,f):", term, next, commit, first)

	return rp, nil
}

func (p *processor) RequestVote(rq *proto.RequestVote) (*proto.Vote, error) {
	raftdb_log(LOGLEVEL_DEBUG, p.cluster.getClusterId(), "Voting on request", rq)

	rp := &proto.Vote{}

	if p.processor_state == state_FOLLOWER_SNAPSHOTTING {
		raftdb_log(LOGLEVEL_WARNINGS, p.cluster.getClusterId(), "Was snapshotting, reverting to FOLLOWER")

		p.st.Reset()
		p.convertFollower()
	}

	term := p.st.Term()
	rp.Term = pb.Uint64(term)
	rp.VoteGranted = pb.Bool(false)

	if rq.GetTerm() <= term {
		rp.FailType = proto.Vote_WRONG_TERM.Enum()

		raftdb_log(LOGLEVEL_WARNINGS, p.cluster.getClusterId(), "Vote denied: Requestor on wrong term (he vs. us):", rq.GetTerm(), term)

		return rp, nil
	}

	if rq.GetConfigVersion() < p.cluster.config.version {
		rp.FailType = proto.Vote_WRONG_CONFIG_VERSION.Enum()

		raftdb_log(LOGLEVEL_INFO, p.cluster.getClusterId(), "Vote denied: Config version too old (he vs. us):", rq.GetConfigVersion(), p.cluster.config.version)

		return rp, nil
	}

	if time.Now().Before(p.leader_lease) {
		rp.FailType = proto.Vote_LEASE_ACTIVE.Enum()

		raftdb_log(LOGLEVEL_INFO, p.cluster.getClusterId(), "Vote denied: Lease is still active until", p.leader_lease)

		return rp, nil
	}

	nextix, _, _ := p.st.Index()

	if rq.GetLogIx().GetLogIx() < nextix-1 {
		rp.FailType = proto.Vote_WRONG_LOGIX.Enum()

		raftdb_log(LOGLEVEL_WARNINGS, p.cluster.getClusterId(), "Vote denied: Requestor is not as up-to-date (he vs. us):", rq.GetLogIx().GetLogIx(), nextix-1)

		return rp, nil
	}

	if rq.GetLogIx().GetLogIx() == nextix-1 &&
		rq.GetLogIx().GetTerm() != p.st.GetLogEntry(rq.GetLogIx().GetLogIx()).GetEntry().GetVersion().GetTerm() {
		rp.FailType = proto.Vote_WRONG_ENTRY_TERM.Enum()
		raftdb_log(LOGLEVEL_WARNINGS, p.cluster.getClusterId(), "Vote denied: Wrong term of last entry")

		return rp, nil
	}

	rp.Term = pb.Uint64(rq.GetTerm())
	rp.VoteGranted = pb.Bool(true)

	raftdb_log(LOGLEVEL_INFO, p.cluster.getClusterId(), "Converted to FOLLOWER")

	for p.st.Term() < rq.GetTerm() {
		p.st.SetNextTerm(rq.GetCandidateId())
	}

	p.convertFollower()

	raftdb_log(LOGLEVEL_INFO, p.cluster.getClusterId(), "Vote casted:", rp)

	return rp, nil
}

func (p *processor) InstallSnapshot(rq *proto.InstallSnapshot) (*proto.InstallSnapshotResponse, error) {
	raftdb_log(LOGLEVEL_INFO, p.cluster.getClusterId(), "Installing snapshot from", rq.GetLeaderId())

	// TODO Move the state machine manipulation to the calling runner (e.g. runFollower())
	if p.processor_state != state_FOLLOWER_SNAPSHOTTING {
		p.st.Reset()
		p.processor_state = state_FOLLOWER_SNAPSHOTTING
	}

	select {
	case p.heartbeat_channel <- true:
		//
	}

	rp := p.st.InstallSnapshot(rq)

	if rq.GetDone() {
		raftdb_log(LOGLEVEL_INFO, p.cluster.getClusterId(), "Snapshot done")

		p.setConfigFromPersistent()
		p.convertFollower()
	}

	return rp, nil
}
