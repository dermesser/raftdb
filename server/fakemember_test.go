package server

import (
	"raftdb/proto"
	"testing"

	pb "github.com/gogo/protobuf/proto"
)

// Test the testing implementation :-P

var entry1 *proto.Entry
var entry2 *proto.Entry

func init() {
	FLAG_loglevel = LOGLEVEL_DEBUG
}

func setFMTUp() {
	entry1 = &proto.Entry{Type: proto.Entry_PUT.Enum(),
		Key: []byte("abc"),
		Entry: &proto.DBEntry{Type: proto.DBEntry_DATA.Enum(),
			Value:   []byte("def"),
			Version: &proto.VersionInfo{Term: pb.Uint64(1), LogIx: (pb.Uint64(1))},
		},
	}

	entry2 = &proto.Entry{Type: proto.Entry_PUT.Enum(),
		Key: []byte("123"),
		Entry: &proto.DBEntry{Type: proto.DBEntry_DATA.Enum(),
			Value:   []byte("456"),
			Version: &proto.VersionInfo{Term: pb.Uint64(1), LogIx: (pb.Uint64(2))},
		},
	}
}

func TestHeartBeatSucceeds(t *testing.T) {
	setFMTUp()
	m := newFakeMember(0, 0, 1, 1)

	rq := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId: pb.String("1"),
		PreviousLogVersion: &proto.VersionInfo{Term: pb.Uint64(1),
			LogIx: pb.Uint64(0)},
		Entries: []*proto.Entry{}}

	rp, err := m.AppendEntries(rq)

	if err != nil {
		t.Error("AppendEntries returned error", err.Error())
	}

	if !rp.GetSuccess() {
		t.Error("AppendEntries did not succeed")
	}
}

func TestFakeAppendEntries1(t *testing.T) {
	setFMTUp()
	m := newFakeMember(0, 0, 1, 1)

	rq := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId: pb.String("1"),
		PreviousLogVersion: &proto.VersionInfo{Term: pb.Uint64(1),
			LogIx: pb.Uint64(0)},
		Entries: []*proto.Entry{entry1}}

	rp, err := m.AppendEntries(rq)

	if err != nil {
		t.Error("AppendEntries returned error", err.Error())
	}

	if !rp.GetSuccess() {
		t.Error("AppendEntries did not succeed", rp.GetLastIndex())
	}
}

func TestFakeAppendEntries2(t *testing.T) {
	setFMTUp()
	m := newFakeMember(0, 0, 1, 1)

	rq := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId: pb.String("1"),
		PreviousLogVersion: &proto.VersionInfo{Term: pb.Uint64(1),
			LogIx: pb.Uint64(0)},
		Entries: []*proto.Entry{entry1}}

	rp, err := m.AppendEntries(rq)

	if err != nil {
		t.Error("AppendEntries returned error", err.Error())
	}

	if !rp.GetSuccess() {
		t.Error("AppendEntries did not succeed", rp.GetLastIndex())
	}

	rq2 := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId: pb.String("1"),
		PreviousLogVersion: &proto.VersionInfo{Term: pb.Uint64(1),
			LogIx: pb.Uint64(4)},
		Entries: []*proto.Entry{entry1}}

	rp2, err := m.AppendEntries(rq2)

	if err != nil {
		t.Error("AppendEntries returned error", err.Error())
	}

	if rp2.GetSuccess() {
		t.Error("AppendEntries did succeed", rp2.GetLastIndex())
	}

	if rp2.GetLastIndex() != 1 {
		t.Error("Failed AppendEntries returned wrong LastIndex", rp2.GetLastIndex())
	}
}

func TestFakeConsecutiveAppendEntries1(t *testing.T) {
	setFMTUp()
	m := newFakeMember(0, 0, 1, 1)

	rq := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId: pb.String("1"),
		PreviousLogVersion: &proto.VersionInfo{Term: pb.Uint64(1),
			LogIx: pb.Uint64(0)},
		Entries: []*proto.Entry{entry1}}

	rp, err := m.AppendEntries(rq)

	if err != nil {
		t.Error("AppendEntries returned error", err.Error())
	}

	if !rp.GetSuccess() {
		t.Error("AppendEntries did not succeed", rp.GetLastIndex())
	}

	rq2 := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId: pb.String("1"),
		PreviousLogVersion: &proto.VersionInfo{Term: pb.Uint64(1),
			LogIx: pb.Uint64(1)},
		Entries: []*proto.Entry{entry2}}

	rp2, err := m.AppendEntries(rq2)

	if err != nil {
		t.Error("AppendEntries returned error", err.Error())
	}

	if !rp2.GetSuccess() {
		t.Error("AppendEntries did not succeed", rp2.GetLastIndex())
	}
}

func TestFakeConsecutiveAppendEntriesSkipOne(t *testing.T) {
	setFMTUp()
	m := newFakeMember(0, 0, 1, 1)

	rq := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId: pb.String("1"),
		PreviousLogVersion: &proto.VersionInfo{Term: pb.Uint64(1),
			LogIx: pb.Uint64(0)},
		Entries: []*proto.Entry{entry1}}

	rp, err := m.AppendEntries(rq)

	if err != nil {
		t.Error("AppendEntries returned error", err.Error())
	}

	if !rp.GetSuccess() {
		t.Error("AppendEntries did not succeed", rp.GetLastIndex())
	}

	rq2 := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId: pb.String("1"),
		PreviousLogVersion: &proto.VersionInfo{Term: pb.Uint64(1),
			LogIx: pb.Uint64(2)},
		Entries: []*proto.Entry{entry2}}

	rp2, err := m.AppendEntries(rq2)

	if err != nil {
		t.Error("AppendEntries returned error", err.Error())
	}

	if rp2.GetSuccess() {
		t.Error("AppendEntries did succeed", rp2.GetLastIndex())
	}
}

func TestFakeRequestVoteSuccess(t *testing.T) {
	setFMTUp()
	m := newFakeMember(0, 0, 1, 1)
	rq := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId: pb.String("1"),
		PreviousLogVersion: &proto.VersionInfo{Term: pb.Uint64(1),
			LogIx: pb.Uint64(0)},
		Entries: []*proto.Entry{entry1}}

	rp, err := m.AppendEntries(rq)

	if err != nil {
		t.Error("AppendEntries returned error", err.Error())
	}

	if !rp.GetSuccess() {
		t.Error("AppendEntries did not succeed", rp.GetLastIndex())
	}

	rq2 := &proto.RequestVote{Term: pb.Uint64(2),
		CandidateId:   pb.String("2"),
		LogIx:         &proto.VersionInfo{Term: pb.Uint64(1), LogIx: pb.Uint64(1)},
		ConfigVersion: pb.Uint64(1),
	}

	rp2, err := m.RequestVote(rq2)

	if err != nil {
		t.Error("AppendEntries returned error", err.Error())
	}

	if !rp2.GetVoteGranted() {
		t.Error("AppendEntries did not succeed", rp2.GetFailType().String())
	}
}

func TestFakeRequestVoteFailTerm(t *testing.T) {
	setFMTUp()
	m := newFakeMember(0, 0, 1, 1)
	rq := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId: pb.String("1"),
		PreviousLogVersion: &proto.VersionInfo{Term: pb.Uint64(1),
			LogIx: pb.Uint64(0)},
		Entries: []*proto.Entry{entry1}}

	rp, err := m.AppendEntries(rq)

	if err != nil {
		t.Error("AppendEntries returned error", err.Error())
	}

	if !rp.GetSuccess() {
		t.Error("AppendEntries did not succeed", rp.GetLastIndex())
	}

	// Use too old term
	rq2 := &proto.RequestVote{Term: pb.Uint64(1),
		CandidateId: pb.String("2"),
		LogIx:       &proto.VersionInfo{Term: pb.Uint64(1), LogIx: pb.Uint64(1)},
	}

	rp2, err := m.RequestVote(rq2)

	if err != nil {
		t.Error("AppendEntries returned error", err.Error())
	}

	if rp2.GetVoteGranted() {
		t.Error("AppendEntries did succeed", rp2.GetFailType().String())
	}
}

func TestFakeRequestVoteFailLogix(t *testing.T) {
	setFMTUp()
	m := newFakeMember(0, 0, 1, 1)
	rq := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId: pb.String("1"),
		PreviousLogVersion: &proto.VersionInfo{Term: pb.Uint64(1),
			LogIx: pb.Uint64(0)},
		Entries: []*proto.Entry{entry1}}

	rp, err := m.AppendEntries(rq)

	if err != nil {
		t.Error("AppendEntries returned error", err.Error())
	}

	if !rp.GetSuccess() {
		t.Error("AppendEntries did not succeed", rp.GetLastIndex())
	}

	// Use too old term
	rq2 := &proto.RequestVote{Term: pb.Uint64(2),
		CandidateId: pb.String("2"),
		LogIx:       &proto.VersionInfo{Term: pb.Uint64(1), LogIx: pb.Uint64(0)},
	}

	rp2, err := m.RequestVote(rq2)

	if err != nil {
		t.Error("AppendEntries returned error", err.Error())
	}

	if rp2.GetVoteGranted() {
		t.Error("AppendEntries did succeed", rp2.GetFailType().String())
	}
}

func TestFakeRequestVoteFailLastLogTerm(t *testing.T) {
	setFMTUp()
	m := newFakeMember(0, 0, 1, 1)
	rq := &proto.AppendEntriesRequest{Term: pb.Uint64(1),
		LeaderId: pb.String("1"),
		PreviousLogVersion: &proto.VersionInfo{Term: pb.Uint64(1),
			LogIx: pb.Uint64(0)},
		Entries: []*proto.Entry{entry1}}

	rp, err := m.AppendEntries(rq)

	if err != nil {
		t.Error("AppendEntries returned error", err.Error())
	}

	if !rp.GetSuccess() {
		t.Error("AppendEntries did not succeed", rp.GetLastIndex())
	}

	// Use too old term
	rq2 := &proto.RequestVote{Term: pb.Uint64(2),
		CandidateId: pb.String("2"),
		LogIx:       &proto.VersionInfo{Term: pb.Uint64(0), LogIx: pb.Uint64(1)},
	}

	rp2, err := m.RequestVote(rq2)

	if err != nil {
		t.Error("AppendEntries returned error", err.Error())
	}

	if rp2.GetVoteGranted() {
		t.Error("AppendEntries did succeed", rp2.GetFailType().String())
	}
}
